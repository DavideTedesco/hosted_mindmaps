#!/usr/bin/env bash

# cdo-gitpush-linux.sh
#
# use it by terminal typing:
# bash cdo-linux.sh

cd /home/davide/gitlab/DavideTedesco/hosted_mindmaps
git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push
