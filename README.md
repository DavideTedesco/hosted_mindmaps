# hosted_mindmaps

Repository full of mindmaps created in Markdown and renderd with [markmap](https://markmap.js.org/)!

## List of mindmaps (TODO like those one below)
1. [https://davidetedesco.gitlab.io/hosted_mindmaps/mindmap.html?md=CatenaDirettaControreazione.md](https://davidetedesco.gitlab.io/hosted_mindmaps/mindmap.html?md=CatenaDirettaControreazione.md)
2. or specifing the whole URL like this [https://davidetedesco.gitlab.io/hosted_mindmaps/mindmap.html?materia=Fisica_I&data=20240404&md=01_Meccanica.md](https://davidetedesco.gitlab.io/hosted_mindmaps/mindmap.html?materia=Fisica_I&data=20240404&md=01_Meccanica.md)

## How does all of this functions?

### Markmap
Using [markmap](https://markmap.js.org/) make it easier to realise mindmaps from standard plain Markdown.

Something like this:
```
# Controllo a catena aperta e a controreazione
- Introduzione: a cosa serve un controllore
    - trattando di sistemi all'interno dell'automatica
    - i controllori sono: dispositivi utilizzati per modificare il comportamento di un sistema
- cosa è il controllo a catena aperta
    - separazione $P(s)$ e $C(s)$
    - azione di controllo che non dipende dall’uscita
    - senza feedback
    - esempio con controllore proporzionale in catena diretta
- cosa è il controllo a controreazione (catena chiusa)
    - azione di controllo che dipende dall’uscita
    - separazione $P(s)$, $C(s)$ e $H(s)$
    - con feedback
    - esempio di acceleratore in macchina
- pregi e difetti della catena aperta
    - pregi
      - più semplice da implementare
    - difetti
      - non ha un controllo sugli errori
      - molto sensibile alle variazioni parametriche
      - non può effettuare asservimento e regolazione
- pregi e difetti della controreazione
    - pregi
      - più preciso
      - capace di rigettare distrubi sulla catena diretta
      - può effettuare asservimento e regolazione
    - difetti
      - più costoso da realizzare

```

Will be rendered as this:
![example/example.png](example/example.png)

### Gitlab pages

When the Markdown file is ready is updated to a Gitlab Repository (the one that you are in now), and a pipeline is being triggered to render an HTML page to show the HTML result of the map, like the example seen above: [Controllo a catena aperta e a controreazione](https://davidetedesco.gitlab.io/hosted_mindmaps/mindmap.html?md=CatenaDirettaControreazione.md).

The HTML page used to render all the maps is this one:
```
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title id="titolo">Mindmap</title>
    <style>
      svg.markmap {
        width: 100%;
        height: 100vh;
      }
      html {
        background-color: white;
      }
    </style>
    <script>

        window.markmap = {
        autoLoader: { manual: true, toolbar: true }
  };

    </script>

    <script src="https://cdn.jsdelivr.net/npm/markmap-autoloader@latest"></script>
  </head>
  <body>
    <div class="markmap">
      <script type="text/template" id="markdown">
      </script>
    </div>
<script
			  src="https://code.jquery.com/jquery-3.7.0.min.js"
			  integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g="
			  crossorigin="anonymous"></script>

    <script>
        const BASE_URL = "https://davidetedesco.gitlab.io/hosted_mindmaps/"
        let params = new URLSearchParams(document.location.search);
        let name = params.get("md");
        let titolo_con_estensione = params.get("md").replace(".md", "");
        $("#titolo").html(titolo_con_estensione)
        var materia = params.get("materia") || "FdA";
        var data = params.get("data");
        if(data != null)
        {
          if(data.length == 4)// ddmm
               data = "2024"+data;
            var url = BASE_URL+materia+"/"+data+"/"+name; }
        else
          var url = BASE_URL + materia +"/"+ name;



        $.ajax({
          url: url,
          type: 'get',
          success: function(response){
            console.log("Loaded mindmap "+name);
            $("#markdown").html(response)
            setTimeout(() => {
            markmap.autoLoader.renderAll();
            }, 200);
          },
          error: function(xhr, status, error)
          {
            console.log("Ajax Error:\nURL: "+url+" status: "+xhr.status);
          }
        })
    </script>
  </body>
</html>
```
This page accepts some parameters to dynamically build the Markdown file's URL and load it with AJAX request to use a single HTML page to display all the mindmaps.

In details:
- `materia` is the subject (can be _FdA_ or _Fisica\_1_), defaults with _FdA_ to keep backwards compatibility with old URLs
- `data` is the date of the lesson: for newer lessons they are kept in different folders (eg: Fisica_1/2024xxxx/Riepilogo.md)
- `md` is the filename for the markdown file.
The URL is build and an AJAX request is made, after which the `response` data is inserted in the `<div id="markdown">`

After that we use the `autoLoader.renderAll()` method to render the mindmap on the webpage.

---
Realised by @FrancescoMartino & @DavideTedesco !
