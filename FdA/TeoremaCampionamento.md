---
markmap:
  maxWidth: 400
---

# Teorema del campionamento (spettro di un segnale campionato, ricostruttore di Shannon)

## Introduzione
### a cosa serve campionare
#### campionare è utile per effettuare un passaggio dal segnali continui a segnali discreti
### quindi ad esempio per passare da segnali analogici a segnali digitali
### è importante differenziare i procedimenti di campionamento e quantizzazione
#### campionamento: è la discretizzazione dei valori di un segnale continuo in alcuni suoi particolari istanti di tempo
##### il tempo che intercorre tra i vari istanti di campionamento lo chiameremo _tempo di campionamento_
#### quantizzazione: è la discretizzazione del livello di ampiezza di un segnale continuo in un preciso istante (essa è riferita ad un preciso campione)
### richiamo sugli intervalli di campionamento
#### ![FdA/images/tempcamp.png](FdA/images/tempcamp.png)
##### Osserviamo che il tempo che comprende conversione ADC, elaborazione (tempo di calcolo del microprocessore) e conversione DAC, più l’intervallo di _housekeeping_ del microcontrollore (tempo necessario al microcontrollore per effettuare delle sue operazioni), viene chiamato __Tempo di campionamento__

## Il segnale campionato
### sequenza di impulsi, ovvero il segnale ottenuto discretizzando
#### può essere rappresentato come una sommatoria di $\delta$ traslate nel tempo $\displaystyle x_c(t)=\sum_{k=0}^{+\infty}{x_k \delta(t-kT_c)}$
##### si osserva che $x_c(t)$ è nullo per $t<0$, quindi non sarà necessario inserirlo nella sommatoria
#### effettuandone la Trasformata Laplace otteniamo:
##### $\displaystyle X_c(s)=\sum_{k=0}^{+\infty}X_ke^{-\sigma kT_c}\cdot e^{-j\omega kT_c}$
##### osserviamo che $\omega_c = 2\pi \cdot f_c$ e, sapendo che $\displaystyle f_c = \frac {1}{T_c}$ allora $\displaystyle T_c=\frac{2\pi}{\omega_c}$
##### l'oggetto matematico ottenuto è un qualcosa di periodico che possiamo studiare in Laplace e successivamente in frequenza
##### osserviamo che esso in Laplace, essendo periodico, realizza delle fasce orizzontali continue
###### ![FdA/images/laplacecamp.png](FdA/images/laplacecamp.png)


## Trasformata di Fourier di un segnale campionato
### essa è utile per poter osservare l'effetto del campionamento a livello frequenziale, ed arrivare all'enunciato del Teorema del campionamento
### da essa si possono identificare artefatti nel segnale campionato a livello frequenziale come l'aliasing
### effettuando la Trasformata di Fourier di $x_c(t)$ nell'intervallo di interesse che va da $\displaystyle -\frac {T_c} {2}$ $+\frac {T_c} {2}$
### $\displaystyle c_k=\frac{1}{T_c}\int_{-T_c/2}^{T_C/2}f(t)e^{-jk\omega_c t}dt$
### osserviamo che essendovi nel tempo un solo impulso in tale intervallo, il risultato di tale integrale sarà $c_k = \frac 1 {T_c}$
### quindi il campionamento di un segnale fa riscalare il suo modulo (ampiezza) di un fattore dipendente dal tempo di campionamento
### osserviamo quindi che il segnale continuo di partenza si possa riscrivere come se stesso, moltiplicato per una serie di impulsi riportante l'antistrasformata di Fourier per un coefficiente $\frac 1 {T_c}$
### quindi in Laplace avremo: $\displaystyle X_c(s)=\frac1{T_c}\sum X(s-jk\omega_c)$
### e sostitudendo $s=j\omega$ otterremo $\displaystyle X_c(s)=\frac 1{T_c}\sum X(j\omega-jk\omega_c)$
#### ed osserviamo che prendendo in esame la sola fascia centrale potremo svolgere la Trasformata di Fourier per il solo intervallo descritto prima vedendo come con l'ultima sostituzione possiamo descrivere su un diagramma di Bode il modulo
#### ![FdA/images/freqcamp.png](FdA/images/freqcamp.png)
##### nell'immagine osserviamo che vi sono varie repliche del segnale di partenza i quali estremi chiameremo $-\omega_m$ ed $\omega_m$
##### è importante notare che il segnale appena visto risulta costituito da infinite repliche dello stesso segnale, sarà dunque utile solo una replica per ricostruire il segnale di partenza
##### ciò è possibile per mezzo di un filtraggio che faccia passare solamente la replica centrata in zero

## Enunciato del Teorema del campionamento
### _Il teorema afferma che dato un segnale a banda limitata, nella sua conversione analogico-digitale la minima frequenza di campionamento necessaria per evitare aliasing e perdita di informazione nella ricostruzione del segnale analogico originario dev’essere maggiore del doppio della sua frequenza massima._
### la frequenza minima descritta da Shannon per il campionamento e la ricostruzione del segnale originale deriva da:
#### $\omega_c>2\omega_m \rightarrow \omega_m<\frac {\omega_c} {2}$  in cui $2\omega_m$ è la larghezza del segnale in zero (nell'ultima immagine), esso identifica la banda limitata del segnale campionato in Laplace
#### tale ricostruttore è quello ideale descritto da Shannon per cui la frequenza di campionamento $f_c$ debba essere esattamente pari al doppio della frequenza massima del segnale da campionare, o ancor meglio pari al doppio dell'$\omega_{taglio}$ (quando il modulo nel diagramma di Bode taglia lo zero)


## Dal segnale discreto a quello continuo (ricostruzione ideale di Shannon)
### come descritto precedentemente la frequenza di campionamento deve essere al minimo $2\cdot f_c$, ma essa non è spesso la migliore scelta
### problemi del ricostruttore ideale di Shannon
#### in realtà non esiste nel mondo reale un filtraggio possibile che possa mantenere la sola replica del segnale centrata in zero
#### dato che un filtro passa basso ideale non è realizzabile, inoltre un filtro a forma di rettangolo in frequenza, significherebbe nel tempo un qualcosa a forma di Sinc
#### da ciò si denota che si dovrebbe avere anche una parte anti-causale, quindi sarebbe come se per ogni campione si avesse nel tempo una Sinc che si va a sommare con tutte le altre Sinc dei vari campioni nel tempo
##### ![FdA/images/sincscamp.png](FdA/images/sincscamp.png)
#### per poter utilizzare comunque un filtro passa basso, ma diminuire le componenti laterali ad esso, si utilizza in genere una frequenza di campionamento molto maggiore del doppio della $\omega_m$ come ad esempio $10\omega_m$

## Difficoltà nell'utilizzo dell'organo di tenuta di ordine zero (ZOH) al posto del ricostruttore ideale
### esso è un particolare sistema non ideale di ricostruzione del segnale in grado di mantenere il valore di ampiezza di un campione fino all'istante di campionamento successivo
### seppur tale soluzione è molto semplice e computanzionalmente economica (dato che vi sono solamente segnali elementari da trattare, ovvero impulsi), essa può rendere nei sistemi di controllo, il segnale ricostruito molto spigoloso (con gradini)
### di fatto esso è in grado di _tenere_ il valore di ampiezza per un certo periodo di tempo
### approfondimento sull'[organo di tenuta](https://www.notion.so/Fondamenti-di-Automatica-8eb190133f0e48c1a7de6bdcf03d02ea?pvs=4#37d6d7bb528e4c72ba2e48115c400732)

## Aliasing
### le repliche laterali nel segnale campionato visto in frequenza sino ad ora, prendono il nome di alias, e per poter evitare che esse entrino all'interno del segnale ricostruito è utile porre un filtro passa basso denominato filtro anti-aliasing
### in caso non si inserisse tale filtro, si potrebbe rischiare di avere parte delle repliche laterali all'interno del segnale finale:
#### ![FdA/images/aliasing.png](FdA/images/aliasing.png)
### tale filtro in genere taglia in genere tutte le frequenza maggiori di $\frac {\omega_c} 2$
