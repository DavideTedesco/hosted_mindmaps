---
markmap:
  maxWidth: 300
---

# Luogo delle Radici

## Introduzione
### il Luogo delle Radici è un grafico sul piano di Gauss che rappresenta la posizione dei poli del sistema in studio al variare del guadagno del controllore $K_c$
### Dato un __sistema di controllo a controreazione__ con processo $P(s) = \frac {N(s)}{D(s)}$
### Inseriamo un controllore proporzionale $K_c$ all'interno del sistema

## Funzione di trasferimento del sistema in osservazione
### $W(s) = \frac {K_c\cdot N(s)} {D(s)+K_c+N(s)}$
### Il luogo delle radici di $W(s)$ (è l’insieme dei punti che rappresentano la posizione dei poli di $W(s)$ al variare di $K_c$) parte dai poli del ciclo aperto e finisce negli zeri del ciclo aperto.


## A cosa serve
### stabilire il valore di $K_c$ tale che il sistema rispetti le specifiche date:
#### __stabilità__: ovvero mantenere le radici del denominatore a parte reale negativa e minimizzare l'errore a monte del controllore. 
#### specifiche sul transitorio, ovvero studio di:
##### Overshoot (sovraelongazione): determinata dallo smorzamento e dalla vicinanza dei poli allo zero.
##### Rise time (tempo di salita): determinato dalla posizione di poli e zeri e dal guadagno 

## Studio del grafico del luogo delle radici:
### Studio del guadagno $K_c$
#### Andremo ad osservare il denominatore $D(s) +K_c+N(s)$ per determinare il comportamento delle dinamiche del sistema:
##### Modificando il guadagno $K_c$ potremo modificare __attraverso il denominatore__ della funzione $W(s)$ il comportamento del sistema.
### Il luogo delle radici può essere utilizzato per trovare il massimo guadagno $K_c$ inseribile in un sistema per farlo rimanere stabile: i poli devono sempre rimanere sul semipiano reale negativo.
### Il grafico del luogo delle radici parte nei poli e termina negli zeri: se il sistema è stabile 
### Osservando dove sono posizionati i poli e sapendo che l'oscillazione dipende dalla presenza o meno di poli complessi e coniugati, e che le dinamiche dipendono dalla parte reale dei poli; potremo osservare che: 
#### con due poli molti vicini all’asse immaginario (con parte reale piccola) le oscillazioni impiegheranno maggior tempo per essere smorzate, 
#### con due poli molto lontani dall’asse degli immaginari lo smorzamento sarà piú elevato.

## Come utilizzare il luogo delle radici
### Avendo già realizzato il luogo delle radici di un sistema e dovendo rispettare delle specifiche date, dovremmo scegliere un guadagno che le rispetti, nello specifico:
#### $K_c \rightarrow 0$ (assenza del controllore proporzionale): il sistema si evolverà secondo le sue dinamiche (date dal denominatore)
#### $K_c \rightarrow \infty$ (guadagno elevato): Le dinamiche originali del sistema vengono alterate, in quanto il numeratore $N(s)$ acquista maggior peso.

## Effetti di un più alto guadagno
### Diminuzione errore al permanente
### Diminuzione Rise Time
### Aumento sovraelongazione
