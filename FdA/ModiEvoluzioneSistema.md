---
markmap:
  maxWidth: 500
---

# Modi propri d’evoluzione di un sistema lineare e stabilità di un sistema lineare

## Introduzione
### osservando la tipologia e il posizionamento dei poli della trasformata di Laplace di un sistema sul piano di Gauss
#### è possibile prevedere l'andamento nel tempo di un sistema

## Costante di tempo $\tau$
### tracciando una retta che interseca l'asse delle ordinate e tangente al segnale ed osservandone l'intercetta sull'asse delle ascisse è possibile trovare una costante di tempo che rappresenta quanto velocemente il sistema converga a 0
### essa è caclolabile come $\tau = - \frac 1 p$
#### dopo  $3\tau$ si è ridotto il 95% del segnale iniziale
#### con posizione del polo pari a $-p$ in $(s-p)$

## Tipologie di modi
### costante
#### $y(t)=\delta_{-1}(t) \rightarrow \mathscr{L}_{-}\{y(t)\} = \frac 1 s$
### esponenziale
#### $y(t)= e^{pt} \rightarrow \mathscr{L}_{-}\{y(t)\} = \frac 1 {s+p}$
##### da notare che il polo è parte reale negativa, quindi l'esponente di $e$ deve essere negativo, quindi $p$ è un numero negativo
### oscillatorio
#### si presenta il Laplace in questa forma $\frac 1 {s^2+a\cdot s+b}$
#### si riferisce a poli complessi e coniugati

## Stabilità asintotica
### un sistema è __asintoticamente stabile__
#### se dopo una certa quantità di tempo, esso assume lo stesso comportamento dell’ingresso
#### se tutti i coefficienti al denominatore sono positivi (condizione sufficiente ma non necessaria) ovvero tutti i poli sono a parte reale negativa
#### esempi
##### ![FdA/images/1.png](FdA/images/1.png)
###### andamento esponenziale con polo reale negativo
##### ![FdA/images/4.png](FdA/images/4.png)
###### andamento oscillatorio con poli complessi e coniugati a parte reale __negativa__: _inviluppo convergente_
### sistema al limite di stabiltà
#### se i coefficienti del denominatore sono tutti positivi ma esiste un polo nell’origine allora siamo al limite della stabilità
#### esempi
##### ![FdA/images/3.png](FdA/images/3.png)
###### andamento costante con polo in zero
##### ![FdA/images/6.png](FdA/images/6.png)
###### andamento oscillatorio con poli complessi e coniugati con parte reale nulla
### sistema __asintoticamente instabile__
#### tutti quei sistemi divergenti
#### tutti quei sistemi al limite di stabilità che abbiano molteplicità maggiore di 1 dei poli immaginari puri con parte reale nulla
#### esempi
##### ![FdA/images/2.png](FdA/images/2.png)
###### andamento esponenziale con polo reale positivo
##### ![FdA/images/5.png](FdA/images/5.png)
###### andamento oscillatorio con poli complessi e coniugati a parte reale __positiva__: _inviluppo divergente_

## Sistema semplicemente stabile
### se non si hanno modi divergenti ma solamente modi convergenti o stazionari
#### in tal caso il sistema è _semplicemente stabile_
