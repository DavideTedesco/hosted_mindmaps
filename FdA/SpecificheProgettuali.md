---
markmap:
  maxWidth: 610
---

# Specifiche ad anello chiuso ed aperto per un sistema di controllo

## Introduzione
### oltre ad utilizzare la catena diretta di un sistema, in genere si usa anche la sua funzione di trasferimento a ciclo chiuso, per individuare altre caratteristiche del sistema, e capire come vengano influenzati i margini di stabilità e l'$\omega_{taglio}$
### partendo da una funzione  di trasferimento $F(s)$ possiamo scriverne la sua funzione di trasferimento a ciclo chiuso che può essere così realizzata $W(s)=\frac{K_dF(s)}{1+K_dF(s)}$
#### notiamo in essa come sia riportato per esteso anche il guadagno statico $K_d$

## Specifiche derivanti dalla risposta a gradino della funzione a ciclo chiuso
### ![FdA/images/specificheGradino.png](FdA/images/specificheGradino.png)
### __Tempo di salita__ o __Rise time__ misurato in secondi
#### esso specifica il tempo impiegato dal sistema per portare il valore di uscita dal 10% al 90% del valore finale, $t_r$ nell'immagine
### __Sovraelongazione__ o __Overshoot__ misurata in percentuale
#### essa specifica la differenza tra il valore massimo raggiunto e il valore finale in percentuale, $S$ nell'immagine
### __Tempo di assestamento__  misurato in secondi, $t_a$ nell'immagine
#### esso indica il tempo in cui il sistema si stabilizza verso il valore a regime, in una fascia del $\approx 3\% $

## Specifiche derivanti dal diagramma di Bode della funzione a ciclo chiuso
### ![FdA/images/specificheBode.png](FdA/images/specificheBode.png)
### __Modulo alla risonanza__  misurato in dB, $M_r$ nell'immagine
#### esso indica alla frequenza di risonanza, un valore del modulo in dB che tale frequenza amplifica; la frequenza di risonanza è la frequenza naturale del sistema
### __Banda passante a 3dB__  misurato in radianti al secondo, $\omega_{-3}$ nell'immagine
#### dopo avere passato la risonanza, e quindi successivamente all'$\omega_{taglio}$ indentifichiamo quella frequenza in cui il modulo scende di 3dB; essa rappresenta un riferimento che indica il punto dopo il quale le frequenze subiscono un’attenuazione di oltre il 30%. In pratica, esso è il punto oltre il quale il sistema smette di funzionare come vorremmo.

## Legami tra tali parametri e margini di stabilità
### il _Tempo di salita_
#### un _Tempo di salita_ minore, rende un sistema più reattivo e supporta frequenze elevate, quindi è in grado di oscillare più rapidamente ed avere una più alta _Banda passante_
### la _Sovraelongazione_
#### influenza lo _Smorzamento_ $\zeta$, poichè più è piccola la _Sovraelongazione_, maggiore sarà lo _Smorzamento_
#### influenza anche il _Modulo alla risonanza_, poichè un grande _Smorzamento_ implica un minore _Modulo alla risonanza_
### il guadagno statico
#### permette di traslare verso l'alto il grafico del modulo, implicando una traslazione verso destra di $\omega_{taglio}$ e di _banda passante_
#### inoltre aumentando il guadagno, il tempo di salita $t_r$ diminuirà come potrebbe anche diminuire il _margine di fase_
### osserviamo anche come all'aumentare del _margine di fase_, il _modulo alla risonanza_ diminuisce, inoltre dato che il modulo alla risonanza è legato alla _Sovraelongazione_ (come espresso precedentemente), vorrà dire che per aumentare il _margine di fase_, significherà diminuire la _Sovraelongazione_
