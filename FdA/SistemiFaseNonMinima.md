---
markmap:
  maxWidth: 600
---

# Sistemi a fase non minima

## Introduzione
### I sistemi a fase minima sono quelli dove ogni zero e ogni polo hanno parte reale negativa.
### I sistemi a fase non minima sono sistemi di controllo che, a ciclo aperto, hanno uno o più zeri e/o poli allocati nel semipiano positivo, cioè sono a parte reale positiva.


## Il sistema studiato
### ![FdA/images/SistemaFaseNonMinima.png](FdA/images/SistemaFaseNonMinima.png)
### La funzione a ciclo chiuso di tale sistema è $W(s)=\frac{K_c\cdot N(s)}{D(s)+K_c\cdot N(s)}$
### Osserviamo che per tale funzione per:
#### $K_c\rightarrow 0$ allora $K_c\cdot N(s)$ tende a 0, quindi sia al numeratore che al denominatore tale componente sarà irrilevante
#### $K_c\rightarrow \infty$ allora $K_c\cdot N(s)$ tende a $\infty$, quindi al denominatore la $D(s)$ diverrà irrilevante e quindi i poli di $W(s)$ saranno pari a circa $N(s)$
##### Ciò significa che per alti guadagni si potrobbero avere in quest'ultimo caso poli a parte reale positiva nel sistema che lo rendono instabile

## Esempio di sistema a fase non minima
### Osserviamo di seguito un esempio in MATLAB di sistema a fase non minima con funzione di trasferimento pari a $\frac {2-s} {s+1}$
#### In esso vediamo che:
##### si ha uno zero a parte reale positiva
##### si ha un polo a parte reale negativa
### effettuiamo `bode` ![FdA/images/BodeSistemaFaseNonMinima.png](FdA/images/BodeSistemaFaseNonMinima.png)
### effettuiamo la `step`![FdA/images/StepSistemaFaseNonMinima.png](FdA/images/StepSistemaFaseNonMinima.png)
#### osserviamo come il sistema (inserito un ingresso a gradino in esso), parta da -1, per arrivare solamente a regime permanente a 0.5


## Differenza tra sistemi a fase minima e non minima
### Per definizione, i sistemi a __fase minima__ sono quelli in cui la funzione di trasferimento non presenta poli o zeri sul semipiano complesso positivo, ergo sistemi stabili (che siano asintotici o meno, va studiato nello specifico).
### I sistemi a __fase non minima__, presentano funzione di trasferimento con qualche polo o zero a parte reale positiva. Anche qui bisogna studiare la stabilità nello specifico (con i criteri di stabilità classici o plottando la funzione di trasferimento in qualche simulatore).

## Conclusioni
### Per realizzare controlli per sistemi con zeri a parte reale positiva, bisogna attendere che il processo si stabilizzi, perchè nella regione transitoria si potrebbero riscontrare comportamenti che hanno direzione opposta rispetto a quella per cui è stato realizzato il controllore
### Si ricorda consiglia di osservare come cambino gli andamenti di poli e zeri a [questo link](https://davidetedesco.gitlab.io/hosted_mindmaps/mindmap.html?md=DiagrammaBode.md)
