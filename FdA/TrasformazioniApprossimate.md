---
markmap:
  maxWidth: 400
---

# Trasformazioni approssimate dal piano S al piano Z

## Introduzione
### dopo aver osservato la trasformazione esatta dal piano S al piano Z, che permette di passare semplicemente da un segnale continuo ad uno discretizzato
### abbiamo osservato come essa sia limitata e poco corretta dal punto di vista del guadagno poichè dipendente dal tempo di campionamento

## Le varie tipologie di trasformazioni
### trasformazione guadagno
#### essa è utile per mantenere inalterato il guadagno rispetto alla forma precedente alla discretizzazione
#### moltiplicando e dividendo per gli adeguati valori il guadagno in $z$ potremo ottenere una trasformazione con guadagno analogo a quella in $s$
#### anche tale trasformazione non sarà soddisfacente, quindi ne osserviamo delle altre
#### ![FdA/images/trasformazioneGuadagno.png](FdA/images/trasformazioneGuadagno.png)
##### possiamo osservare come essa sia dipendente dal tempo di campionamento $T_c$
##### per realizzare tale trasformazione è necessario utilizzare il metodo dei poli e residui
### trasformazione ZOH
#### un'altra tipologia di trasformazione si basa sul funzionamento, visto in precedenza, dell'Organo di tenuta, anche chiamato ZOH (Zero Order Holder)
#### tale tipologia di trasformazione, permette di ottenere al posto degli impulsi delle gradinate
#### ricordando la funzione di trasferimento dell'Organo di tenuta $T(s) = \frac {1-e^{-sT_c}} { s}$ potremo effettuare la trasformazione da s a Z, della funzione di partenza $F(s)$, moliplicata per la funzione di ZOH
#### $Z[F(s)\cdot T(s)] = Z \Big[\frac {F(s)} s \Big]\cdot (1-z^{-1})$
#### quindi sarebbe come dire che si effettua la sottrazione tra $\frac {F(s)} s$ e la se stessa ritardata di un campione.
### trasformazione Tustin (o bilineare)
#### essa è l'unica trasformazione che approssima bene un segnale continuo, affrontata nel corso
#### partendo dall'integrale dell'istante $k+1$ osserviamo diverse tipologie di approssimazione dell'integrazione, infatti nel dominio di Laplace $\frac 1 s$ indica un integratore
##### tale approssimazione è implicitamente rappresentata come un qualcosa di campionato dal continuo
###### ![FdA/images/integraleApprossimato.png](FdA/images/integraleApprossimato.png)
##### forward approximation
###### tale approssimazione si basa sull'osservare (dal campione $kT_c$) il campione successivo $(k+1)T_C$
###### $Y_{k+1}=Y_k+U_kT_c$ quindi trasformata in $Z$ diviene $Y(z)z=Y(z)+U(z)T_c$ e osservando la funzione di trasferimento, avremo $\frac {Y(z)} {U(z)} = \frac {T_c z^{-1}}{1-z^{-1}} =\frac{T_c}{z-1}\approx\frac 1 s \Rightarrow s\approx\frac{z-1}{T_c}$
###### ![FdA/images/forwardApproximation.png](FdA/images/forwardApproximation.png)
##### backward approximation
###### tale approssimazione si basa sull'osservare (dal campione $(k+1)T_c$) il campione precedente $kT_C$
###### $Y_{k+1}=Y_k+U_{k+1}T_c$  quindi trasformata in $Z$ diviene $Y(z)z=Y(z)+U(z)zT_c$ e osservando la funzione di trasferimento, avremo $\frac {Y(z)} {U(z)} = \frac {Tc} {1-z^{-1}}  = \frac{zT_c}{z-1} \approx \frac 1 s \Rightarrow s\approx\frac{z-1}{zT_c}$
###### ![FdA/images/backwardApproximation.png](FdA/images/backwardApproximation.png)
##### bilinear approximation
###### tale approssimazione si basa sull'area sottesa dalla curva, approssimando la regione per mezzo di un trapezio che ha come base $u_k$ e $u_{k+1}$ e come altezza $T_C$ guardando il trapezio ruotato di 90º in senso antiorario
###### protremo quindi osservare che l'approssimazione è pari all'area del trapezio $Y_{k+1}=Y_k+\frac{(U_k+U_{k+1})T_c} 2$ e quindi trasformato in $z$ diviene $Y(z)z=Y(z)+\frac{(U(z)+zU(z))T_c}2$ e calcolando la funzione di trasferimento, avremo: $\frac {Y(z)} {U(z)} = \frac {T_c}{2}\cdot \frac{1+z^{-1}}{1-z^{-1}} = \frac{T_c}2\frac{z+1}{z-1}\approx\frac1s\Rightarrow s\approx\frac2{T_c}\frac{z-1}{z+1}$
###### ![FdA/images/bilinearApproximation.png](FdA/images/bilinearApproximation.png)
#### l'ultima è la trasformazione Tustin
####  la trasformazione Tustin porta nel piano Z elementi del piano S

## considerazioni sulla trasformazione Tustin e  compressioni del piano che essa realizza
### mapping da piano s a piano z
#### come visto in precedenza, è possibile effettuare un mapping dal piano S al piano Z
#### tale mapping è in genere realizzato ponendo $z=e^{sT_c}$
##### ![FdA/images/mappingSZ.png](FdA/images/mappingSZ.png)
#### come vediamo il mapping della forward approximation (in grigio) comprime alcune sezioni del piano S in Z, facendo spostare degli eventuali poli o zeri a parte reale negativa a destra:
##### ![FdA/images/mappingSZforward.png](FdA/images/mappingSZforward.png)
##### ![FdA/images/Zforward.png](FdA/images/Zforward.png)
### le approsimazioni backward e forward sono simili nel passaggio al piano Z, invece l'approsimazione bilineare, o Tustin, pur avendo alcune aree della circonferenza unitaria, compresse, soffre in maniera minore dei problemi riscontrati precedentemente
#### ![FdA/images/Ztustin.png](FdA/images/Ztustin.png)
### per ovviare al problema delle compressioni dell'approssimazione Tustin si può effettuare un'operazione di pre-warping, ovvero  espandendo uno o più poli __alla stessa frequenza__ con un'altra trasformazione bilineare calcolata in funzione dei poli, e quindi successivamente applicando Tustin otterremo il polo o i poli a quella specifica frequenza nella posizione voluta. __L'espansione mi interessa solo in poli in cui voglio conservare una pulsazione.__

## Conclusioni
### è da specificare, che non esiste una trasformazione universalemente giusta, ma sono tutte approssimazioni diverse che possono essere utili in circostanze variegate
### in base alla trasformazione con margine di fase, più simile all'originario in $S$, si sceglie la trasformazione più congrua
### al diminuire del tempo di campionamento, anche la trasformazione più stupida potrebbe cambiare in meglio, poiché si starebbe aumentando la risoluzione del campionamento e una trasformazione più rapida in alcuni casi potrebbe essere più semplice da implementare.
