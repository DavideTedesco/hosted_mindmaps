---
markmap:
  maxWidth: 400
---

# Struttura di un sistema di controllo a segnali campionati

## Microcontrollore
### microprocessore
#### in ingresso ed uscita dal microprocessore, vi sono dei BUS a $n$ bit per poter trasportare i segnali digitale dall'ADC al microprocessore e dal microprocessore al DAC
### DAC
#### Digital to Analog Converter
#### convertitore da digitale ad analogico, ovvero da dominio discreto a dominio continuo
#### per effettuare una discretizzazione del segnale effettuiamo la quantizzazione dello stesso, che sarà rappresentato con un gradino, per ogni passo di quantizzazione
##### Sotto il passo di quantizzazione a livello digitale saremo quindi ciechi.
#### alcuni convertitori converto correnti, mentre altri convertono tensioni
### ADC
#### Analog to Digital Converter
#### convertitore da analogico a digitale, ovvero da dominio continuo a dominio discreto
#### esso ha esattamente lo stesso funzionamento dell'DAC ma all'inverso
### segnali SOC ed EOC
#### essi sono dei segnali di controllo necessari per iniziare e terminare la conversione: Start Of Conversion e End Of Conversion
#### Non esiste quindi una conversione continua ed abbiamo bisogno dello start e dell'end per portare in maniera sicura il dato dentro e fuori dal Microcontrollore, in genere realizzato per mezzo di interrupt


## Circuito di adattamento
### utile per adattare il segnale in ingresso ed uscita dai convertitori, per poter riscalare opportunamente il segnale
### esso nello schema è rappresentato da un cerchio con la "a"

## Trasduttore dell'uscita
### amplificatore di potenza in uscita dal Microcontrollore
### un amplificatore di potenza è in grado di amplificare una tensione in ingresso per ottenere una corrente in uscita molto superiore a quella in ingresso
### esso nello schema è rappresentato da un triangolo con la "A"

## Processo
### esso nello schema è rappresentato da un rettangolo con la "P"

## Sensore al cui è applicato il rumore $z$
### esso nello schema è rappresentato da un cerchio vuoto con una freccia del rumore $z$ applicata ad esso

## Filtro anti-aliasing
### esso è un particolare filtro passa-basso utile al processamento di segnali discreti (da analogico a digitale e viceversa)
### esso nello schema è rappresentato da un rettangolo con il disegno di un filtro passa basso al suo interno
