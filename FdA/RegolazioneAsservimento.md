---
markmap:
  maxWidth: 400
---

# Regolazione e Asservimento

## Introduzione
### sono in genere sistemi a controreazione e sono in grado di effetturare
#### Regolazione
#### Asservimento

## Che cos'è la regolazione
### avendo una grandezza di riferimento costante
#### il controllore cerca di portare il sistema a tale grandezza
#### ![FdA/images/regolazione.png](FdA/images/regolazione.png)
## Che cos'è l'asservimento
### avendo una grandezza di riferimento che varia nel tempo
#### il controllore tenderà a forzare l'uscita del sistema verso la grandezza di riferimento variabile nel tempo, assumendo un comportamento proporzionale al riferimento
#### ![FdA/images/asservimento.png](FdA/images/asservimento.png)
