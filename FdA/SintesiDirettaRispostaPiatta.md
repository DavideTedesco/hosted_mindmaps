---
markmap:
  maxWidth: 400
---



# Sintesi diretta della risposta piatta

## Introduzione
### Sapendo come si possano trasformare e portare campionandolo un sistema nel dominio discreto
### andiamo a osservare come si realizza il controllo di sistemi discreti tramite la sintesi diretta
### quindi con un sistema a segnali campionati è possibile avere un controllore che ci assicura un errore nullo se accettiamo un ritardo abbastanza grande.

## Il sistema in esame
### partendo dal sistema nel continuo:
#### ![FdA/images/SintesiDirettaContinuo.png](FdA/images/SintesiDirettaContinuo.png)
### arriviamo a realizzare un sistema discreto di cui dobbiamo comprendere come realizzare il controllore:
#### ![FdA/images/SintesiDirettaDiscreto.png](FdA/images/SintesiDirettaDiscreto.png)
### Questo sistema ha una funzione di trasferimento pari a:
#### $W(z)=\frac{CP}{1+CP}$


## La sintesi
### osservato il sistema, vediamo che possiamo esprimere la funzione di trasferimento a ciclo chiuso come:
#### $U(z)W(z)=Y(z) \space \Rightarrow \space W(z)=\frac{Y(z)}{U(z)}=\frac{CP}{1+CP}=z^{-n}$
#### in cui vediamo che il rapporto tra catena diretta e catena di controreazione è pari a $z^{-n}$ ovvero a un qualcosa ritardato di $n$ campioni
### d'ora in poi cerchiamo di capire come poter desumere il controllore da ciò appena visto nel dominio discreto:
#### $CP=z^{-n}+CPz^{-n} \space\Rightarrow\space C(P-z^{-n}P)=z^{-n}$
#### $C=\frac1{z^{n}} \frac{1}{P-Pz^{-n}}=\frac{1}{Pz^n-P}$
#### e considerando $P=\frac {N_P} {D_P}$ otterremo
#### $C(z)=\frac {D_P}{N_Pz^n-N_P}$
#### in cui vediamo che al denominatore abbiamo la differenza tra il numeratore del processo iniziale ritardato di n campioni a cui viene sottratto il numeratore stesso
### l’unica condizione è che siamo disposti ad accettare un ritardo in tempi di campionamento $n>\text{grado}(D_P)-\text{grado}(N_P)$ (altrimenti il controllore non sarebbe causale, e quindi non realizzabile).

## La risposta piatta
### quindi attendendo un numero $n$ di campioni si potrà ottenere un sistema che ha uscita pari all'ingresso, quindi errore nullo
### bisogna stare attenti al non cancellare dinamiche instabili
### esso si chiama a risposta piatta, perchè comunque lo metto, se lo realizzo nel modo descritto qui ci farà ottenere una risposta piatta, ovvero in un campione (o dopo una manciata di campioni) arriva dove deve arrivare e basta con errore nullo, mentre nei sistemi continui, il sistema  si adatta come una curva per raggiungere il regime permanente
