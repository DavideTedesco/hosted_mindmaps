---
markmap:
  maxWidth: 400
---

# Carta di Nichols

## Introduzione
### in tale carta, riportiamo la fase sulle ascisse ed il modulo sulle ordinate
### il punto centrale è $(0 \text{dB}, -180°)$
### tale carta è utile per poter legare fra loro i parametri che descrivono un sistema di controllo
### sulla carta sono riportate curve a modulo costante
#### Fissando $0 \ dB$ come modulo e variando la fase, otteniamo una serie di punti interpolabili attraverso una curva, così è costruita una curva a modulo costante
### vi sono anche curve a fase costante
#### tali curve si ottengono fissando la fase e variando il modulo
### ![FdA/images/nichols.png](FdA/images/nichols.png)

## Dalla funzione a ciclo chiuso alla sua riscrittura a ciclo aperto
### osservando una funzione a ciclo chiuso, possiamo con la Carta di Nichols, ottenere
### data una funzione a ciclo chiuso $\displaystyle W(s) = \frac {F(s)} {1+F(s)}$
### la rispettiva funzione a ciclo aperto $\displaystyle F = \frac {W(s)} {1-W(s)}$

## Come si ottiene la carta di Nichols
### la curva di $F(s)$, intersecata con le quelle a fase e modulo costante, permetteranno di comprendere i valori dei principali parametri utili

## Parametri osservabili grazie alla carta di Nichols:
### ![FdA/images/parametriNichols.png](FdA/images/parametriNichols.png)
### curva di $\blue {F(s)}$
### Margine di guadagno $\purple {m_g}$
#### distanza verticale dal punto centrale alla linea degli 0 dB
### Margine di fase $\red { m_{\varphi}}$
#### distanza orizzontale dal punto centrale alla linea degli 0dB
### Modulo alla risonanza $\green {M_r}$
#### osservando il cerchio interno tangente al grafico, si ottiene un valore corrispondente al modulo alla risonanza
### Banda passante $\orange {\omega_{-3}}$
#### osservando il valore della curva dei -3 dB, nel quale la curva interseca il grafico della funzione

## A cosa è utile tale carta
### essa è utile per poter comprendere andamento di modulo e fase rispetto ai parametri descritti
