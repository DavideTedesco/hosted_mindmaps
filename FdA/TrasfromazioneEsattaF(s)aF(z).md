# Trasformazione esatta da F(s) ad F(z)

## Introduzione
### ipotizzando di avere un sistema a controreazione con un controllore $C(s)$ e un processo $P(s)$
#### ![FdA/images/sistemacontrolloreprocesso.png](FdA/images/sistemacontrolloreprocesso.png)
### vogliamo portare il controllore nel dominio $z$, ovvero discretizzarlo per poterlo utilizzare su un calcolatore

## Campionamento del segnale
### per poter raggiungere tale scopo sappiamo che il controllore in $s$ è pari a $C(s)=\frac {1} {s-p}$
### quindi andandolo ad anti-trasformare nel tempo otterremo $c(t)=e^{pt}$
### ora campioniamo tale controllore nel tempo ed otteniamo:
#### $c_c(t) = \sum_{k=0}^\infty e^{pkT_c}\cdot \delta(t-kT_c)$

## Trasformazione esatta
### ![FdA/images/trasformazioni.png](FdA/images/trasformazioni.png)
### dopo averlo campionato e sapendo che $\delta(t-kT_c)=z^{-k}$
#### otterremo  $C(Z) = \sum_{k=0}^\infty e^{pkT_c}\cdot z^{-k}$
##### che possiamo riscrivere  $C(Z) = \sum_{k=0}^\infty (e^{pT_c}\cdot z^{-1})^k$
##### sapendo essa è una serie geometrica, otteremo $C(Z)=\frac {1} {1-e^{pT_c}z^{-1}}=\frac {z} {z-e^{pT_C}}$ che sappiamo convergere per  $|z|>e^{pT_c}$
##### quella qui ottenuta è la __trasformazione esatta__ dal dominio del tempo al dominio $z$
### è possibile passare direttamente al dominio $z$ applicando il metodo dei poli e residui
#### ![FdA/images/zetapoliresidui.png](FdA/images/zetapoliresidui.png)

## Calcolo dei guadagni
### tale trasformazione non viene di fatto utilizzata perchè non permette di mantenere un guadagno simile al sistema di partenza
### andando a calcolare i guadagni del controllore in $s$ e di quello in $z$ per mezzo del teorema del valor finale con ingresso gradino unitario ($\frac 1 s$ in Laplace e $\frac 1 {1-z^{-1}}$ in $z$), ci rendiamo subito conto della differenza:
#### $\displaystyle k_{C{s}}\lim_{s\rightarrow 0} s\cdot \frac {1} {s+p} \cdot \frac 1 s = \frac 1 p$
#### $\displaystyle k_{C{z}}\lim_{z\rightarrow 1}  (1-z^{-1})\cdot \frac {z}{z-e^{pT_c}} \cdot \frac {1} {1-z^{-1}} = \frac 1  {1-e^{pT_c}}$
### utilizzando invece poli e residui per calcolare il guadagno avremo
####  ![FdA/images/zetaguadagnopoliresidui.png](FdA/images/zetaguadagnopoliresidui.png)
##### in cui vediamo che abbiamo posto $z=0$


## Considerazioni finali
### Abbiamo quindi che la risposta al gradino cambia, perché il guadagno lo calcoliamo per la risposta al gradino e non quella all'impulso per cui essa è esatta.
### Quindi tale trasformazione non andrà bene poiché riporta un guadagno diverso da quello che ci staremmo aspettando e tale guadagno dipende dal tempo di campionamento scelto $T_c$
