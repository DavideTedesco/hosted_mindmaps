---
markmap:
  maxWidth: 550
---

# Principio del modello interno

## Introduzione
### per poter ottenere un sistema che al regime permanente abbia errore pressochè nullo, sarà necessario porre degli accorgimenti che facciano in modo da minimizzare tale errore
### vedremo di seguito che per fare ciò viene applicato il __principio del modello interno__  

## Sistema in esame
### dato un sistema descritto a blocchi come:
#### ![FdA/images/sistemaPMI.png](FdA/images/sistemaPMI.png)
### e volendo una $W(s)=Kd$ derivata da $Y_d=K_d\cdot u$ ricordando che $W(s)=\frac {Y(s)} {U(s)}$
### vediamo come potremo scrivere l'errore a regime permanente $e=u-\frac y {K_d}\\e=Kd\cdot u - y$
#### ![FdA/images/sistemaPMIErrore.png](FdA/images/sistemaPMIErrore.png)

## Errore al regime permanente senza l'applicazione del modello interno
### avremo quindi come errore al regime permanente in Laplace
#### $E(s)=KdU(s)-W(s)U(s)=\big(Kd-\frac {G(s)}{\frac {1+G(s)}{Kd}}\big)\cdot U(s) = (Kd-\frac {Kd G(s)}{Kd+G(s)})\cdot U(s) = \frac {Kd^2}{Kd+G(s)}\cdot U(s)$
#### l'errore è pari al comportamento ideale meno il comportamento reale

## Esplicazione degli integratori
### Ora per ciò detto precedentemente, ovvero ricordando il principio del modello interno, osserviamo che ciò che inseriamo in ingresso deve essere presente anche dentro il sistema.
### sarà quindi necessario sapere la tipologia di segnale da far entrare nel sistema, per poter inserire un numero di integratori pari a far tendere l'errore a regime a zero
### quindi osservando un esempio in cui valutiamo l'errore per $U(s)=\frac 1 {s^{k+1}}$ vediamo che l'errore in Laplace sarà pari a $E(s)_{esempio}=\frac {Kd^2}{Kd+G(s)}\cdot \frac 1 {s^{k+1}}$

## Calcolo dell'errore al regime e applicazione del Teorema del valor finale dopo l'applicazione del principio del modello interno
### avendo quindi osservato l'errore in Laplace, applichiamo il teorema del valor finale, e osserviamo cosa accada:
### $\lim_{s\rightarrow 0 }s \cdot \frac {Kd^2}{Kd+G(s)}\cdot \frac 1 {s^{\red{k}+1}}$
### in essa si semplifica la $s$ iniziale con quella al denominatore e osserviamo che vi saranno $k$ ostacoli che non permetteranno di inseguire il segnale in ingresso
### per far si che sia possibile confrontare il segnale in ingresso con il sistema, esplicitiamo il numero di integratori presenti già all'interno del processo $G(s)=\frac {G'(s)} {s^{\green h}}$

## Richiami alla tabellina della classificazione di tipi e reiezione dei disturbi
### avendo esplicitato il numero di integratori presenti in $G(s)$ che sono pari ad $h$, possiamo ora comprendere come per inseguire un segnale in ingresso con grado $k$, dovremo avere una $h$ confrontabile con esso
### e quindi riscrevendo l'equazione dell'errore in Laplace, avremo:
#### $E(s)=\frac {s^hKd^2}{s^hK+G'(s)}\cdot \frac 1 {s^{k+1}}$
#### andando ora a riapplicare il teorema del valor finale, otterremo
#### $\lim_{s\rightarrow 0 }s \cdot \frac {s^hKd^2}{s^hK+G'(s)}\cdot \frac 1 {s^{k+1}}=\lim_{s\rightarrow 0 }s \cdot \frac {Kd^2}{s^hK+G'(s)}\cdot \frac 1 {s^{k-h}}$
### quindi vediamo che in caso siano $h>k$ l'errore sarà nullo
### ma in caso $h=k$ possiamo affermare di aver inserito $h$ integratori tali da poter rendere costante l'errore del sistema, infatti per $h=k \rightarrow E(s)=\frac {Kd^2}{s^hK+G'(s)}$
#### nel caso contrario in cui $h<k$ l'errore andrà ad infinito

## Conclusioni
### quindi per il principio del modello interno, per ottenere errore nullo in uscita, bisognerà avere una dinamica all'interno del sistema, simile alla dinamica che viene applicata in ingresso
#### per inseguire una dinamica di un certo tipo $k$, avremo bisogno della stessa tipologia di dinamica all'interno del sistema compensata tramite $h$ integratori
#### come descritto dalla tabellina delle classificazioni in tipo:
##### ![FdA/images/tabellinaTipi.png](FdA/images/tabellinaTipi.png)
