---
markmap:
  maxWidth: 500
---

# Risposta armonica e sue rappresentazioni grafiche (Nyquist, Bode)

## Introduzione
### cosa è importante sapere
#### poniamo in ingresso ad un sistema lineare un ingresso sinusoidale (con forma a seno o coseno)
#### con sistemi stabili
#### sapendo come siano descritti i sistemi in grado di reiettare i disturbi
### un sistema con ingresso sinusoidale
#### ![FdA/images/sistsin.png](FdA/images/sistsin.png)
##### ponendo l'ipotesi che $G(s)$ sia asintoticamente stabile

#### sapendo che il segnale nel tempo è $u(t)=\sin(\omega t)$
#### trasformandolo con Laplace diverrà $\frac{\omega}{s^2+\omega^2}$

## Come verrà modificato all'uscita del sistema il segnale sinusoidale inserito in ingresso?
### sappiamo che dato che il sistema è asintoticamente stabile, allora sicuramente dall'uscita avremo un segnale sinusoidale
### dovremo capire come vengano modificati modulo e fase
### scomponiamo con poli e residui il polinomio:
#### $Y(s)=G(s)\cdot U(s)=G(s)\cdot\frac{\omega}{s^2+\omega^2}=\frac{R}{s-j\omega}+\frac{R^*}{s+j\omega}\space+\space\sum\frac{R_i}{s-p_i}$
##### Ipotizziamo che $G(s)$ sia stabile e che quindi le sue componenti esponenziali (i residui con $p_i$) vadano a 0 (la componente transitoria). Quello che ci rimane sono i residui dovuti all’ingresso sinusoidale, pertanto avremo un segnale sinusoidale in uscita
### dopo aver scomposto in poli e residui, ne calcoliamo i singoli valori che osserviamo essere complessi e coniugati
#### $R_{j\omega}=\lim_{s\rightarrow j\omega}(s-j\omega)Y(s)=\lim_{s\rightarrow j\omega}(s-j\omega)G(s)\frac{\omega}{(s+j\omega)(s-j\omega)}=\frac{G(j\omega)}{2j}$
##### e ne consideriamo anche il suo valore complesso e coniugato
### dal passaggio precedente otteniamo l'uscita del sistema antitrasformando le sole dinamiche del regime permanente
### $\mathscr{L}_{-}^{-1}\{\frac{R}{s-p}\}=Re^{pt}$ e il suo complesso e coniugato
### ottenendo da ultimo $y_p(t)=\frac{|G(j\omega)|e^{j\phi}}{2j}e^{j\omega t}-\frac{|G(j\omega)|e^{-j\phi}}{2j}e^{-j\omega t}=|G(j\omega)|\sin(\omega t+\phi)$
### osserviamo quindi nell'uscita finale del sistema che fase e modulo dipendono dalla funzione $G(j\omega)$ e possiamo usare strumenti come Bode o Nyquist per studiare il comportamento dell'uscita

## Diagrammi
### Bode
#### diagramma che riporta due grafici di modulo e fase
#### diagramma che sull'ascissa di entrambi i grafici riporta la la pulsazione naturale $\omega$
#### il primo grafico rappresenta il modulo
#### il secondo grafico rappresenta la fase
#### tracciato su carta logaritmica
### Nyquist
#### grafico che presenta assi reale e immaginario
#### esso è utile per rappresentare congiuntamente modulo e fase
##### si utilizza per il tracciamento un vettore con lunghezza pari al modulo e angolo pari alla fase
#### sfrutta il Teorema dell'indicatore logaritmico
#### sfrutta il __percorso di Nyquist__
#### tale diagramma serve per poter osservare quante volte al funzione giri attorno al punto -1, utile a definire se il sistema sia stabile o meno
##### in caso il numero i rotazioni attorno a -1 sia pari al numero dei poli a parte reale positiva di $F(s)$ allora il sistema sarà stabile
#### esempio di diagramma di Nyquist: ![FdA/images/dianyquist.png](FdA/images/dianyquist.png)
