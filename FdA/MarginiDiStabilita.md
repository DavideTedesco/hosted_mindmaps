---
markmap:
  maxWidth: 400
---


# Margini di stabilità (guadagno e fase)

## Introduzione
### tali paramateri sono utili a comprendere la stabilità di un sistema
### dato che le due proprietà fondamentali di un sistema sono modulo e fase, useremo esse per misurare la stabilità
### sapendo che il sistema in esame sia asintoticamente stabile (secondo il Criterio di stabilità di Nyquist) e le radici siano tutte a parte reale negativa
### tali margini, sono calcolabili accuratamente solo sul diagramma di Nyquist, solo in maniera semplificata possono essere desunti dal diagramma di Bode

## Margine di guadagno
### esso risponde alla domanda: di quanto possiamo amplificare il sistema finché esso rimanga stabile?
### esso è calcolabile per mezzo dell'inverso del punto $\alpha$ che identifica l'intersezione delle curve del sistema con con l'asse delle ascisse: $m_g = \frac 1 \alpha = \frac 1 {0.02} = 50$
#### per un sistema riportante questa funzione di trasferimento: $F(s) = \frac 1 {s\cdot (s+1)\cdot (\frac {s} {10}+1)}$
#### con questo diagramma (non in scala) di Nyquist
##### ![FdA/images/margineGuadagno.png](FdA/images/margineGuadagno.png)

## Margine di fase
###  tale parametro risponde alla domanda: di quanto posso al massimo sfasare il ciclo aperto prima che il sistema diventi instabile?
### Tracciando la circonferenza di raggio unitario (in grigio e che esprime il limite minimo di stabilità con il punto -1) e tirando un segmento verso la circonferenza unitaria (in verde nell’immagine) laddove si interseca con la curva del sistema, si forma un angolo (in rosso nell’immagine) che chiameremo margine di fase.
###  ![FdA/images/margineFase.png](FdA/images/margineFase.png)

## Margini di stabilità su Bode
### visto che la definizione di tali margini è espressa per mezzo del diagramma di Nyquist, possiamo ora comprendere come sia possibile ricavarli (nei casi semplici) dal diagramma di Bode:
#### il margine di guadagno (sempre del sistema in esame) è osservabile andando a vedere il grafico del guadagno in corrispondenza del punto in cui la fase taglia i -180º
##### il margine di guadagno sarà la distanza tra l'asse orizzontale degli 0dB ed il punto raggiunto
#### il margine di fase è osservabile laddove viene individuato l'omega di taglio, ovvero andando a vedere la differenza tra -180º e la fase della curva del sistema, in corrispondenza del punto in cui la curva del guadagno taglia 0dB
###  ![FdA/images/marginiBode.png](FdA/images/marginiBode.png)

## Conclusioni
### I margini di stabilità non possono essere calcolati sul diagramma di Bode, in tutti i casi in cui la fase ha comportamenti strani o il modulo ha comportamenti strani.
### maggiore sarà il margine di guadagno, migliore sarà la stabilità del sistema
### Margini di fase tipici sono 50º,60º,70º, con margini più piccoli avremo oscillazioni spiacevoli, mentre con margini più grandi, la stabilità sarà garantita.
### Maggiori sono i margini di stabilità, più il sistema sarà stabile (anche in presenza di disturbi o variazioni parametriche).
