---
markmap:
  maxWidth: 400
---

# Risposta libera e risposta forzata

## Introduzione
### Tali termini sono definiti osservando una generica equazione differenziale lineare, ordinaria, stazionaria e a coefficienti costanti
### Cos'è la risposta libera
#### Risposta del sistema dipendente dal solo sistema
### Cos'è la risposta forzata
#### Risposta del sistema dipendente dall'ingresso e dal sistema stesso

## Sapendo che la trasformata di Laplace unilatera destra della derivata è pari a
### $\displaystyle L_-\{\frac{d^n}{dt^n}y(t)\}=s^nY(S)-\sum_{k=0}^{n-1}s^{n-k-1}\frac{d^k}{dt^k}Y(0^-) $

## Osservando che avendo un'equazione differenziale del tipo prima proposto
### $ a_0y(t)+a_1\dot{y}(t)+a_2\ddot{y}(t)=b_0u(t)+b_1\dot{u}(t) $
#### trasformandola con Laplace otteniamo $\displaystyle Y(S)=\frac{b_1s+b_0}{a_2s^2+a_1s+a_0}\cdot U(S)-\frac{I_{y^1}(S)}{a_2s^2+a_1s+a_0}+\frac{I_{u^0}(S)}{a_2s^2+a_1s+a_0} $
##### che potremo semplificare in questa $Y(S)=\frac{b_1s+b_0}{a_2s^2+a_1s+a_0}\cdot U(S)-\frac{I_{y^1}(S)}{a_2s^2+a_1s+a_0}$ dato che non ci interessa l'ultimo termine della trasformata precedente perché data l'ipotesi di causalità, sappiamo che il sistema valga 0 in 0 essa vediamo essere della forma $Y(S)= Y_f(S)-Y_l(S)$

## È possibile dedurre un tipo di uscita
### osservando che il segnale in uscita dal sistema è suddivisibile in due sezioni
#### risposta libera
#### risposta forzata
### $Y(S)= Y_f(S)-Y_l(S)=F(S)\cdot U(S) - Y_l(S)$
#### osserviamo che la $Y(S)=\text{risposta forzata}-\text{risposta libera}$
#### risposta libera $Y_l(S)=\frac{I_{y^1}(S)}{a_2s^2+a_1s+a_0}$ (notare che la parte finale fa riferimento al sistema d'esempio)
#### risposta forzata $Y_f(S)=F(S)\cdot U(S)=\frac{b_1s+b_0}{a_2s^2+a_1s+a_0}\cdot U(S)$ (notare che la parte finale fa riferimento al sistema d'esempio)
### come scritte nell'esempio, entrambe le risposte (forzata e libera) hanno un denominatore in comune chiamato _polinomio caratteristico_ che consente di dedurre l'andamento dell'uscita del sistema
#### con solamente poli a parte reale negativa l'uscita del sistema sarà pari a $Y(S)= F(S)\cdot U(S)$ da cui deriva la [Funzione di Trasferimento](https://davidetedesco.gitlab.io/hosted_mindmaps/mindmap.html?md=FunzioneTrasferimento.md)
