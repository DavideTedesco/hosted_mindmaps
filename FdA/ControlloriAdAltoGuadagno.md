---
markmap:
  maxWidth: 1000
---

# Controllori ad alto guadagno

## Introduzione
### di seguito sono espresse alcune modalità con cui un controllore ad alto guadagno può alterare la risposta di un sistema positivamente e negativamente

## Osservando un amplificatore operazionale per realizzare un controllore di un sistema, osserviamo che per un guadagno del controllore molto elevato esso inizia a comportarsi in maniera inversa al suo funzionamento
### possiamo dimostrare ciò in due modi
#### osservando un circuito e il suo schema a blocchi vediamo come il flusso della corrente cambi di verso per elevati guadagni
##### ![FdA/images/ControlloreAltoGuadagnoPartitore.png](FdA/images/ControlloreAltoGuadagnoPartitore.png)
##### osservando tale schema a blocchi comprendiamo che la funzione a ciclo chiuso sia pari a $\frac {RCS} {1+RCS}$ mentre per elevati guadagni diviene $\frac {1+RCS} {RCS}$ facendo quindi invertire il funzionamento e essendo inizialmente un integratore, diviene un derivatore
#### scrivendo la funzione a ciclo chiuso e facendo tendere il guadagno $K_a$ a infinito, otterremo l'analoga risposta:
##### $W(s) = \frac  {K_a} {1\cdot \frac {K\cdot RCS}{RCS+1}}=\frac {K\cdot (RCS+1)}{(RCS+1)\cdot(K_aRCS)}$
### sfruttiamo in genere un operazionale perchè permette di ottenere un elevato guadagno


## Errore di riproduzione di segnali di tipo $k$
### un controllore con un alto guadagno e $h$ integratori al crescere del guadagno fa diminuire l'errore finito al permanente

## Reiezione dei disturbi dei segnali di tipo $k$
### un elevato guadagno può aiutare a diminuire i disturbi all'interno di un sistema, permettendo di ottenere un modulo maggiore per la sola parte che interessa nello studio dei sistemi

## Diminuzione della sensibilità in catena diretta del sistema
### tramite un controllore con un elevato guadagno sulla catena diretta del sistema, andando a misurare la sensibilità diretta dello stesso
### si potrà osservare una completa diminuzione dei disturbi, dato che essi tenderanno a 0 poiché il sistema diviene moltiplicato per $S^W_G$ ovvero la sensibilità diretta, che per definizione tende a zero per guadagni elevati
#### calcolo della sensibilità in catena diretta $S^W_G=\frac 1 {1+G(s)\cdot H(s)}$
#### attenuazione dei disturbi in catena diretta tramite la sensibilità diretta $Y(s)_{z_1}=\frac{G_2(s)}{1+G_1(s)G_2(s)H(s)}z_1(s)=S^W_G\cdot G_2(s)z_1(s)$

## Nei sistemi non lineari: un controllore ad alto guadagno permette la linearizzazione della controreazione
### perchè gli elementi moltiplicati per $k$ per elevati guadagni fanno tendere tali termini a zero, riuscendo quindi a linearizzare il sistema per via della controreazione
#### ![FdA/images/linearizzazioneAltoGuadagno.png](FdA/images/linearizzazioneAltoGuadagno.png)
### i componenti fisici atti alla linearizzazione (ed utilizzo con alto guadango) sono in genere:
#### amplificatori operazionali
#### transistor

## Nei sistemi a fase non minima: un elevato guadagno rende instabile il ciclo chiuso
### per elevati guadagni del controllore, il numeratore della funzione a ciclo chiuso tende ad infinito, farà quindi divenire instabile il sistema
### $W(s)=\frac{K_c\cdot N(s)}{D(s)+K_c\cdot N(s)}$

## Come il variare del guadagno (e quindi anche avendo un guadagno elevato) faccia variare il comportamento di anello chiuso ed aperto
### influenza sui margini di stabilità
### sapendo che la funzione d'anello a ciclo chiuso è $W(s)=\frac{K_dF(s)}{1+K_dF(s)}$
#### è facile osservare come il guadagno (e quindi un controllore con alto guadagno) faccia variare il comportamento del sistema
### ad esempio andando a osservare la risposta la gradino di un tale sistema, aumentando il guadagno del controllore:
#### il rise time diminuisce
#### la sovraelongazione aumenta
#### il tempo di assestamento potrebbe aumentare

## Riepilogo sui controllori ad alto guadagno di anello, esso:
### un amplificatore operazionale integratore ad elevati guadagni, permette di ottenere un effetto di derivazione
#### ciò ha anche influenza sulla banda passante quando si inserisce un operazionale con la controreazione in pratica la controreazione riesce a barattare guadagno per banda passante. Ovvero seppure diminuisce il guadagno iniziale della $W(s)$, avremo però una banda passante più estesa.
###  permette di diminuire l'errore di riproduzione dei segnali di tipo $k$
### permette di rigettare i disturbi di tipo $k$
### permette di diminuire la sensibilità in catena diretta del sistema (lo fa tendere a 0 e fa tendere ad 1 la sensibilità complementare)
### permette la linearizzazione della controreazione
#### in generale dove si può aumentare il guadagno e mantenere la stabilità, si ha la possibilità di linearizzare i segnali. Essa è chiamata linearizzazione tramite la controreazione.
#### in pratica un sistema con alto guadagno di anello permette di linearizzare un sistema e quindi di:
##### riprodurre un segnale di tipo $k$
##### reiettare un disturbo di tipo $k$
##### diminuire la sensibilità diretta
##### diminuire il tempo di salita
##### linearizzare infine il sistema
### permette di variare il sistema di controllo nell'anello chiuso ed aperto
