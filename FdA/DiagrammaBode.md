---
markmap:
  maxWidth: 600
---

# Diagramma Bode

## Introduzione
### Costituito da due diagrammi distinti che riportano entrambi sull'asse delle ascisse la pulsazione $\omega$, in particolare essi permettono di osservare:
#### andamento del modulo, in dB sull'asse delle ordinate
#### andamento della fase, in gradi sull'asse delle ordinate
### equazione generale per il tracciamento del diagramma di Bode $G(s)=K_gs^h\frac{(\frac{s^2}{\omega^2_{r_i}}+\frac{2{\zeta_i}}{\omega_{r_i}}s+1)(sr_i+1)}{(\frac{s^2}{\omega^2_{r_j}}+\frac{2{\zeta_j}}{\omega_{r_j}}s+1)(sr_j+1)}$
### Per capire i contributi di ogni termine basta porre $s=j\omega$ e vedere come cambia il contributo col variare di $\omega$
### esempio di diagramma di Bode ![FdA/images/bode.png](FdA/images/bode.png)
### in genere viene riportato sopra il modulo e sotta la fase
### l'asse delle ascisse è espresso in scala logaritimica
### la tipologia di tracciamento utilizzato è un tracciamento asintotico, ciò significa che il diagramma è molto preciso agli estremi e meno sui valori intermedi

## [Esempio di diagramma di Bode per una funzione di trasferimento non banale](https://www.notion.so/Fondamenti-di-Automatica-8eb190133f0e48c1a7de6bdcf03d02ea?pvs=4#0b02045105f640bda95e8d61137d6010)

## Alcuni andamenti tipici
### in genere i poli fanno abbassare il modulo e la variazione parte dalla posizione polo
#### poli a parte reale positiva alzano la fase e abbassano il modulo
##### $\frac {1} {s-1}$
##### ![FdA/images/pprp.png](FdA/images/pprp.png)
##### in genere un polo a parte reale positiva alza la fase di 90º
#### poli a parte reale negativa abbassano la fase e abbassano il modulo
##### $\frac {1} {s+1}$
##### ![FdA/images/pprn.png](FdA/images/pprn.png)
##### in genere un polo a parte reale negativa abbassa la fase di 90º
### in genere gli zeri fanno alzare il modulo e la variazione parte dalla posizione dello zero
#### zeri a parte reale positiva abbassano la fase e alzano il modulo
##### ${s-1}$
##### ![FdA/images/zprp.png](FdA/images/zprp.png)
##### in genere uno zero a parte reale positiva abbassa la fase di 90º
#### zeri a parte reale negativa alza la fase e alzano il modulo
##### ${s+1}$
##### ![FdA/images/zprn.png](FdA/images/zprn.png)
##### in genere uno zero a parte reale negativa alza la fase di 90º

## Termine trinomio
### abbiamo trattato termini
#### monomi $K_G$ oppure $s^h$
#### binomi $sr+1$
##### il punto di rottura per tale termine è dato da $\frac 1 r$ tale punto indica da quale momento in poi il binomio ha effetto sul diagramma
### in questa sezione osserviamo invece il termine trinomio $\frac{s^2}{\omega^2_{n_i}}+\frac{2{\zeta_i}}{\omega_{n_i}}s+1$
### il punto di rottura per il termine trinomio è dato da $\omega_n$ che è anche la pulsazione naturale
### anche per questo il comportamento asintotico è di facile tracciamento, infatti:
#### seguendo gli andamenti tipici i comportamenti rimangono invariati ma cambia la pendenza degli stessi, prima avevamo per ogni polo o zero 20 dB per decade, mentre ora avremo 40 dB per decade
### nel punto di rottura, avremo invece quello che viene chiamo punto di risonanza per valori piccolo di $\zeta$ o punto di anti-risonanza per valori grandi di $\zeta$
#### con il termine risonanza, per il diagramma di Bode del modulo osserviamo delle gobbe molto pronunciate in corrispondenza di $\omega_n$ esse descrivo il fatto che su quello specifico intorno vi è un'amplificazione (risonanza) o un'attenuazione (anti-risonanza) dei dB indicati
#### la fase del termine triniomio sale di +180º
