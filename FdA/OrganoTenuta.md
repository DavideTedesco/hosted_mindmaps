---
markmap:
  maxWidth: 500
---

# Organo di Tenuta

## Introduzione
### cosa è?
#### esso è un ricostruttore di segnale reale, non solamente ideale come quello di Shannon
### a cosa serve?
#### per poter rendere continuo un segnale precedentemente campionato
### come funziona?
#### il suo funzionamento si basa sul mantenere fino al campione successivo ($k+1$) il livello del campione $k$
##### ![FdA/images/zohcampionamento.png](FdA/images/zohcampionamento.png)

## Funzione di Trasferimento dell'Organo di Tenuta
### osserviamo di seguito che possiamo rappresentare ogni singolo campione e la sua parte di _hold_ come un gradino all'istante $kT_c$ sottratto ad un altro gradino all'istante $(1+k)Tc$
#### ottenendo da ultimo la Rect raffigurante il campione e l'hold della sua ampiezza per un singolo istante di campionamento
### dall'osservazione precedente potremo scrivere il segnale "tenuto" di tutti i campioni come la sommatoria di Rect così descritta:
#### $y(t)=\sum_{k=0}^{+\infty} x_k\delta_{-1}(t-kT_c)-x_k\delta_{-1}(t-(k+1)T_c)$
##### trasformiamo il Laplace $Y(s)=\frac1s\sum_{k=0}^{+\infty}X_ke^{-kT_cs}(1-e^{-T_cs}) = \sum_{k=0}^{+\infty}X_ke^{-kT_cs}\space\cdot\space(\frac{1-e^{-T_cs}}s)$
###### ricordiamo che per le approssimazioni di Taylor e MacLaurin $e^x\approx 1+x$ quindi $e^{T_cs}=\approx 1+T_cS$ utile per riscrivere $T(s)=\frac{1-{\frac {1} {e^{T_cs}}}} {s}= \frac {T_c} {1+T_cs}$
###### in cui vediamo che a sinistra vi è il segnale campionato in Laplace
##### ricordiamo che $\displaystyle \sum_{k=0}^{+\infty}X_ke^{-kT_cs}$ indica la trasformata di Laplace di un segnale $x$ campionato
### trasformando in Laplace e andando a cercare di separare il segnale in ingresso ed il resto otterremo la funzione di trasferimento dello ZOH (a destra della trasformata finale di Laplace di $Y(s)$), essa la chiameremo $T(s)$
#### $T(s)=\frac{1-e^{-T_cs}}s$
### riconosciamo in tale funzione di trasferimento un singolo polo in $\frac 1 {T_c}$
### il guadagno di tale funzione di trasferimento è pari a $T_c$ ovvero proprio l'inverso di quello utilizzando durante il campionamento

## Approssimazione a basse frequenze e problemi dell'Organo di Tenuta
### sapendo che la frequenza in cui il filtro ideale utilizzato da Shannon è pari a $\omega_{m_{Shannon}} = \frac {\omega_c} {2}$ in cui $\omega_m$ è definita frequenza massima
### allora  effettuando lo stesso calcolo con il il filtro reale che stiamo ora utilizzando otterremo $\omega_{m_{ZOH}} = \frac {\omega_c} {2}=\frac {2 \pi f_c} {2} = \frac {\pi}{T_c}$
### che differisce rispetto a quella di Shannon dato che:
#### per Shannon $\omega_{m_{Shannon}} = \frac {\omega_c} {2}$
#### per ZOH sostituendo al frequenza di campionamento $\omega_c$ otteniamo $\omega_{m_{ZOH}} = \frac {\pi}{T_c}$
#### quindi il filtro ottenuto da ZOH, oltre ad avere una pendenza e non essere verticale (come quello ideale di Shannon), inizia a tagliare parte del segnale in $\frac 1 {T_c}$
####  ![FdA/images/confronto_ZOH_Shannon.png](FdA/images/confronto_ZOH_Shannon.png)
#### tale filtro taglia quindi 20 dB a decade partendo da   $\frac 1 {T_c}$ quindi utilizzando un filtro reale del tipo osservato la $\omega_m<\frac 1 {T_c}$ perchè altrimenti si perderebbe una parte del segnale
#### ciò implica che si debba usare con tale ricostruttore $\omega_c\geq 10\omega_m$ diependentemente dalla componente di frequenze che si sa essere contenuta nel segnale
