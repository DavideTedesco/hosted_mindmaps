---
markmap:
  maxWidth: 400
---

# Mapping dal piano S al piano Z

## Introduzione
### l'operazione di mapping serve per mettere in relazione il dominio di Laplace, con il dominio $z$ discreto
### effettuando il passaggio da un segnale nel dominio del tempo, campionandolo, trasformandolo in Laplace e poi trasformandolo ancora nel dominio $Z$, otteniamo una serie così descritta $\displaystyle Z\{x_{_k}\} = \sum_{k=0}^\infty x_{_k} \red{z^{-k}}$ che vediamo essere simile alla serie che descrive il segnale campionato nel dominio di Laplace $\displaystyle X_c(s) = \sum_{k=0}^\infty x_{_k} \red {e^{-ksT_c}}$
### in particolare vediamo come $e^{sT_c}=z$
### ciò permette di effettuare un passaggio da un dominio all'altro

## Sostituzione per il passaggio da un piano all'altro
### ![FdA/images/pianoS.png](FdA/images/pianoS.png)
### osservando una generica rappresentazione del piano di Laplace per un segnale campionato, nella sola sua rappresentazione centrale della fascia compresa dagli estremi $\pm \frac {j\omega_c}{2}$
### potremo, realizzando la sostituzione di $s=j\omega$ e di $T_c=\frac 1 {f_c} = \frac {2\pi} {\omega_c}$
### ottenere $z=e^{j\omega\frac {2\pi}{\omega_c}}$ e sapendo che $\omega = \frac {\omega_c} {2}$ ottenere $z=e^{\pm j\pi}$
### ciò significherà che la fase andrà tra $\pm180°$


## Mapping
### ![FdA/images/pianoSConSegmenti.png](FdA/images/pianoSConSegmenti.png)
### osserviamo che possiamo quindi realizzare un legame tra i due piani, in particolare:
#### il segmento a, diviene la circonferenza unitaria
#### il segmento b e il segmento c sono rappresentati in Z con verso opposto entrambi sull'asse dei reali
#### il segmento in giallo è invece un'ulteriore circonferenza all'interno di quella unitaria (perchè si trova sull'asse negativo dei reali)
### ![FdA/images/pianoZConMapping.png](FdA/images/pianoZConMapping.png)
### lo 0 nel piano S corrisponde all'1 nel piano Z


## Esempio
### un polo qualsiasi con parte reale negativa in S sull'asse delle ascisse è rappresentato in Z, come un polo sull'asse delle ascisse ma nella parte positiva
#### ![FdA/images/mappingPolo.png](FdA/images/mappingPolo.png)


## Considerazioni finali
### riepilogo sul legame piano S piano Z:
#### I punti del piano S a parte reale negativa ($\sigma<0$) sono mappati nei punti del piano Z all’interno del disco unitario.
#### I punti del piano S collocati sull’asse immaginario, aventi cioè parte reale nulla ($\sigma=0$) hanno modulo unitario e sono in corrispondenza con i punti del piano Z collocati sul perimetro del disco unitario.
#### I punti del piano S a parte reale positiva ($\sigma>0$) sono in corrispondenza con i punti del piano Z all’esterno del disco unitario.
### sistemi esistenti nel solo dominio discreto $z$
#### sistemi con elementi su asse dei reali negativo in $z$ sono un qualcosa di esistente solamente in $z$ stesso ciò significherà avere:
##### ![FdA/images/poloSoloZ.png](FdA/images/poloSoloZ.png)
##### $W_2(z) = \frac 1 {z+0.3}$
##### per il campione successivo a $k$ un uscita in $z$ pari a $y_{k+1}=-0.3\cdot y_k+u_k$
### contraction mapping e convergenza della serie
#### per determinare che un sistema sia stabile, è utile, dato che stiamo trattando sistemi con segnali campionati, ricordare il _Teorema della convergenza assoluta_ per definire se un sistema converga o meno
##### definita quindi la sua convergenza assoluta, si potrà utilizzare un criterio per stabilire che il sistema sia stabile, ovvero che il campione successivo a quello attuale sia sempre minore del precedente:
###### $|\frac {y_{k+1}}{y_k} |=|\alpha|<1$
