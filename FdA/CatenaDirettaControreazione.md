# Controllo a catena aperta e a controreazione
- Introduzione: a cosa serve un controllore
    - trattando di sistemi all'interno dell'automatica
    - i controllori sono: dispositivi utilizzati per modificare il comportamento di un sistema
- cosa è il controllo a catena aperta
    - separazione $P(s)$ e $C(s)$
    - azione di controllo che non dipende dall’uscita
    - senza feedback
    - esempio con controllore proporzionale in catena diretta
- cosa è il controllo a controreazione (catena chiusa)
    - azione di controllo che dipende dall’uscita
    - separazione $P(s)$, $C(s)$ e $H(s)$
    - con feedback
    - esempio di acceleratore in macchina
- pregi e difetti della catena aperta
    - pregi
      - più semplice da implementare
    - difetti
      - non ha un controllo sugli errori
      - molto sensibile alle variazioni parametriche
      - non può effettuare asservimento e regolazione
- pregi e difetti della controreazione
    - pregi
      - più preciso
      - capace di rigettare distrubi sulla catena diretta
      - può effettuare asservimento e regolazione
    - difetti
      - più costoso da realizzare
