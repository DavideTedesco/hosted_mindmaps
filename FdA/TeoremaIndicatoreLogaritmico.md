---
markmap:
  maxWidth: 400
---

# Teorema dell'Indicatore Logartimico

## Introduzione
### perchè dobbiamo esprimere il Teorema dell'Indicatore Logartimico:
#### esso è utile per comprendere (applicato all'inverso) quanti poli e zeri vi sono all'interno di un sistema
#### in particolare per scoprire quanti sono i poli a parte reale positiva nella funzione di trasferimento d'anello a ciclo chiuso $W(s)$
##### in caso siano assenti poli a parte reale positiva, il sistema sarà stabile
### arriveremo alla fine del teorema ad esprimere una sottrazione fra zeri e poli che ci faccia comprendere se il sistema in esame sia stabile

## Esplicazione del Teorema
### ![FdA/images/teorema-indicatore-logaritmico.png](FdA/images/teorema-indicatore-logaritmico.png)
### definendo la funzione $M(s)=\frac {s-a} {s-b}$ con zero in $a$ e polo in $b$
### osservando un piano di Gauss con una curva $G$ tre punti: $a,b,s$
#### tracciamo dei vettori $\vec {as}$ e $\vec {bs}$
#### ed osserviamo come il movimento del punto $s$ sulla curva $G$ (facendo ruotare il punto in senso orario sulla curva) faccia tracciare al valore di $M(s)$ una seconda curva (__differente da quella precedente__)
##### è importante notare che se un vettore realizza una rotazione completa, allora avrà avuto una variazione di fase (angolo) di 360 gradi
### possiamo quindi effettuare un calcolo della fase sottraendo il numeratore al denominatore (ricordiamo che stiamo effettuando un’operazione per mezzo di logaritmi, quindi una divisione diviene una sottrazione)
### ![FdA/images/teorema-indicatore-logaritmico-fasi.png](FdA/images/teorema-indicatore-logaritmico-fasi.png)
#### in cui osserviamo che:
#####  $\frac {s-a} {s-b} \rightarrow \frac {2\pi = 1 \ giro }{0\pi = 0 \ giri}$
##### considerazioni sulle rotazioni attorno all'origine
###### i vettori descritti dai punti interni alla curva (come $a$ unito ad $s$) realizzano giri completi, __queste rotazioni saranno le uniche che terremo da conto per il Criterio di Nyquist__
###### i vettori descritti dai punti esterni alla curva (come $b$ unito ad $s$) dalla curva realizzano oscillazioni


## Verso il Criterio di Nyquist
### dalle osservazioni precedenti, potremo individuare le rotazioni della funzione $M(s)$
### esse sono pari al numero di zeri interni ad $M(s)$ meno il numero di poli interni ad $M(s)$
#### $R_{M(s),0}=\text{zeri}[M(s)]_G-\text{poli}[M(s)]_G$
##### esse sono le rotazione di $M(s)$ attorno al punto 0
##### notare come il numero di rotazioni sia calcolato (sapendo che stiamo parlando di funzioni esponenziali) e quindi avremo come numeratore a cui viene sottratto il denominatore della $\frac {s-a} {s-b}$ ovvero zeri a cui vengono sottratti i poli
