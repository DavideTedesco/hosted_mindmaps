---
markmap:
  maxWidth: 300
---

# La funzione di trasferimento

## Introduzione
### essa è il rapporto tra le trasformate di Laplace tra l'uscita e l'ingresso del sistema
#### $\text{Funzione di trasferimento}=\frac {\text{Uscita del sistema}} {\text{Ingresso del sistema}} $


## Da dove proviene
### nel caso in cui il polinomio caratteristico (ovvero il polinomio al denominatore comune tra [risposta libera e risposta forzata](https://davidetedesco.gitlab.io/hosted_mindmaps/mindmap.html?md=RispostaLiberaRispostaForzata.md)) del sistema abbia solo poli a parte reale negativa, si ottiene $\displaystyle Y(S)= F(S)\cdot U(S)$
#### da ciò si può dedurre che la funzione di trasferimento è esprimibile come $\displaystyle F(S) = \frac {Y(S)}  {U(S)}$

## A cosa serve
### essa è uno strumento che permette di descrivere e determinare alcune caratteristiche del sistema in esame
#### Stabilità asintotica
##### è possibile determinare la stabilità del sistema per mezzo dei poli, in particolare se tutti poli hanno parte reale negativa il sistema sarà asintoticamente stabile
#### Velocità di convergenza
##### i poli a minor distanza dall'origine faranno convergere il sistema più rapidamente (se a parte reale negativa)
#### Comportamento oscillatorio
##### nel caso in cui vi siano poli complessi e coniugati si potrà riscontrare un'oscillazione
#### Valore a regime
##### utilizzando il teorema del valor finale si potrà esprimere il valore dell'uscita al permanente $\displaystyle y(t)=\lim_{s\rightarrow0}s\cdot F(s)U(s)$
