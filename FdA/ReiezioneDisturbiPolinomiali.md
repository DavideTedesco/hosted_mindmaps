---
markmap:
  maxWidth: 600
---



# Reiezione dei disturbi polinomiali

## Introduzione
### avendo un sistema del tipo rappresentato, ci chiediamo come si possa reiettare, il disturbo $z_1$ che entra all'interno del sistema
### ![FdA/images/sistema-con-un-disturbo.png](FdA/images/sistema-con-un-disturbo.png)
### sappiamo infatti che un disturbo è un qualcosa chiediamo danneggia il segnale elaborato dal sistema, e sarà quindi utile tentare di espellerlo il prima possibile
### osservando quindi il sistema a controreazione sopra raffigurato, e vedendo che il disturbo si presenta in catena diretta ci chiediamo come possiamo fare per annullarlo

## Dalla funzione di trasferimento a ciclo chiuso al teorema del valor finale
### osservando la funzione a ciclo chiuso di tale sistema $W_{z_1}(s)=\frac{G_2(s)}{1+G_1(s)G_2(s)H(s)}$
### e sapendo che il disturbo sia pari $z(s)=\frac1{s^{k+1}}$
### ci ricordiamo che per mezzo del teorema del valor finale, possiamo osservare l'andamento di un sistema per $t\rightarrow \infty$
### effettuando quindi: $\displaystyle \lim_{s\rightarrow0}sW_z(s)z(s)=\lim_{s\rightarrow0}s\frac{G_2(s)}{1+G_1(s)G_2(s)H(s)}\frac1{s^{k+1}}=0$

## La scelta del segnale per la reiezione
### visto che il segnale che disturba è $\frac 1 {s^{k+1}}$
### dovremo avere un qualcosa che possa arginare tale disturbo
### moltiplichiamo e dividiamo per $s^h$, dato che abbiamo visto che $G_1(s)= \frac {G_1'(s)} {s^h}$ potremo infatti riscrivere il limite in questo modo:
#### $\displaystyle \lim_{s\rightarrow0}\frac{s^{h}\cdot G_2(s)}{s^h+G_1'(s)G_2(s)H(s)}\cdot \frac 1 {s^k}=\lim_{s\rightarrow0}\frac{s^{h-k}\cdot G_2(s)}{s^h+G_1'(s)G_2(s)H(s)}=0$
### vediamo quindi come la reiezione sia dipendente da $h$

## Riepilogo dei casi
### se $h>k$ l'errore è tendente a 0
### se $h<k$ l'errore è tendente a $\infty$
### se $h=k$ l'errore è costante e tendente a $e_z(\infty)=\frac{K_{G_2}}{K_{G_1} K_{G_2} K_{h}}$ in cui $K_h=\frac 1 {K_d}$

## Si tratta di astatismo
### per sistemi astatici, ovvero per quei particolari sistemi con disturbi costanti (gradino)
### che riescono ad essere reiettati utilizzando un numero di integratori pari ad uno
#### dato che il segnale a gradino costante è di tipo $k=0$
#### per reiettare tale gradino avremo bisogno di $h=k+1=1$ integratori, ovvero 1 polo nell'origine

## Riepilogo degli integratori per la reiezione dei disturbi:
### gradino
#### $z(t)=\delta_{-1}(t) \rightarrow Z(s)=\frac 1 s$ quindi esso ha $k=0$ ovvero sarà necessario $h=1$ integratori a monte del disturbo
### rampa
#### $z(t)=\delta_{-2}(t) \rightarrow Z(s)=\frac 1 {s^2}$ quindi esso ha $k=1$ ovvero sarà necessario $h=2$ integratori a monte del disturbo
### parabola
#### $z(t)=\delta_{-3}(t) \rightarrow Z(s)=\frac 1 {s^3}$ quindi esso ha $k=2$ ovvero sarà necessario $h=3$ integratori a monte del disturbo

## Conclusioni
### in un sistema si cerca di avere tutta la sezione di misurazione "impermeabile" ai disturbi (per questo motivo inseriamo a monte del disturbo un elemento per poter ovviare al disturbo)
### in base al grado del segnale di disturbo che dobbiamo rigettare, inseriremo un numero di integratori $h$ a monte del disturbo, tali da reiettare tale disturbo
#### per ricordarsi quanti integratori dobbiamo inserire per reiettare un particolare disturbo, basterà ricordarsi che per reiettare un segnale $k$ serviranno $h=k+1$ integratori
