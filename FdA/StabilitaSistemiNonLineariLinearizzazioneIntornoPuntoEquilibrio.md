---
markmap:
  maxWidth: 600
---

# Stabilità dei sistemi non lineari e linearizzazione intorno a un punto di equilibrio

## Stabilità dei sistemi non lineari

### Introduzione
#### la linearizzazione serve ad approssimare sistemi dal comportamento non lineare
#### in particolare affrontiamo quella che viene definita, linearizzazione intorno ad un punto:
##### in cui si cerca di fissare un punto e data una curva, si tenta di approssimare tale curva con un andamento lineare
###### ![FdA/images/linearizzazioneAttornoAdUnPunto.png](FdA/images/linearizzazioneAttornoAdUnPunto.png)
#### effettuiamo la linearizzazione tramite le scoperte di Lyapunov
#### scegliamo un punto di equilibro per linearizzare un sistema , in base a dove tale sistema abbiamo la derivata prima $\dot x = 0$
##### tale punto viene chiamato $x_{equilibrio}$
#### un sistema non lineare può essere definito __semplicemente stabile__ (e quindi linearizzabile) se la sua evoluzione a partire dall'intorno $\varepsilon$ di un punto, rimane all'interno di un altro intorno (una circonferenza) $\delta_{\varepsilon}$
#### Quindi osserviamo che se il limite dell’evoluzione del sistema è uguale al punto di equilibrio, il sistema sarà __asintoticamente stabile__
#### Sappiamo che un sistema lineare converge con velocità esponenziale a zero, mentre un sistema non lineare converge a un punto ma non per forza in maniera esponenziale.


### Scoperta di Lyapunov
#### Lyapunov affermò che: se la discesa verso il punto di convergenza è al di sotto di una curva esponenziale nella forma $e^{-\alpha t}$ si avrà una stabilità asintotica che discende in maniera esponenziale, quindi il sistema sarà definito esponenzialmente asintotico ed il punto di convergenza è raggiunto __localmente__ in maniera stabile.
#### definizioni di stabilità per i sistemi non lineari:
##### semplicemente stabile
##### asintoticamente stabile
##### esponenzialmente stabile

### Capire se un sistema non lineare è stabile
#### data la funzione $V(x) : \forall x^* \in \{\R -{x_{eq}}\}\\V(x^*) > 0$
#### così descritta ![FdA/images/linearizzazioneAttornoAdUnPuntoVx.png](FdA/images/linearizzazioneAttornoAdUnPuntoVx.png)
#### un sistema non lineare è asintoticamente stabile se data la funzione del sistema $V(x)$ e calcolata la derivata $\dot V(x(t)) = \frac {dV}{dx}(x(t)) \cdot \frac {dx}{dt}=\frac {dV}{dx}(x(t)) \cdot f(x)<0$ essa è minore di zero
#### $\dot V(x)\cdot f(x) < 0 \rightarrow \sum_{non \ lineare} \text{è \textbf{asintoticamente stabile}}$
#### $\dot V(x)\cdot f(x) \le 0 \rightarrow \sum_{non \ lineare} \text{è \textbf{semplicemente stabile}}$
#### notiamo che la derivata negativa indica che $V$ può solo diminuire

### Funzioni di Lyapunov
#### funzioni che diminuiscono sempre
#### esempio del pendolo in cui l'energia diminuisce sempre
#### ![FdA/images/pendolo.png](FdA/images/pendolo.png)
#### partendo dall'equazione del pendolo e scrivendo l'equazione energetica dello stesso come somma di energia cinetica più energia potenziale
#### calcoliamo la derivata della stessa, per verificare che essa sia negativa o al più $\leq 0$ ciò ci permette di comprendere se il sistema non lineare sia asintoticamente stabile o semplicemente stabile
#### in alcuni casi tale funzione non è stabile, potremo quindi:
##### cercare ed utilizzare un'altra funzione di Lyapunov
##### trovare un altro ragionamento per ovviare al fatto che la derivata sia nulla

### Studio dei punti in cui la derivata è nulla
#### avere derivata prima nulla, significa avere velocità pari a zero del pendolo
#### ovvero identificare punti che __non__ sono invarianti del sistema
#### dato che tali punti sono un qualcosa di momentaneo sapremo che la derivata sarà sempre negativa
##### dal Teorema di Lasalle

## linearizzazione intorno a un punto di equilibrio

### Criterio di linearizzazione (intorno al punto di equilibrio)
#### prendendo una funzione non lineare
##### ![FdA/images/funzioneNonLineare.png](FdA/images/funzioneNonLineare.png)
##### sfruttando l'espansione in serie di Taylor
##### ![FdA/images/Taylor.png](FdA/images/Taylor.png)
##### linearizziamo la funzione, facendola divenire combinazione lineare di derivate
##### il sistema diverrà quindi:
###### $\dot {\Delta x}=\frac {df}{dx}\big|_{eq} \Delta x$ simile a $\dot x = Ax+Bu$ in cui riconsciamo stato ed ingresso al sistema
#### per il sistema qui trattato dovremo inserire i suoi punti di equilibrio
#### Se il sistema linearizzato è asintoticamente stabile, il sistema non linerarizzato è asintoticamente stabile
#### Quando queste proprietà valgono solo in un intorno, dovremo dire che il sistema linearizzato è localmente stabile, distinguendo globalmente da localmente stabile.

### Considerazioni sui sistemi non lineari e l'instabilità
#### in caso non siamo riusciti a comprendere precedentemente la stabilità di un sistema, usando il nostro modello dinamico, potremo sfruttare il metodo Montecarlo, quindi individuare una mappa possibile dell'intorno di stabilità
#### quindi, se riusciamo a modellare bene il sistema possiamo lavorare con la sua forma linearizzata e studiarne la stabilità
#### in caso contrario dobbiamo studiare analiticamente la funzione $\dot V(x(t))$ per determinare l’intervallo $(t_1,t_2)$ in cui essa è definita negativa e poi linearizzarlo
