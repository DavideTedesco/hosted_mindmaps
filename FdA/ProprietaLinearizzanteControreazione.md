---
markmap:
  maxWidth: 400
---

# Proprietà linearizzante della controreazione

## Introduzione
### linearizzare un sistema è utile per poter studiare la maggior parte dei sistemi che hanno in genere comportamenti non lineari
### la controreazione linearizza alcune tipologie di sistemi
### tali sistemi che possono essere linearizzati tramite feedback, sono sia statici che dinamici
### in genere per attuare una linearizzazione bisognerà andare ad inserire un elevato guadagno

## Linerazzizzione di un sistema statico
### osservando un sistema di questo tipo:
#### ![FdA/images/LinearizzazioneSistemaStaticoSchema.png](FdA/images/LinearizzazioneSistemaStaticoSchema.png)
### vediamo che esso è non lineare e presenta una controreazione con processo pari a $y(v)=v+v^3$
### gli altri parametri sono:
#### $e=u-y$
#### $v=Ke$
### quindi riscrivendolo con le sostituzioni, avremo:
#### $K(u-y)+K^3(u-y)^3=y$ che diviene $\frac {K(u-y)-y}{K^3}+(u-y)^3=0$ e da ultimo, facendo tendere $K\rightarrow +\infty$ si ottiene $(u-y)^3=0\rightarrow u=y$ quindi un sistema con una relazione lineare
### quindi partendo da un sistema statico non lineare, ne abbiamo raggiunto uno statico lineare, ponendo un alto guadagno in catena diretta sfruttando la controreazione

## Linerazzizzione di un sistema dinamico: il pendolo
### come visto per il sistema non lineare statico, osserviamo che si possa linearizzare, tramite la controreazione anche un sistema non lineare dinamico
### nello specifico osserviamo un sistema caratterizzato da un pendolo
#### ![FdA/images/LinearizzazioneSistemaDinamicoSchema.png](FdA/images/LinearizzazioneSistemaDinamicoSchema.png)
#### ![FdA/images/LinearizzazioneSistemaDinamico.png](FdA/images/LinearizzazioneSistemaDinamico.png)
#### in cui osserviamo che i parametri sono:
##### $\frac 1 {\varepsilon}$ che è il guadagno
##### $\dot \theta$ che è la velocità angolare del pendolo
#### $v$ che è la velocità di riferimento
### sappiamo che tale sistema è esprimibile come $u(t)=Ml^2\ddot \theta+Mgl \sin(\theta)$ ignorando lo smorzamento
### scegliendo un elevato guadagno otterremo $u(t)=\frac 1 {\varepsilon}(v-\dot\theta)$ ovvero $\frac 1 {\varepsilon}$la velocità di riferimento meno la velocità del pendolo
### ottenendo quindi $Ml^2\ddot \theta+Mgl \sin(\theta)=\frac 1 {\varepsilon}(v-\dot\theta) \rightarrow \varepsilon Ml^2\ddot \theta+\varepsilon Mgl \sin(\theta)=(v-\dot \theta)$
### da cui quindi possiamo osservare che per $\varepsilon\rightarrow 0$ ovvero guadagno $\frac 1 {\varepsilon} \rightarrow \infty$ ovvero per elevato guadagno, si ottiene che $v=\dot \theta$ poichè gli elementi moltiplicati per $\varepsilon$ vanno a zero
### avremo ottenuto quindi con un elevato guadagno un sistema dinamico lineare
### sapremo anche che sarà mantenuta la stabilità perchè cancelleremo solamente dinamiche che sappiamo essere stabili
