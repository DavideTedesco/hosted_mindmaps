---
markmap:
  maxWidth: 400
---
# Trasformata Z

## Introduzione
### la trasformata Z è una tecnica che ci permette di trattare le __equazioni alle differenze__, ovvero equazioni differenziali nel dominio discreto
### esempio di equazione alle differenze
#### $y_{_{k+1}} = 3y_{_k}+u_{_k}\\\text{con }u_{_k} = \sum_{k=0}^{\infty}1 \\y_{_0}=2$
### avevamo osservato come semplificare la lettura delle equazioni differenziali scrivendo le stesse per mezzo della Trasformata di Laplace, nel continuo
### dopo aver discretizzato un segnale nel tempo, ottenendo una sequenza di campioni, possiamo passare in un dominio simile a quello di Laplace per mezzo della trasformata Z
### la grande differenza tra la trasformata di Laplace e la Trasformata Z è che quest'ultima permette di scrivere in maniera semplice il ritardo di un segnale campionato

## Definizione
### la trasformata Z è $Y_k(z)=\sum_{k=0}^{+\infty}y_k\cdot z^{-k}$
### portando tale serie a convergere, dato che essa è una serie geometrica, potremo studiarne il carattere:
#### $Y_k(z)=\sum_{k=0}^{+\infty}y_k\cdot z^{-k}$ ricordando che per la convergenza di una serie geometrica si verifica ponendo $\displaystyle \frac 1 {1-q}$ in cui nel nostro caso $q=y_k\cdot z^{-k}$ andremo a scrivere $\frac 1 {1-y_k\cdot z^{-k}}$
##### per poter convergere deve valere $|{y_k\cdot z^{-k}}|<1$
### esempio di trasformata Z della funzione esponenziale:
#### $y(z)=\sum e^{pkT_c}z^{-k}=\sum(e^{pT_c}z^{-1})^k= \frac 1 {1-e^{pT_c}\cdot z^{-1}}=\frac{z}{z-e^{pT_c}}$
##### ed avremo come condizione di convergenza $|z|>e^{pT_c}$
##### da essa osserviamo che nel dominio Z possiamo riconoscere che vi sia uno zero in 0 ed un polo in $e^{pT_c}$
###### questo si può interpretare immaginando una circonferenza centrata in 0 di raggio pari a $e^{pT_c}$. I valori di $z$ che rispettano la condizione sono quelli che si trovano fuori dalla circonferenza. Questo perché $z$ è un numero complesso.
###### ![FdA/images/trasformataZraggio.png](FdA/images/trasformataZraggio.png)

## Utilizzo della Trasformata Z nell'analisi di un sistema di controllo a segnali campionati
### volendo scrivere la Trasformata Z scritta in maniera estesa e scritta in maniera compatta $Z\{y_{_k}\} = y_{_0}+y_{_1}z^{-1}+y_{_2}z^{-2}+\cdots++y_{_n}z^{-n} = \sum_{k=0}^\infty +y_{_k}z^{-k}$
### osserviamo che essa è dipendente dal fatto che il segnale che si vuole trasformare sia già stato precedentemente campionato
### si può osservare che è conveniente alle volte (e di semplice applicazione) passare dal dominio di Laplace al dominio Z e viceversa, osservando che $z=e^{sT_c}$
### ricordando tutti i vantaggi visti nell'utilizzo della Trasformata di Laplace, come: semplificare i calcoli ragionando con funzioni razionali fratte, includere le condizioni iniziali, trovare soluzione a regime e al transitorio
#### analogamente, la Trasformata Z permette di realizzare lo stesso tipo di semplificazione ma sui segnali discreti
#### inoltre aggiunge un'ulteriore semplificazione per la rappresentazione del ritardo di un segnale di un numero di campioni identificato dall'esponente della Z
### essa permette quindi partendo dal dominio continuo con un'equazione differnziale, di passare ad un'equazione alle differenze semplificando l'implementazione informatica
