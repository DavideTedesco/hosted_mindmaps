---
markmap:
  maxWidth: 562
---

# Funzione di sensibilità ed applicazione ai sistemi a controreazione

## Introduzione
### essa è una particolare funzione calcolata per mezzo della formula $S_{\alpha}^{Q}=\frac{\frac {dQ}{Q}} {\frac {d\alpha}{\alpha}}= \frac {\alpha}{Q}\cdot \frac {dQ}{d\alpha}$
#### osserviamo che nel calcolo della sensibilità al numeratore è presente il parametro
### è utile a capire la variazione percentuale di $Q$ rispetto ad $\alpha$, ovvero valuta l'effetto della retroazione
### ![FdA/images/sistemacontroreazione.png](FdA/images/sistemacontroreazione.png)
### la sensibilità è quindi studiata a partire dalla funzione di trasferimento del ciclo chiuso (anello) $W(s)= \frac {G(s)} {1+G(s)\cdot H(s)}$

## Sensibilità diretta e complementare
### __sensibilità diretta__:
#### essa è studiata calcolando la sensibilità di $W(s)$ rispetto a $G(s)$, ovvero il processo in catena diretta
#### $S^W_G=\frac 1 {1+G(s)\cdot H(s)}$
#### essa varia tra 0 ed 1
### __sensibilità complementare__:
#### essa è studiata calcolando la sensibilità di $W(s)$ rispetto a $H(s)$, ovvero il processo in retroazione
#### $S^W_H=\frac {G(s)\cdot H(s)} {1+G(s)\cdot H(s)}$
#### essa varia tra -1 e 0
##### tende a -1 per guadagni di anello molto elevati

### ricordiamo sempre che nel calcolare le sensibilità bisogna sostituire a $W(s)= \frac {G(s)} {1+G(s)\cdot H(s)}$
### esse si complementano e la sottrazione tra sensibilità diretta e complementare è sempre pari ad 1 $S^W_G-S^W_H=1$
#### è impossibile che la somma delle sensibilità si annulli
#### minimizzando una delle due, l'altra andrà a crescere


## Il ruolo del guadagno d'anello per la sensibilità e la reiezione ai disturbi
### il guadagno d'anello
#### il comportamento delle funzioni di sensibilità (diretta e complementare) per guadagni di anello elevati fa:
##### tendere a 0 la sensibilità diretta
##### tendere a 1 la sensibilità complementare
### sistema con due disturbi
#### ![FdA/images/sistema-con-due-disturbi.png](FdA/images/sistema-con-due-disturbi.png)
##### osserviamo due disturbi in punti diversi del sistema
###### $z_1$ è posto nella catena diretta
###### $z_2$ è posto nella catena di retroazione
### reiezione a disturbo in catena diretta
#### osservando i __disturbi in catena diretta__ come $z_1$ vediamo che dato che la sensibilità diretta tende a zero per elevati guadagni allora tali disturbi __potranno essere completamente attenuati__:
##### $Y(s)_{z_1}=\frac{G_2(s)}{1+G_1(s)G_2(s)H(s)}z_1(s)=S^W_G\cdot G_2(s)z_1(s)$
### reiezione a disturbo in retroazione
#### osservando i __disturbi in controreazione__ come $z_2$ vediamo che dato che la sensibilità complementare tende a uno per elevati guadagni allora tali disturbi __non potranno essere _mai_ completamente attenuati__:
##### $Y(s)_{z_2}=\frac{H(s)G_1(s)G_2(s)}{1+H(s)G_1(s)G_2(s)}z_2(s)=S^W_H\cdot z_2(s)$

## Considerazioni finali
### dato che è impossibile azzerare entrambe le sensibilità
### si tende in genere a decidere su quale delle due lavorare
### data la natura delle due sensibilità, è comune cercare di minimizzare i disturbi in catena diretta
### è inoltre importante osservare che la sensibilità agli errori è maggiore in corrispondenza della frequenza di taglio
### la sensibilità complementare tende a scendere con l’aumentare della pulsazione, quella diretta tende ad aumentare.
### inoltre è utile osservare che tutti i sistemi che presentano dei percorsi chiusi sono soggetti ad avere un funzionamento lineare fino ad un punto cui vi è una risonanza
