---
markmap:
  maxWidth: 500
---


# Errore di riproduzione di una sinusoide e specifiche in frequenza

## Introduzione
### sapendo che sia verificata la stabilità asintotica del sistema
### con un sistema di questo tipo
#### ![FdA/images/sistemaErroreSinusoidale.png](FdA/images/sistemaErroreSinusoidale.png)
### ricordando che l'errore è calcolabile come $E(s)=\frac{K_d^2}{K_d+G(s)}\cdot U(s)$
### ora in $U(s)=sin(\omega t)$ consideriamo di avere un segnale sinusoidale
### e cerchiamo di comprendere come sia osservabile, diversamente da prima l'errore per la tipologia di funzioni in esame

## Funzione di trasferimento errore-uscita
### osserviamo che per poter esprimere una funzione di trasferimento errore-uscita
### dividiamo per $K_d$ e portiamo $U(s)$ dall'altro lato:
#### $\frac{E(s)}{U(s)}=\frac{K_d}{1+\frac{G(s)}{K_d}}$
### così notiamo che questa $G(s)\cdot \frac 1 {K_d}$ è la funzione d'anello che possiamo chiamare $F(s)$ e sostituirla nella funzione di trasferimento errore-uscita:
#### $|\frac{E(s)}{U(s)}|=|\frac{K_d}{1+F(s)}|$
### dato che non ci interessa il segno dell'errore, ma solo il modulo (dato che stiamo calcolando la misura di un errore), andiamo a calcolarne il modulo

## Considerazioni sull'errore
### avendo un $F(s)$ molto grande, potremo trascurare l'1 al denominatore, quindi l'errore sarà pari a:
#### $e=\frac{K_d}{|F(s)|}$
### avendo l'errore massimo $e_{max}$ e frequenza massima $\omega_{max}$ potremo imporli all'interno dell'equazione vista prima espletando la funzione $F(s)$ sostituendo $s=j\omega$

## Conclusioni
### potremo quindi affermare che $|F(j\omega)|\ge\frac{K_d}{e_{\text{max}}}$
### ciò significherà che potremo individuare una finestra proibita sul diagramma di Bode del modulo che ha:
#### come ascissa finale l'$\omega_{max}$
#### come ordinata finale il valore trovato $\frac {K_d}{e_{max}}$
### tale finestra proibita deve essere evitata andando a inscrivere il sistema all'esterno della stessa
#### ![FdA/images/finestraProibita.png](FdA/images/finestraProibita.png)
