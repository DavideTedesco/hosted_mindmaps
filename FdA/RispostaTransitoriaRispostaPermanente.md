---
markmap:
  maxWidth: 300
---

# Risposta transitoria e permanente

## Introduzione
### considerando un sistema a tempo continuo $F(s) = \frac  {N(s)} {D{s}}$
### ed applicando a tale sistema un ingresso canonico del __tipo k__ $u(t)=\frac {t^k} {k!}$ che trasformato $U(s)= \frac 1 {s^{k-1}}$
### otterremo una risposta del sistema in Laplace pari a $Y(s) = \frac  {N(s)} {D{s}} \cdot U(s)$
### riportando nel dominio del tempo l'uscita del sistema si possono osservare tre componenti:
#### contributo dei poli asintototicamente stabili (faranno parte del regime transitorio)
#### contributo dei poli semplicemente stabili (faranno parte del permanente)
#### contributo dei poli instabili (faranno parte del permanente)

## Che cos'è la risposta di un sistema
### Risposta transitoria
#### risposta di un sistema durante la sua fase di assestamento
#### si studia il comportamento dei poli asintoticamente stabili
### Risposta al permanente
#### risposta di un sistema quando le dinamiche transitorie si sono esaurite


## Determinare la risposta transitoria
### essa si può studiare per mezzo della notazione $s^2+2\zeta\omega_ns+\omega_n^2$ che si riferisce a
#### pulsazione naturale $\omega_n$
#### smorzamento $\zeta$
### tali parametri ci permettono di comprendere
#### pulsazione naturale: velocità di oscillazione ovvero il _modulo_ del polo in notazione polare (segmento che unisce il polo all'origine)
#### smorzamento: valore che indica quanto velocemente le oscillazioni vengono attenuate esso varia tra 0 ed 1 (0 assenza di smorzamento, 1 smorzamento massimo ovvero assenza di oscillazioni)

## Determinare la risposta al regime permanente
### Il teorema del valor finale:
#### $\lim_{t\rightarrow \infty} f(t)= \lim_{s\rightarrow 0^+} s\cdot F(s)$
##### permette di esprimere a quale valore il sistema tenderà dopo il transitorio studiando dove si sarà giunti a seguito degli andamenti dei poli asintoticamente stabili; ovvero si andranno ad osservare solo i poli semplicemente stabili e quelli instabili
