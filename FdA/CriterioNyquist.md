---
markmap:
  maxWidth: 400
---

# Criterio di stabilità di Nyquist

## Introduzione
### avendo osservato il funzionamento del Teorema dell'Indicatore Logaritmico, cerchiamo di comprendere in maniera inversa, quanti poli e zeri abbia un sistema e da ciò comprendere se abbia o meno poli a parte reale positiva
### passaggio dal Teorema dell'Indicatore Logaritmico al criterio di Nyquist


## Sistema a Controreazione unitaria
### ![FdA/images/sistemacontroreazionesemplice.png](FdA/images/sistemacontroreazionesemplice.png)
### prendendo in esame una funzione di trasferimento con controreazione $W(s)$ di un sistema a controreazione semplice, sappiamo che:
#### sapendo che $W(s)=\frac{F(s)}{1+F(s)}$ e che $F(s)=\frac{N(s)}{D(s)}$

## Considerazioni zeri e poli
### osserveremo che possiamo riscrivere $W(s)=\frac{N(s)}{D(s)+N(s)}$
### concentrandoci sul solo denominatore della $W(s)$ iniziale, ovvero $1+F(s)$ osserviamo che:
#### I poli della funzione $F(s)$ coincidono con i poli di $1+F(s)$
#### Gli zeri della funzione $W(s)$ coincidono con gli zeri di $F(s)$
#### I poli della funzione $W(s)$ coincidono con gli zeri di $1+F(s)$

## Percorso di Nyquist
### abbiamo osservato come relazionare i poli e gli zeri delle funzioni fino ad ora viste ($F(s)$ e $W(s)$) con la funzione $1+F(s)$
#### tale funzione permette di descrivere una particolare curva al cui interno siano contenuti tutti i poli a parte reale positiva (che sfruttiamo in maniera inversa per l'indicatore logaritmico), essa permette quindi di individuare elementi scomodi per i sistemi in esame
#### tale curva è detta __Percorso di Nyquist__
#### ![FdA/images/percorsonyquist.png](FdA/images/percorsonyquist.png)
### considerazioni sul Percorso di Nyquist
#### se abbiamo una radice proprio sull’asse verticale allora possiamo considerarla a sinistra della curva di Nyquist (come se la curva passasse a destra di quella radice)
#### possiamo quindi affermare che le rotazioni intorno allo zero, della funzione $1+F(s)$ siano descrivibili come:
##### $R_{1+F(s),0}=\text{zeri}[1+F(s)]_C-\text{poli}[1+F(s)]_C$
###### utilizzando le considerazioni prima viste, otteniamo $R_{1+F(s),0}=\text{poli}[W(s)]_C-\text{poli}[F(s)]_C$
### ipotizzando che $W(s)$ sia stabile, quindi che non abbia poli a parte reale positiva
#### possiamo escludere i poli di $W(s)$, ottenendo:
##### $R_{1+F(s),0}=-\text{poli}[F(s)]_C$



## Criterio di Nyquist (criterio di stabilità completo)
### volendo ottenere la $F(s)$ originale (e non lavorare più su $1+F(s)$) potremo andare a contare le rotazioni attorno al punto -1
#### ottenendo quindi come rotazioni $R_{F(s),-1}=-\text{poli}[F(s)]_C$
##### i possibili casi:
###### n rotazioni= n poli a parte reale positiva: __se e solo se__ vi sono un numero di rotazioni intorno a -1 pari al numero di poli a parte reale positiva di $F(s)$ allora la funzione sarà stabile
###### n rotazioni ≠ m poli, dove $n>m$: il sistema sarà instabile
### quindi il criterio esprime che: _data una funzione di trasferimento ad anello $F(s)$, se il numero di rotazioni del grafico completo della $F(s)$ attorno al numero $-1$ è_ $-\#Poli_{Re>0}[F]$ _(il numero dei poli a parte reale positiva della $F(s)$) allora il sistema sarà stabile._

## Sotto-criterio di Nyquist (criterio di stabilità ridotto)
### _se la funzione a ciclo aperto è asintoticamente stabile, per avere stabilità a ciclo chiuso, non bisogna avere curve attorno al punto_ $-1$.
###### 0 rotazioni: nel caso in cui il diagramma di Nyquist non passi attorno al punto -1, saremo ora certamente sicuri, che il sistema sia stabile, ovvero ha solo poli a parte reale negativa e non vi sono rotazioni intorno a -1

## Casi particolari
### un polo o zero sulla curva induce una variazione della fase:
#### per i poli di $-\pi = -180°$, variazione oraria
#### per gli zeri $+\pi = +180°$ , variazione antioraria
### quindi:
#### per poli sull'origine, avremo che essi verrano evitati realizzando un percorso uncinato, che esclude il polo dalla curva, e realizza una variazione oraria della fase con vettori che puntano verso il polo

## Esempio di diagramma di Nyquist
### ![FdA/images/esempionyquist.png](FdA/images/esempionyquist.png)

## Considerazioni generali sulle rotazioni intorno a -1: stabilità del sistema
### visto che il ciclo aperto è più facilmente manipolabile rispetto al ciclo chiuso, esso diverrà uno strumento di progetto, dandoci una relazione tra ciclo aperto e ciclo chiuso.
