---
markmap:
  maxWidth: 500
---

# Metodologie di sintesi di sistemi di controllo a segnali campionati

## Introduzione
### con il termine sintesi diretta, viene imposta la scrittura di una funzione di trasferimento e da essa viene dedotta la forma del controllore, ciò può essere sviluppato sia in $s$ che in $z$ e deve rispettare delle condizioni:
#### causalità
#### stabilità del ciclo chiuso
#### stabilità interna
### osserviamo quindi di seguito
#### sintesi di un controllore dal continuo al discreto
#### sintesi diretta di un processo completo nel dominio discreto


## Sintesi di controllori dal continuo al discreto
### in questa tipologia di sintesi si discretizza solamente il controllore e non tutto il processo che risiede nel continuo
### partendo dal sistema
#### ![FdA/images/SintesiDirettaContinuo.png](FdA/images/SintesiDirettaContinuo.png)
### per poter portare il controllore in $z$ possiamo utilizzare una delle trasformazioni vista precedentemente
#### esatta
#### guadagno
#### ZOH
#### Tustin
### otterremo quindi un processo che ha un controllore discreto, dei quantizzatori prima e dopo lo stesso ed un filtro anti-aliasing
#### ![FdA/images/SintesiContinuoDiscreta.png](FdA/images/SintesiContinuoDiscreta.png)
### preso quindi un processo continuo e discretizzato il controllore, sapremo che in $C(s)$ il processo è continuo, ma non sapremo mai come si comporterà in $z$:
#### ![FdA/images/rispostaNonPiatta.png](FdA/images/rispostaNonPiatta.png)
### stando a certe condizioni osserveremo che il controllore riesce a rendere la risposta piatta anche se discreto ed applicato ad un processo continuo:
#### ![FdA/images/rispostaPiatta.png](FdA/images/rispostaPiatta.png)

## Sintesi proprie dei sistemi discreti (sintesi diretta ovvero sintesi a risposta piatta)
### trasformazione in $z$ di tutto il processo (discretizzazione di tutto il processo):
#### tutto il sistema viene trasformato in $z$
##### ![FdA/images/sintesiDirettaZ.png](FdA/images/sintesiDirettaZ.png)
### sintesi diretta del controllore totalmente realizzata in $z$
### si sostitusce $z=e^{sT_c}$
### funzione di trasferimento del processo in $z$
#### $W(z)=\frac {C(z)\cdot P(z)}{1+C(z)\cdot P(z)}$
#### osserviamo che per essere causale essa deve essere nel dominio discreto, ritardata di qualche campione $n$ quindi ponendo $\frac {C(z)\cdot P(z)}{1+C(z)\cdot P(z)}= z^{-n}$ potremo ricavare il controllore in $z$
#### $U(z)W(z)=Y(z) \space \Rightarrow \space W(z)=\frac{Y(z)}{U(z)}=\frac{CP}{1+CP}=z^{-n}$ che poi diviene $CP=z^{-n}+CPz^{-n} \space\Rightarrow\space C(P-z^{-n}P)=z^{-n}$ e quindi $C=\frac1{z^{n}} \frac{1}{P-Pz^{-n}}=\frac{1}{Pz^n-P}$
#### e sostituendo $P=\frac {N_p}{D_P}$
#### avremo $C(z)=\frac {D_P}{N_Pz^n-N_P}$
### $n$ è quell’eccesso poli-zeri che devo aggiungere per recuperare l’anti-causalità di $\frac 1 P$, quindi la $n$ rappresenta il numero minimo di poli da aggiungere nel controllore per rendere $\frac 1 {P(z)}$ stabile, indicati nel discreto come passi di ritardo, prima dei quali mi è impossibile ottenere questo tipo di uscita.
### è detta sintesi a risposta piatta, perchè si otterà una risposta del sistema a gradino ritardata di qualche campione, mentre nel continuo si aveva una curva per arrivare al regime permanente
