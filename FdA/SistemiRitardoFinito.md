---
markmap:
  maxWidth: 400
---

# Sistemi a ritardo finito

## Introduzione
### tra le varie tipologie di sistemi utili da studiare, andiamo ad analizzare, quelli a __ritardo finito__
#### essi sono particolari sistemi fisici caratterizzati dalla presenza di un ritardo finito della misurazione dello stato del sistema dovuto a:
##### fattori intrinsechi al sistema
##### fattori esterni al sistema
### è importante notare come partendo dalla trasformata di Laplace di alcuni particolari sistemi di questa tipologia $L_-\{f(t-d)\}=F(s)e^{-ds}$ in cui $d$ è il _delay_ o ritardo
#### si osserva che la traslazione del sistema nel dominio del tempo, corrisponda in Laplace a un prodotto che tenga conto all'esponente dell'esponenziale, del ritardo introdotto nel dominio del tempo
#### da ciò si deduce che il ritardo applicato al sistema debba essere limitato per non rendere instabile il sistema dal punto di vista della fase
#### inserire un ritardo in un sistema è rilevante solamente dal punto di vista della fase (e non del modulo)
### un sistema con ritardo, così come lo abbiamo descritto è rappresentato dal seguente schema a blocchi
#### ![FdA/images/ritfinito.png](FdA/images/ritfinito.png)


## Esempi
### gli esempi di seguito riportati hanno un tempo di ritardo che avviene durante la comunicazione e permettono di osservare gli effetti dell’azione del controllore (che si sta utilizzando), solo dopo un certo lasso di tempo.
### esempio del controllo di un robot sulla Luna
#### avremo un paio di secondi di latenza per comunicare un’azione da compiere ad un robot posto sulla Luna
#### tale sistema ha quindi un ritardo dal momento in cui si effettua un'operazione di controllo, al riscontro della stessa, per via della distanza tra terra e Luna
### esempio del laminatoio
#### vedendo un laminatoio a due rulli, in cui arriva in ingresso una lastra di acciaio ancora calda e deve uscire poi una lastra di spessore più sottile, osserviamo che avviene un ritardo nella misurazione dello spessore in uscita
#### infatti per tale misurazione i sensori sono posti all'uscita dei rulli, __inserendo quindi un ritardo nella misurazione__ dato che dal momento in cui la lamina entra a quando esce è trascorso del tempo
#### ![FdA/images/laminatoio.png](FdA/images/laminatoio.png)
### esempio numerico, prendo un sistema con una funzione pari a $F(s)=\frac{10(s+1)}{s(s/10+1)(s/100+1)^2}$
#### ne osserviamo il diagramma di Bode ![FdA/images/boderitfin_1.png](FdA/images/boderitfin_1.png)
#### inserendo un ritardo $d=1 \text{ms} $ nella $L_-\{f(t-d)\}=F(s)e^{-ds}$ in cui tale ritardo ha come diagramma di Bode ![FdA/images/boderitfin_2.png](FdA/images/boderitfin_2.png)
##### otterremo un diagramma di Bode pari al sistema originario ritardato che va a modificare la fase (in blu il segnale con l'effetto del ritardo), in particolare, la fase osserviamo che vada verso $-\infty$  ![FdA/images/boderitfin_3.png](FdA/images/boderitfin_3.png)


## Intervento del ritardo su modulo e fase di un qualsiasi sistema
### osserviamo che per il modulo il ritardo introdotto $e^{-d j\omega}$ non influisce sul risultato finale, infatti $|e^{-d j\omega}|= 20 \cdot \log_{10} (1)=0 \ dB$
### osserviamo che per la fase, il ritardo introdotto $e^{-d j\omega}$ influisce sul risultato finale, infatti $\angle { e^{-d j\omega}}= -d\cdot\omega $ gradi; è importante notare che l’effetto del ritardo è lineare in $\omega$ ma nel diagramma di Bode (essendo la scala logaritmica) esso diviene un esponenziale che va come $e^{-d\omega}$.
###  ![FdA/images/andfasrit.png](FdA/images/andfasrit.png)


## Stabilizzazione con ciclo chiuso
### se l'andamento esponenziale della fase (dopo l'introduzione del ritardo), compare dopo l'$\omega$ di taglio, il riardo non incide sulla stabilità del sistema
### se invece il contributo del ritardo influisce sulla fase del sistema prima dell'$\omega$ di taglio si avrà una diminuzione del margine di fase
### saranno quindi utili delle tecniche per stabilizzare il sistema qualora si introduca un ritardo finito per poter preservare un margine di fase minimo che mantenga stabile il sistema
#### si cercherà quindi di compensare la perdita di fase con una diminuzione del guadagno del segnale in ingresso al sistema (che potrebbe portare ad altri problemi); quindi si cercherà di evitare la presenza di ritardi (anche piccoli) all'interno del sistema
