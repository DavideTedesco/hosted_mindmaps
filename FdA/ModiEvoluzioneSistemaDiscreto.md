---
markmap:
  maxWidth: 400
---

# Modi propri d’evoluzione di un sistema discreto

## Introduzione
### come visto in precedenza possono esistere due tipologie di sistemi discreti:
#### quelli che esistono nel continuo e si possono discretizzare
#### quelli che esistono nel solo dominio discreto
### precedentemente avevamo visto come i poli facessero stabilizzare o meno il sistema, in genere in $s$:
#### si ha un sistema stabile con poli a parte reale negativa
#### ciò è ancora vero per i sistemi discreti (in $z$) poichè la stabilità in genere è mantenuta all'interno del cerchio unitario, infatti, andando a posizionare poli all'esterno di esso, avremo in $z$ dei sistemi instabili poichè essa sarebbe dove viene mappato il piano a parte reale positiva

## Sistema discreto mappato dal piano s
### Una funzione in $z$ è __asintoticamente stabile__ se tutti i poli sono interni alla circonferenza di raggio 1, se i poli sono nel limite di stabilità avremo un qualcosa sulla circonferenza e se abbiamo qualcosa sugli estremi sappiamo che oscillerà.
### Oscillazione di un sistema asintoticamente stabile campionato:
#### ![FdA/images/ModiEvoluzioneSistemaDiscretoOscillante.png](FdA/images/ModiEvoluzioneSistemaDiscretoOscillante.png)
#### La pulsazione naturale è la distanza del polo dall'origine, quindi se la pulsazione naturale sarà più elevata, allora avremo un'oscillazione più veloce.
### i modi di evoluzione di un sistema discretizzato dal dominio continuo saranno verificabili, come sarà verificabile la sua stabilità tramite l'osservazione del posizionamento del sistema all'interno o all'esterno della circonferenza unitaria
#### interno o sulla circonferenza unitaria: il sistema è stabile
#### esterno alla circonferenza: il sistema è instabile

## Sistema discreto esistente solamente nel dominio discreto
### per tutti quei sistemi che non siano derivati dal continuo, e che esistono solamente nel dominio discreto (segmento all'interno della circonferenza unitaria negativo)
#### si dovrà osservare se i campioni che costituiscono tali sistemi abbiano un andamento decrescente
##### come in genere una successione alternante non proveniente da un sistema lineare
###### ![FdA/images/ModiEvoluzioneSistemaDiscretoAlternante.png](FdA/images/ModiEvoluzioneSistemaDiscretoAlternante.png)
### per verificare la convergenza della serie realizzata dal sistema discreto, si utilizza  il teorema della convergenza assoluta della serie
#### per verificare tale teorema, bisogna osservare che il rapporto tra l'elemento successivo e quello precedente sia minore di 1 definitivamente
##### $k<k_0$ con $k_0$ elemento precedente di $k$

## Conclusioni
### per i sistemi mappati direttamente dal dominio continuo, sappiamo che essi siano stabili se sono all'interno del circonferenza unitaria
### per i sistemi esistenti solo nel dominio discreto, bisognerà utilizzare il __Teorema della convergenza assoluta di una serie__, ma la convergenza assoluta di una serie è garantita dal fatto che il campione successivo sia più piccolo del campione precedente. Quindi il coefficiente che sta davanti all’uscita del campione precedente, ovvero $\alpha$, deve essere dentro il cerchio unitario.
#### $y_{k+1}=\alpha y_k+u_k$ che deve rispettare $|\frac {y_{k+1}}{y_k}|= |\alpha|<1$
