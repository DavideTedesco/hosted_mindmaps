---
markmap:
  maxWidth: 550
---

# Comportamento a Regime

## Introduzione
### studiamo l'errore del sistema a regime (al permanente) in relazione al tipo di segnale in ingresso ($k$) e successivamente al numero di integratori nel processo ($h$)
### Questo studio è utile per capire a seguito del transitorio la differenza tra l'uscita desiderata ($y_d$) e l'uscita ottenuta ($y$).
#### Osserviamo quindi che l'errore è pari a $e(t) = y_d(t) - y(t) \rightarrow E(s) = Y_d(s) - Y(s)$

## Sistema in esame
### partiamo da un sistema a controreazione __unitaria__ con in ingresso $U(s)$, in catena diretta il processo $G(s)$ ed in uscita $Y(s)$
#### ![FdA/images/fig1.png](FdA/images/fig1.png)
### successivamente per ottenere un errore nullo, potremo osservare che l'uscita desiderata $Y_d(s)=K_d \cdot U(s)$. Sarà quindi necessario studiare l'andamento dell'errore in funzione di $K_d$, come visibile in #### ![FdA/images/fig2.png](FdA/images/fig2.png)
#### abbiamo in controreazione $\frac 1 {K_d}$ per permetterci di arrivare successivamente allo studio dell'__uscita ideale__.
### per ottenere l'uscita ideale $Y_d$ e poterla confrontare con l'uscita reale $Y$ inseriamo un altro sistema _ideale_ che ci permette di ottenere $Y_d=K_d \cdot U(s)$ come visibile in:
#### ![FdA/images/fig3.png](FdA/images/fig3.png)

## Calcolo dell'errore tra l'uscita del sistema reale e quella del sistema ideale
### a questo punto l'errore è pari a  
#### $E(s)=Y_d-Y=K_dU(s)-W(s)U(s)\\=K_dU(s)-\displaystyle\frac{K_dG(s)}{K_d+G(s)}U(s)\\=\left(\displaystyle\frac{K_d^2+K_dG(s)-K_dG(s)}{K_d+G(s)}\right)U(s)$
##### quindi otteniamo $E(s)=\displaystyle\frac{K_d^2}{K_d+G(s)}U(s)$
###### notiamo che l'errore __dipende dal segnale in ingresso__ $U(s)$


## Generalizzazione con segnali di tipo $k$
### dati:
#### un segnale $u(t) = \frac {t^k}{k!} \rightarrow U(s) = \frac 1 {s^{k+1}}$
#### un processo con $h$ integratori (poli) in catena diretta, espresso nella forma $G(s) = \frac {G(s)'}{s^h}$
### calcoliamo l'errore in uscita pari a $E(s)=\displaystyle\frac{K_d^2}{K_d+\displaystyle\frac{G'(s)}{s^h}}\frac{1}{s^{(k+1)}}$
### tramite il __Teorema del Valor Finale__ calcoliamo l'errore al permanente:
#### $\displaystyle \lim_{s\to0}s\cdot E(s)=\lim_{s\to0}s\cdot \frac{K_d^2}{K_d+\frac{G'(s)}{s^h}}\cdot \frac{1}{s^{(k+1)}}=\lim_{s\to0} s\cdot \frac{K_d^2 \cdot s^h}{K_d^2+G'(0)}\cdot \frac 1 {s^{k+1}} = \frac{K_d^2}{G'(0)}\lim_{s\to0}\frac{1}{s^{k-h}}$
### definiamo ora $K_g$ ovvero __guadagno generalizzato__ di un processo $G(s) = \frac {G(s)'}{s^h}$ come  $K_g = G'(0)$, ovvero il valore del processo $G(s)$ senza considerare gli integratori in $s=0$
### dal limite osserviamo i vari casi:
#### $\displaystyle \lim_{s\to0}\frac{K_d^2}{s^h \cdot K_d + K_G}\cdot\frac{1}{s^{k-h}}=\begin{cases}    +\infty &k>h\\ \displaystyle\frac{K_d^2}{K_G} &k=h \text{ con }h,k\neq0 \\ \displaystyle\frac{K_d^2}{K_d+K_G} &k=h \text{ con }h,k=0 \\ 0 &k<h \end{cases}$


## Tabella classificazione in tipi
### osserviamo la tabella in figura: la prima riga indica il tipo di segnale in ingresso (da $0$ a $n$, con schematizzazione del segnale), mentre la prima colonna indica il numero di integratori nel controllore (da $0$ a $n$).
#### ![FdA/images/fig4.png](FdA/images/fig4.png)
#### ![FdA/images/fig5.png](FdA/images/fig5.png)
### Nella tabellina di seguito riportata, vediamo che sulla diagonale vi sono i sistemi di controllo di tipo $k$: si dice un sistema di controllo di tipo $k$ quando ha errore finito al regime permanente in risposta a un ingresso di tipo $k$.
### da notare che per $k=h=0$, ovvero un segnale di tipo 0 (gradino, = $\delta_{-1}(t)$ ) con 0 integratori ($h=0$) la formula per il calcolo dell'errore differisce poichè non si semplifica il $K_d$ al denominatore nel limite.
