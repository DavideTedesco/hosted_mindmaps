---
markmap:
  maxWidth: 400
  extraCss:
    - 'https://davidetedesco.gitlab.io/hosted_mindmaps/FdA/fix_dark_mode.css'
---

# La trasformata di Laplace

## Introduzione
### Utilizziamo la Trasformata unilatera destra
### Si applica a equazioni differenziali lineari a coefficienti costanti
### è definita come
#### $\displaystyle \mathscr{L}_{-}\{f(t)\} = F(S)  \\ \int_{0^-}^{+\infty}f(t) e^{-st} dt\\ \text {con }s= \sigma + j\omega$
##### cosa è $\sigma$
###### ascissa di convergenza, ovvero quella particolare ascissa che divide in due parti lo spazio gaussiano alla quale destra la trasformata sia calcolabile

## A cosa serve
### Per semplificare i calcoli e ragionare con funzioni razionali fratte invece di esponenziali
### include direttamente le condizioni iniziali
### si trovano contemporaneamente la soluzione al transitorio e quella al regime
## Trasformate elementari
### gradino
#### $\displaystyle \mathscr{L}_{-}\{\delta_{-1}(t)\} = \frac 1 s $
### rampa
#### $\displaystyle \mathscr{L}_{-}\{\delta_{-2}(t)\} = \frac 1 {s^2} $
### rampa parabolica
#### $\displaystyle \mathscr{L}_{-}\{\delta_{-3}(t)\} = \frac 1 {s^3} $
### delta (impulso)
#### $\displaystyle \mathscr{L}_{-}\{\delta_{0}(t)\} =  1 $
### seno
#### $\displaystyle \mathscr{L}_{-}\{ \sin (\omega t)\} = \frac {\omega} {s^2+\omega^2} $
### coseno
#### $\displaystyle \mathscr{L}_{-}\{ \cos (\omega t)\} = \frac {s} {s^2+\omega^2} $

## Metodo dei poli e dei residui
### tale metodo serve per poter ottenere da un qualcosa trasformato nel dominio di Laplace, la funzione di partenza nel dominio del tempo
#### elemento di partenza nel dominio di Laplace $\displaystyle F(S)= \frac {N(S)} {D(S)} = \frac {\sum b_i\cdot s^i} {\sum a_i\cdot s^i}$
### cos'è l'anti-trasformata di Laplace
#### essa è quel procedimento utile a portare dal dominio di Laplace al dominio del tempo
### cosa sono i residui
#### essi sono quei valori che consentono di scomporre la funzione di partenza (che deve essere un polinomio) in una somma di di fratti semplici (essi sono dei moltiplicatori del numeratore)
##### essi si ottengono calcolando il limite
###### per poli singoli $\displaystyle R_i = lim_{s\rightarrow p_i}(s-p_i)\frac {N(S)} {D(S)}$ in cui $p_i$ è lo specifico polo $i$
###### per poli multipli $\displaystyle R_{ij} = lim_{s\rightarrow p_i}\frac {1}{(j-1)!}\frac {d^{j-1}} {ds^{j-1}} \cdot (s-p_i)^{r_i} \cdot \frac {N(S)} {D(S)}$
### cosa sono i poli e gli zeri
#### i poli sono le soluzioni del polinomio al denominatore
#### gli zeri sono le soluzioni del polinomio al numeratore
### applicazione del metodo dei poli e residui
#### si scompone la $F(S)$ in una somma di frazioni utilizzato poli e residui e poi si antitrasforma ognuna di queste frazioni riconducendole alle anti-trasformate elementari
