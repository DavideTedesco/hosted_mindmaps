---
markmap:
  maxWidth: 400
---

# Il regolatore standard PID

## Introduzione
### esso è un particolare tipo di controllore, realizzato utilizzando vari controlli contemporaneamente
### viene spesso utilizzato nell'ambito industriale in rack
### in particolare esso è composto da:
#### parte Proporzionale
#### parte Integrativa
#### parte Derivativa
### ha in genere:
#### riferimento (ingresso al sistema) che può essere alle volte non utilizzato, poichè viene calcolato internamente
#### misura (controreazione)
#### azione di controllo (collegata al processo)
### ![FdA/images/sistemaPID.png](FdA/images/sistemaPID.png)

## I parametri
### come precedentemente descritto esso è composto da tre grandezze con tre guadagni differenti
### $PID = \underbrace{K_P}_{\text{Proporzionale}}+\underbrace{\frac {K_I}{s}}_{\text{Integrativo}}+\underbrace{K_D\cdot s}_{\text{Derivativo}}$
### ognuna di tali componenti, è utile per fornire un contributo di controllo all'intero sistema
#### in particolare possiamo vedere, come l'errore, venga gestito inserendo una componente derivativa $\dot e= \dot r-\dot y$ ovvero riferimento a cui viene sottratta l'uscita
##### dato che in genere il riferimento è una costante, derivandola, otterremo $\dot e= -\dot y$ ed avremo quindi uno smorzamento
#### le altre componenti andranno di pari passo

## Forma ideale, forma parallela
### in realtà il PID, come lo abbiamo osservato sino ad ora è di fatto anti-causale, poichè la parte derivativa non può essere causale, a meno che non venga divisa per un particolare valore
### a tale scopo, viene riportato di seguito, una tipologia di PID realizzabile e causale, che prende il nome di PID parallelo che fa uso di un derivatore reale
### $PID = \underbrace{K_P}_{\text{Proporzionale}}+\underbrace{\frac {K_I}{s}}_{\text{Integrativo}}+\underbrace{\frac {K_D\cdot s} {s\cdot \varepsilon +1}}_{\text{Derivativo}}$

## Comportamento al variare dei parametri
### Parte Proporzionale
#### è indirettamente proporzionale al tempo di salita
### Parte Integrativa
#### è utile per attenuare l'errore a regime permanente
### Parte Derivativa
#### è utile per diminuire l'oscillazione del sistema nel regime transitorio, al crescere della stessa diminuisce l'oscillazione
