---
markmap:
  maxWidth: 665
---

# Scelta del tempo di campionamento

## Introduzione
### abbiamo osservato come sia possibile rappresentare un segnale continuo nel dominio discreto tramite dei convertitori A/D  e un filtro anti-aliasing; come riconvertirli da digitale a analogico tramite convertitore D/A e amplificatore
#### ovvero attraverso il Teorema del Campionamento
### infatti un segnale continuo può essere rappresentato come una serie di impulsi distanziati nel tempo e con specifiche ampiezze
#### tale tempo che intercorre tra un campione ed un altro è chiamato __Tempo di campionamento__
### è necessario comprendere come scegliere adeguatamente tale tempo

## Precisione del tempo di campionamento in base al segnale in ingresso
### abbiamo osservato come sia possibile decidere una frequenza di campionamento in base al Teorema di Shannon, ovvero sapendo che un segnale debba essere necessariamente campionato con frequenza $\omega_c>2\omega_m$ in cui $\omega_c$ è la frequenza di campionamento e $\omega_m$ la frequenza massima del segnale in esame
### tale frequenza di campionamento non è spesso veritiera, poichè essa è solamente ideale, in realtà molti segnali (e quindi sistemi) necessitano nella loro applicazione reale di frequenze di campionamento maggiori di $2\omega_m$  ovvero la $\frac {\omega_c} 2>>\omega_m$
### ciò è dovuto dalla pendenza del filtro reale
### quindi dato che la frequenza di campionamento e il tempo di campionamento sono strettamente correlati, osserveremo una variazione del tempo di campionamento, in base a quella che viene identificata come $\omega_m$
### potremo quindi calcolare il Tempo di campionamento sapendo che la frequenza di campionamento è sicuramente $\omega_c>2\omega_m$ e che la stessa può essere scritta come $\omega_c=2\pi \frac 1 {T_c}$  (ricordiamo che il tempo di campionamento è sempre l'inverso della frequenza di campionamento)
#### quindi $T_c>\frac {\pi} {\omega_m}$
### detto ciò sarà comunque necessario osservare altri elementi per la scelta del tempo di campionamento

## Relazione tra tempo di campionamento e quantizzazione
### osservato che il segnale per essere discretizzato, deve essere campionato in istanti distanziati dal tempo di campionamento
### comprendiamo che non debba essere immagazzinata la sola informazione riguardante l'andamento sull'asse del tempo del segnale in ingresso, ma serva anche memorizzare i valori di ampiezza ad i vari istanti
### tale operazione viene denominata quantizzazione e influisce implicitamente anche nella scelta del tempo di campionamento
### infatti sapendo che un numero di intervalli (espressi come $2^{n \ bit}$) siano quelli dedicati alla quantizzazione, capiamo subito che se intercorre una grande variazione di ampiezza dei segnali nel continuo, avremo necessità di tempo di campionamento utile a catturare tutti gli specifici valori quantizzabili con gli $n \ bit$
### ricordiamo quindi che per poter definire la grandezza di un passo di quantizzazione (che indica la differenza di valore di ampiezza catturabile durante la discretizzazione) $q=\frac {\text{ampiezza segnale in ingresso}} {2^n}$
### quindi va da se che il legame tra il passo di quantizzazione e il tempo di campionamento è evidente: se scelgo un tempo di campionamento troppo piccolo e lo abbino ad un passo di quantizzazione troppo grande avrò intervalli di valori che vengono mappati in un unico valore


## Conclusioni
### abbiamo osservato che:
#### il tempo di campionamento dipende dalla frequenza massima del segnale in ingresso
#### il tempo di campionamento se molto grande potrà raccogliere informazioni adeguate solamente per frequenze basse
#### un tempo di campionamento maggiore, permette al microprocessore un'elaborazione più accurata, ma da contro un tempo di campionamento minore permette una precisione nella discretizzazione del segnale maggiore
### è inoltre rilevante il legame tra quantizzazione e tempo di campionamento
### oltre che il legame tra $\omega_m$ (frequenza massima del segnale in esame), $\omega_c$ (frequenza di taglio per il campionamento del segnale) e tempo di campionamento
### inoltre è importante precisare che esso identifica più precisamente:
#### il tempo che comprende conversione ADC, elaborazione (tempo di calcolo del microprocessore) e conversione DAC, più l’intervallo di housekeeping del microcontrollore (tempo necessario al microcontrollore per effettuare delle sue operazioni)
### ![FdA/images/anaolgicoCampionamentoQuantizzazione.png](FdA/images/anaolgicoCampionamentoQuantizzazione.png)
