---
markmap:
  maxWidth: 500
---

# Reiezione di disturbi sinusoidali e specifiche in frequenza

## Introduzione
### sapendo che la funzione di sensibilità serva per comprendere l'effetto della controreazione all'interno di un sistema
### ricordando che la sensibilità diretta si esprime come $S^W_G=\frac{1}{1+H(s)G(s)}$
### riconosciamo $H(s)G(s)$ che sostiuiamo con $F(s)$ come per l'errore di riproduzione di una sinusoide

## Reiezione dei disturbi sinusoidali
### dato che sappiamo che la funzione di sensibilità serve a farci comprendere dell'effetto della controreazione, in particolare, quella diretta serve a osservare il processo in catena diretta
### poniamo una sensibilità massima, che non possa essere superata
### $\frac1{|1+F(s)|}\le S_{\text{max}}$
#### L'uno al denominatore è trascurabile per valori di $F$ elevati

## Passaggio $s=j\omega$
### anche in questo caso per comprendere e tracciare una finestra sul diagramma di Bode
### sostituiamo $s=j\omega$ e potremo quindi osservare che:
#### $\frac1{S_{\text{max}}}\le|F(j\omega)|$

## Conclusioni
### quindi il modulo della funzione d'anello dovrà stare al di sopra della finestra proibita, stavolta identificata con la sensibilità massima come ordinata $S_{\text{max}}$ e la frequenza massima come ascissa $\omega_{\text{max}}$
### ![FdA/images/finestraProibitaSensibilita.png](FdA/images/finestraProibitaSensibilita.png)
