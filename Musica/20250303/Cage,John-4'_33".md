---
markmap:
  initialExpandLevel: 2
  maxWidth: 1000
---

# John Cage - 4' 33"

## John Cage

## febbraio 1948 Silent Prayer
### _I have, for instance, several new desires (two may seem absurd but I am serious about them): first, to compose a piece of uninterrupted silence and sell it to Muzak Co. It will be 3 or 4½ minutes long-those being the standard lengths of “canned” music-and its title will be Silent Prayer. It will open with a single idea which I will attempt to make as seductive as the color and shape and fragrance of a flower. The ending will approach imperceptibility._

## 29 agosto 1952
### Prima esecuzione assoluta
### David Tudor
### Partitura persa

## 5 giugno 1953
### Partitura con notazione proporzionale come regalo di Cage a Irwin Kremen
### In occasione del 28esimo compleanno di Kremen

## 1993
### Pubblicazione della partitura da parte di Edition Peters

## Silenzio?
### _The piece is not actually silent (there will never be silence until death comes which never comes); it is full of sound, but sounds which I did not think of beforehand, which I hear for the first time the same time others hear._

## La partitura
### Il testo introduttivo
#### Introduzione storica
### _for any instrument or combination of instruments._
### La notazione proporzionale
#### ![proportional_notation](proportional_notation_upscayl_2x_realesrgan-x4plus.png)
### Come è organizzata la partitura
#### 3 movimenti
##### 33"
##### 2'40"
##### 1'20"

## Cage ed il brano
### _The white paintings were airports for the lights, shadows, and particles._
### ![Rauscheberg,Robert-White_Painting.jpg](Rauscheberg,Robert-White_Painting.jpg)
### L'esperienza di Cage nella camera anecoica della Harvard University
#### ![John_Cage_in_Harvard_University's_Anechoic_Chamber](John_Cage_in_Harvard_University's_Anechoic_Chamber.jpg)

## Comprendere quindi interpretare
### Interpretazione mutevole e che cambia nel tempo in base all'interprete

## Fonti
### [4'33'' su BiblioLMC](https://bibliolmc.uniroma3.it/node/1153)
### John Cage - _4'33"_ - No. 6777a Edition Peters
### [John Cage’s Frequently Misunderstood 4’33” Remains a Masterpiece](https://www.artnews.com/art-news/news/john-cage-4-33-explained-1234704644/)
### [John Cage 4'33" (In Proportional Notation) 1952/1953](https://www.moma.org/collection/works/163616?artist_id=912&page=1&sov_referrer=artist)
### [Robert Rauschenberg - White Paintings (1951)](https://www.rauschenbergfoundation.org/art/galleries/series/white-paintings-1951)
### [John Cage's works database - 4'33"](https://www.johncage.org/pp/John-Cage-Work-Detail.cfm?work_ID=17)
### [“Silent prayer”, the first silent piece](https://rosewhitemusic.com/piano/2018/08/27/silent-prayer-the-first-silent-piece/)
