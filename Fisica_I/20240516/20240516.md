---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# 20240516

## Introduzione e riepilogo
### abbiamo osservato che per un sistema di punti materiali potremo realizzare una visione punto per punto del sistema, oppure una visione complessiva
### la visione di un sistema come punto unico è realizzata tramite la prima equazione cardinale dei sistemi di punti $\frac {d \vec {\mathbb {P}}} {dt} = \vec F^{\text{esterne}}$
### mentre per osservare un sistema e le forze applicate singolarmente ai singoli punti l'abbiamo osservata tramite la seconda equazione cardinale $\frac {d\vec L_p} {dt} = \vec M^{\text{esterne}_p}$
#### essa abbiamo visto essere la risultante dei momenti delle forze esterne a un dato polo $p$
### in realtà quest'ultima è stata possibile specificarla come $\frac {d\vec L_p} {dt} = \vec M^{\text{esterne}_p}- \vec v_{OP}\times \vec {\mathbb{P}}$
### tale ultimo termine in genere cerchiamo di farlo annullare, scegliendo un polo che ci dia una certa simmetria sul sistema di punti
### ad esempio tale termine si annulla se ci posizioniamo con il polo sul centro di massa, ottenendo quindi quella vista prima $\frac {d\vec L_p} {dt} = \vec M^{\text{esterne}_p}$

## Osservazioni sulle equazioni cardinali
### quindi le due equazioni cardinali, sono simili al II principio della dinamica
### poichè osserviamo che la prima equazione cardinale ($\frac {d\vec P } {dt} = \vec F$), applicando tutte le forze al centro di massa ci darà $\frac {d\vec {\mathbb{P}} } {dt} = \vec F^{\text{esterne}}$
### mentre la seconda equazione cardinale, non la potremo legare al centro di massa
### $\frac {d\vec L}{dt} = \vec M^{\text{esterne}}$ e ricordiamo che la risultante dei momenti delle forze esterne è diversa rispetto al momento della risultante delle forze esternee (applicata nel centro di massa)
#### ciò perchè quando calcoliamo i momenti, il calcolo lo eseguiamo rispetto ad un polo
#### quindi per la seconda equazione cardinale, per ogni punto dovrò prendere ed utilizzare il vettore posizione che collega il polo al punto e calcolarne il singolo momento
### quindi per la seconda equazione cardinale, se provo a osservare il momento della risultante delle forze esterne nel centro di massa, osserverò qualcosa di sbagliato
### ![Fisica_I/20240516/images/01.png](Fisica_I/20240516/images/01.png)
#### osserviamo qui il vettore del momento angolare uscente dal centro di massa
#### è importnate notare che __il momento angolare del centro di massa è diverso dal momento angolare del sistema di punti__
### è quindi importante notare che $\vec L_c \neq \vec L$
### anche per il momento delle forze, il momento del centro di massa sarà diverso dalla risultante dei momenti delle singole forze
### mentre osserviamo che $\vec M_c \neq \vec M^{\text{esterne}}$
### è quindi importante ricordare per il momento delle forze di un sistema di punti __si calcolano prima i momenti dei singoli punti, e poi si sommano fra loro per calcolare il momento complessivo delle forze esterne applicato ad un polo__, ciò perchè i vari punti si trovano a distanze diverse dal polo e per calcolare i momenti in maniera adeguata, sono necessarie le distanze singole dal polo al punto a cui è applicata la forza

## Esempio della porta e momento angolare
### osservando una porta ed applicando la stessa quantità di forza all'estremità della porta, oppure ad un punto più vicino al cardine (quindi con un braccio minore, il braccio ricordiamo essere il vettore posizione tra il polo e il punto in cui è applicata la forza)
### riusciremo a spostare più facilmente la porta prendendo il punto più estremo, ciò equivale ad affermare che __il momento della forza varia in base al punto in cui la forza viene applicata__
### prendendo quindi il cardine della porta come polo, ed avendo inizialmente $\vec L_{\text{iniziale}} =0$, dividendo la porta in tanti pezzetti ed ogni pezzo è un punto materiale
### ![Fisica_I/20240516/images/02.png](Fisica_I/20240516/images/02.png)
### il momento  angolare, applicando la forza (e quindi imprimendo una velocità angolare sul braccio collegato al cardine) diverrà diverso da zero
### se applichiamo la forza sul cardine, allora il momento angolare sarà nullo
### conservandosi la quantità di moto, la risultante delle forze esterne sarà pari a zero, per la seconda equazione cardinale
### se si conserva una quantità di moto, la risultante delle forze esterne sarà pari a zero, ma per la seconda equazione cardinale, il momento angolare $\vec L$ si conserva, se e solo se, la risultante dei momenti delle forze esterne è uguale a zero
### vogliamo che il momento non sia uguale a zero per poter muovere la porta, verrà causata una variazione del momento angolare
###  ![Fisica_I/20240516/images/03.png](Fisica_I/20240516/images/03.png)
### i punti materiali che compongono la porta ruoteranno con una velocità angolare media uscente
### nello specifico, ogni punto che compone la porta avrà una sua propria velocità angolare, tutti i punti materiali giaceranno su di circonferenze e le loro velocità saranno tangenti alle circonferenze su cui sono iscritti e crescenti in modulo
### ![Fisica_I/20240516/images/04.png](Fisica_I/20240516/images/04.png)
#### sarebbe come dire che ogni punto della porta compie un moto circolare uniforme
### ricorderemo anche che $\vec v = \vec \omega \cdot d$ in cui $d$ è la distanza tra il centro e il punto materiale, ovvero lo specifico raggio della circonferenza che staremo andando ad osservare
### quindi, il momento angolare a un certo istante finale, sarà diverso da zero $\vec L_{\text{finale}}\neq 0$
### se il momento angolare è cambiato rispetto all'inizio, deve aver agito un momento delle forze esterne
### quindi $\Delta \vec L \neq 0 \iff \Delta \vec M^{\text{esterne}} \neq 0$ in cui ricordiamo che essa è la risultante dei momenti delle forze esterne applicate ai punti

## Quantificare la variazione $\Delta \vec L$
### abbiamo osservato che la forza, nella schematizzazione vista con la porta, l'avevamo applicata in un singolo punto del sistema di punti studiato
### inoltre abbiamo descritto come la forza abbia una risultante dei momenti diversa in base a dove tale forza viene applicata e cambierà quindi anche in base al polo scelto
### ovviamente potremo scegliere poli diversi per quantificare la variazione del momento angolare, ma sarebbe solamente come utilizzare sistemi di riferimento diversi
### ad esempio cosa accade se prendiamo 3 punti diversi su di una porta a cui applichiamo la forza?
### ![Fisica_I/20240516/images/05.png](Fisica_I/20240516/images/05.png)
### osserviamo che avremo da calcolare tre momenti diversi
### $\vec M(\vec F,p_1)=\vec r_1\times \vec F=0$
#### in pratica se spingo sul cardine, la porta non si muove
### $\vec M(F,p_2)=\vec r_2\times \vec F=\frac l 2 \cdot\vec F \hat z$
#### avremo quindi un momento uscente dal piano con altezza $\frac l 2$ dato che avevamo preso il punto al centro della porta
### $\vec M(F,p_3)=\vec r_3\times \vec F=+l\vec F\hat z$
#### riusciremo in questo caso a generare una leva maggiore, semplicemente spostandoci sul punto di applicazione della forza
### stiamo quindi affermando che __faremo meno fatica a spingere un porta se la spingiamo dal bordo__

## Legge della leva
### potremo affermare dopo l'esempio della porta che quanto visto è esemplificabile anche per mezzo della legge della leva
### in questo esempio due masse, una grande $M$ ed una piccola $m$ sono poste su di un piano
### per far si che la massa piccola equilibri quella grande, sarà necessario scegliere un punto in cui la leva, ovvero il momento della forza peso della massa piccola sia eguagliabile a quello della massa grande
### ![Fisica_I/20240516/images/06.png](Fisica_I/20240516/images/06.png)
### non sono in equilibrio se la massa piccola a meno braccio rispetto al punto centrale (che sarà il nostro polo)
### ![Fisica_I/20240516/images/07.png](Fisica_I/20240516/images/07.png)
### sono in equilibrio se la massa piccola a maggior braccio rispetto al polo di quella grande

## Come esprimere la conservazione di tre quantità fisiche: leggi di conservazione
### $\boxed{1}$ conservazione dell'energia meccanica $\Delta E = 0 \iff L_{\text{forze non conservative}}=0$
### $\boxed{2}$ conservazione della quantità di moto $\Delta \vec {\mathbb{P}} = 0 \iff \vec F^{\text{esterne}}=0$
### $\boxed{3}$ conservazione del momento angolare $\Delta \vec L = 0 \iff \vec M^{\text{esterne}}=0$
### in pratica quando la derivata è nulla nella seconda equazione cardinale dei sistemi di punti (con tale derivata calcoliamo a partire dal momento angolare, il momento delle forze) potremo afferemare che $\vec L_A=\vec L_B$ in cui $A$ è lo stato iniziale del sistema e $B$ è lo stato finale del sistema
### quindi se vi è conservazione del momento angolare, all'istante iniziale ed a quello finale avremo lo stesso momento angolare
### per _un singolo punto materiale_ osserviamo che la $\boxed{2}\Rightarrow \boxed{3}$ perchè il fatto che la risultante delle forze esterne sia nulla influenza direttamente il calcolo del momento delle forze che sarà $\vec M^{\text{esterne}}=\vec r\times \vec F^{\text{esterne}}$ in cui $\vec F^{\text{esterne}}=0$
#### ciò implicherà che $\Delta \vec L=0$
#### quindi __per un singolo punto materiale se si conserva la quantità di moto, allora si conserverà il momento angolare__
### per __due punti materiali__ invece vedremo che $\Delta \vec {\mathbb{P}}=0 \iff \vec F^{\text{esterne}}$ ma ciò è possibile solamente se $\vec F_1^{\text{esterne}}=\vec F_2^{\text{esterne}}$
#### quindi il momento delle forze esterne sarà $\vec M^{\text{esterne}}=\vec r_1\times \vec F_1^{\text{esterne}}+\vec r_2\times \vec F_2^{\text{esterne}}$
#### staremo quindi affermando che $\Delta \vec L \neq 0$
### possiamo inoltre osservare che la quantità di moto di un sistema non cambia se ci poniamo sul centro di massa, ma è importante notare che quando il corpo ruota (come una giostra), il suo momento angolare però cambia perchè il corpo starà accelerando
### quindi __per una giostra o un qualsiasi sistema di punti che ruota, varia il momento angolare ma la quantità di moto si conserverà__

## Esempio per l'interpretazione della II equazione cardinale: coppia di forze
### andiamo ora ad analizzare la coppia di forze esterne applicate su due punti materiali
### ![Fisica_I/20240516/images/08.png](Fisica_I/20240516/images/08.png)
### andando a prendere due forze esterne parallele avremo che $\vec F_1 = - \vec F_2$
### la risultante dei momenti delle forze esterne come la dovremo calcolare?
### potremo scegliere un polo qualsiasi
### inoltre avremo che $\vec F^{\text{esterne}}=0$ ed inoltre $\vec {\mathbb{P}}=0$
### cerchiamo di realizzare una situazione in cui tutti gli effetti, oltre quelli delle forze, non inficiano sul sistema
### ![Fisica_I/20240516/images/09.png](Fisica_I/20240516/images/09.png)
#### ricordiamo anche se non c'entra nulla $\cos (\Omega t+\phi)$
#### ed anche che $\omega =\frac {d\Theta} {dt}$
### vedremo che $\vec M_1 = \vec r_1 \times \vec F_1^{\text{esterne}}$ ed inoltre $\vec M_2= \vec r_2 \times \vec F_2^{\text{esterne}}$
### quindi la risultante dei momenti sarà pari a $\vec M^{\text{esterne}} = \vec r_1 \times \vec F_1^{\text{esterne}}+\vec r_2 \times \vec F_2^{\text{esterne}}$
### inserendo quindi $\vec F_1^{\text{esterne}}=-\vec F_2^{\text{esterne}}$
### avremo $\vec M^{\text{esterne}} = \vec r_1 \times \vec F_1^{\text{esterne}}-\vec r_2 \times \vec F_1^{\text{esterne}}$
### che sarebbe praticamente $\vec M^{\text{esterne}} = (\vec r_1-\vec r_2)\times \vec F_1^{\text{esterne}}$
### ![Fisica_I/20240516/images/10.png](Fisica_I/20240516/images/10.png)
### osserviamo che potremo riscriverla come $\vec M^{\text{esterne}} =\vec {\Delta r} \times \vec F_1^{\text{esterne}} = \vec M^{\text{esterne}} = -\vec {\Delta r}\times \vec F_2^{\text{esterne}}$
### quando si ha una __coppia di forze, ovvero due forze applicate in due punti diversi con forze uguali ed opposte, il centro di massa starà fermo ma la risultante dei momente delle forze esterne sarà diversa da zero__
### in modulo avremo $M^{\text{esterne}} = +{\Delta r}\times F \hat z$ con $\Delta r = |\vec r_1-\vec r_2|$ ed inoltre $F=|F_1|=|F_2|$
### quindi avendo una coppia di forze applicate in due punti, se il sistema è isolato, allora sarebbe come se i punti fossero fermi ed abbiano applicate due forze ma con versi opposti
### quindi il momento della coppia di forza sarà $\Delta r\cdot F \cdot \sin (\Theta)\cdot \hat z$ e avremo che il  braccio sarà $b=\Delta r \sin(\Theta)$
### inoltre potremo sempre scrivere $\text{Momento della coppia di forze}=\text{braccio}\cdot \vec {\text{Forza}}$

## Esempio della sbarretta con due palle da tennis
### avendo una sbarretta di massa trascurabile e lunghezza $d$ con due palline di massa $m$ da tennis attaccate agli estremi
### ne osserviamo il centro in cui la sbarretta è vincolata con un asse
### ![Fisica_I/20240516/images/11.png](Fisica_I/20240516/images/11.png)
### il punto di mezzo che le vincola è anche il centro di massa
### avremo quindi la velocità angolare $\vec \omega$ uscente dal piano e le velocità come mostrato con versi opposti rispetto all'asse fissato $\hat y$
### ![Fisica_I/20240516/images/12.png](Fisica_I/20240516/images/12.png)
### esso è un sistema completamente isolato e non vi sono forze esterne
### avremo quindi che $\vec F_i = 0 \forall_i = 1,2$
### quindi se non vi sono forze esterne ci siamo assicurati che $\Delta \vec {\mathbb{P}}=0$ ed inoltre $\Delta \vec L=0$ inoltre non vi sono momenti di forze
### andando a calcolare la quantità di moto complessiva avremo $\vec {\mathbb{P}} = m\vec v_1+m\vec v_2=0$
#### quindi $\vec v_1 = \omega \frac d 2 \hat y$
#### $\vec v_2 = -\omega \frac d 2 \hat y$
### le velocità delle due masse saranno quindi uguali ed opposte
### il centro di massa sarà sempre fermo
### quindi il momento angolare totale sarà $\vec L =\vec r_1\times m\vec v_1+\vec r_2 \times m\vec v_2$ ovvero la somma dei momenti delle singole masse
### ![Fisica_I/20240516/images/13.png](Fisica_I/20240516/images/13.png)
### quindi $\vec r_2 = -\vec r_1$ e $\vec v_2 = -\vec v_1$
### in definitiva avremo che ruotando in senso antiorario che il momento angolare sarà $\vec L =\vec r_1\times m\vec v_1-\vec r_1 \times -m(\vec v_1)=\vec r_1\times m\vec v_1+\vec r_1 \times m(\vec v_1)=2\vec r_1 \times m \vec v_1 = 2\vec r_2 \times m\vec v_2=2\frac d 2 m \omega \frac d 2 \hat z=m \omega \frac {d^2} 2 \hat z$
### inoltre sappiamo che $\vec \omega = \omega \cdot \hat z$
### quindi potremo scrivere che $\vec L = \frac {md^2}2 \cdo \vec \omega$
### in pratica il momento angolare totale è in questo caso parallelo a $\vec \omega$ che giace sull'asse $\hat z$ perpendicolare al piano su cui sono appoggiate le due masse
### quindi __in questo specifico caso il momento angolare totale è parallelo alla velocità angolare, in generale ciò non è quasi mai vero!__
### osserviamo inoltre che abbiamo caratterizzato un sistema in cui la quantità di moto era pari a zero e il momento angolare è parallelo alla velocità angolare
### quindi per conservare il momento angolare, cambiando la distanza fra le due palline, si dovrà conservare la quantità di moto, quindi allungando $d$ avremo velocità angolare minore, accorciando $d$ avremo velocità angolare maggiore dell'inizio

## Conservazione del momento angolare: il ballerino
### approssimando quindi un ballerino che gira su una gamba in equilibrio con le braccia aperte, avendo due masse alle estremità delle braccia, che sono le due mani
### ![Fisica_I/20240516/images/14.png](Fisica_I/20240516/images/14.png)
### potremo osservare che chiudendo le braccia la velocità angolare sarà maggiore dello stato $A$ iniziale
### ![Fisica_I/20240516/images/15.png](Fisica_I/20240516/images/15.png)
### ciò avviene per effetto delle sole forze interne al sistema di punti
### sappiamo che il sistema è isolato quindi $\Delta \vec L =0$ e $\vec L_A = \vec L_B$
### proiettando sulll'asse $\hat z$ avremo $z: \frac {md^2_A} 2\cdot \vec \omega_A = \frac {md^2_B} 2\cdot \vec \omega_B$
### per far conservare il momento angolare avremo $d^2_A\cdot \vec \omega_A = d^2_B\cdot \vec \omega_B$
### __riducendo la distanza fra le due masse allora la velocità angolare dovrà aumentare__ ed osserviamo che $\frac {\omega_B} {\omega_A} = \Big( \frac {d_A} {d_B}\Big)^2$
### in pratica diminuendo la distanza di poco, avremo un aumento del rapporto tra le due velocità angolari quadratico del rapporto tra le due distanze

## Esempio: il tuffatore
### ![Fisica_I/20240516/images/16.png](Fisica_I/20240516/images/16.png)
### il centro di massa del tuffatore fa il suo moto parabolico
### nel frattempo se il tuffatore si mette in rotazione in aria raccogliendosi ridurrà la sua lunghezza $d$
### aumentando quindi la sua velocità angolare in aria con un asse perpendicolare al piano in cui realizza il moto parabolico del tuffo

## Esercizio sulla delle masse e della gravitazione universale (svolto)
