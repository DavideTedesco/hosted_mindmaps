# Istruzioni per la realizzazione delle mappe mentali per Fisica I

- suddividere sempre per macro argomenti dalla radice che è il giorno
- inserire gli svolgimenti degli esercizi, solamente laddove essi sono indispensabili per la comprensione teorica
- non essere troppo prolissi, si tratta di schemi e non di riscrittura
- confrontare varie fonti ed al limite ascoltare la registrazione della lezione

# Istruzioni per la compilazione offline delle mappe mentali per Fisica I

- utilizzare `markmap-cli`
- compilare sempre con opzione `--offline`
- aggiungere al file per compilazione offline "-offline" alla fine
- ricordarsi di aggiungere nello stile dei file .html (per far vedere bene gli schemi sulla dark mode)
  ```
  html {
  background-color: white;
  }
  ```
- ricordarsi di cambiare sempre il percorso delle immagini per la compilazione locale
