---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# 20240527

## Accenni alla gravitazione e alla forza di gravitazione
### dobbiamo ricordare che per la legge di gravitazione universale $F=G \frac {M_1 M_2} {d^2}$
### in  cui $d$ è la distanza geometrica fra i due corpi di massa $M_1$ ed $M_2$
### ![Fisica_I/20240527/images/01.png](Fisica_I/20240527/images/01.png)
### la forza di gravità è inoltre un'approssimazione di questa vista in cui $d=h+R$
### in particolare vedremo che la forza peso sarà $F_P=F= G \frac {M_{\text{terra}}m} {d^2}$ e potremo vedere che $mg=  G \frac {M_{\text{terra}}m} {d^2}$
### semplificando le masse dell'oggetto attratto dalla terra otterremo $gd^2 = GM_{\text{terra}}$
### in cui in realtà la distanza sarebbe $d=h+R$ e quindi $g(h+R)^2= GM_{\text{terra}}$ ma ponendo $h=0$ avremo $gR_{\text{terra}}^2= GM_{\text{terra}}$
###  ![Fisica_I/20240527/images/02.png](Fisica_I/20240527/images/02.png)

## Riepilogo ed introduzione
### abbiamo osservato cosa sia e alcuni casi del calcolo del momento d'inerzia posizionandoci sul centro della massa
### ma cosa accade se invece di posizionarci sull'asse di simmetria $\hat z$ dell'oggetto scelto, andiamo a prendere un altro asse parallelo ad esso ma non passante per il centro di massa $\hat u$?
###  ![Fisica_I/20240527/images/03.png](Fisica_I/20240527/images/03.png)

## Teorema di Huygens-Steiner: introduzione
### osservando quindi l'asse di simmetria di un oggetto qualunque posizionato nel centro di massa e andando a prenderne un'altro non nel centro di massa
### avremo vincolato l'oggetto per un altro asse
### ![Fisica_I/20240527/images/04.png](Fisica_I/20240527/images/04.png)
### potremo in qualche modo calcolare il momento d'inerzia ora che abbiamo spostato l'asse?
### quindi noto $I_z$ vogliamo trovare $I_u$
### vediamo infatti che $I_u$ sarà sicuramente diverso da $I_z$
### ![Fisica_I/20240527/images/05.png](Fisica_I/20240527/images/05.png)
### $I_u$ sarà un momento d'inerzia sicuramente maggiore se si ha una massa omogenea
### in particolare dimostreremo che $I_u = I_z + MD^2$ in cui $M$ è la massa del corpo e $D$ è la distanza dell'asse $u$ dall'asse $z$

## Introduzione alla dimostrazione del Teorema di Huygens-Steiner
### per poter svolgere e osservare la dimostrazione del Teorema che ci permette di esprimere la relazione tra il momento d'inerzia di un'asse posto nel centro di massa ed uno posto in altre posizioni parallelo ad esso
### prendiamo un punto $P$ sullo stesso piano di $C$ (ovvero il centro di massa) e potremo vedere che tale dimostrazione sarà valida anche per altri punti $P$ su altri piani
### ![Fisica_I/20240527/images/06.png](Fisica_I/20240527/images/06.png)
### quindi gli assi saranno perpendicolari a tale piano
### quindi dovremo calcolare il momento d'inerzia rispetto alla distanza dell'assa dal punto e non della quota
###  ![Fisica_I/20240527/images/07.png](Fisica_I/20240527/images/07.png)
### in cui osserviamo che $\vec R$ è il vettore fra i due punti scelti sul piano
### $\vec r$ è il vettore fra $P$ e l'elemento di massa scelto
### $\vec r'$ è il vettore tra il centro di massa e l'elemento di massa
### è importante ricordarsi che __il momento d'inerzia non dipende dalla poszione del polo all'interno del corpo, ma dalla distanza dell'asse rispetto ad esso__

## Dimostrazione del Teorema di Huygens-Steiner
### prendendo quindi un elemento di massa
### 2 sistemi di riferimento
### i vettori rispetto a $p$ e $c$ che legano il i punti all'elemento di massa
### e il vettore che lega $p$ e $c$
### e supponendo un sistema di punti discreto (e non continuo altrimenti avremo bisogno dell'integrale)
### osserviamo che $I_u= \sum_{i=1}^N m_i \cdot r_i^2$
### _ovviamente cambiando piano avremo altri vettori e altri punti analoghi a quelli presi ma sull'altro piano_
#### ![Fisica_I/20240527/images/08.png](Fisica_I/20240527/images/08.png)
### inoltre ricordiamo che $\vec r=\vec R+\vec r'$
### sappiamo anche che dal secondo Teorema di König che la $K=\frac 1 2 mv^2$
### allora potremo scrivere che $I_u=\dots=\sum_i m_i |\vec r_i|^2=\sum_i m_i \vec r_i \cdot \vec r_i$
### andando ad inserire $\vec r=\vec R+\vec r'$ otterremo $I_u=\sum_i m_i(\vec R+\vec r_i')(\vec R +\vec r_i')=\textcolor{blue}{\sum_im_i R^2} +\textcolor{green}{\sum_i m_i r_i^{'2}}+\textcolor{red}{2\sum_i m_i\vec r_i'\vec R}$ in cui abbiamo praticamente svolto il prodotto del quadrato di binomio (dato che il prodotto scalare è commutativo)
### potremo quindi guardarla e riconoscere che il primo termine è $\textcolor{blue}{\sum_im_i R^2} =\textcolor{blue}{MR^2}$ e sarebbe come dire che abbiamo calcolato il momento d'inerzia rispetto al centro di massa al nuovo asse
### il secondo termine invece è proprio il momento d'inerzia rispetto all'asse $z$ ovvero $\textcolor{green}{\sum_i m_i r_i^{'2}} = \textcolor{green}{I_z}$
### il terzo termine è nullo, $\textcolor{red}{2\sum_i m_i\vec r_i'\vec R}=0$, esso diviene nullo perchè moltiplicando e dividendo per la massa totale $M\frac {2(\sum_i m_i \vec r_i')\vec R} {M}=\vec r_c\cdot \vec R$ avremo la posizione del centro di massa rispetto al sistema del centro di massa che sappiamo essere nulla
### quindi $I_u= I_z+MR^2$ in cui $M$ è la massa e $R$ è la distanza tra i due punti $p$ e $c$

## Esempio della sbarretta con cardine su di un lato (come una porta)
### osservando una sbarretta ed avendo un asse posto su un estremo della stessa, potremo ora calcolarne il momento d'inerzia rispetto al nuovo asse
### ![Fisica_I/20240527/images/09.png](Fisica_I/20240527/images/09.png)
### ci ricordiamo che per la sbarretta il momento d'inerzia è $I_z=\frac 1 {12} ML^2$
### allora quello $I_u = \frac 1 {12} ML^2+M\big({\frac L 2}\big)^2$ osservando che $z$ è sul centro di massa

## Pendolo fisico (o pendolo composto)
### si definisce pendolo fisico un __qualsiasi corpo di forma arbitraria con asse orizzontale all'accelerazione di gravità e passante per il centro di massa__
### ![Fisica_I/20240527/images/10.png](Fisica_I/20240527/images/10.png)
### ad esempio un quadro se appeso per il suo centro di massa può esser visto come un pendolo fisico
### osserviamo infatti che su un oggetto di questo tipo, se l'asse non è posto sul centro di massa, non starà fermo
### sarà quindi libero di oscillare
### ma come trovare il centro di massa di un tale oggetto?
#### per trovare il centro di massa, vincoliamo l'oggetto per un punto e poi per un altro punto
#### tracciando la retta fra essi, e trovando il punto dove essi si congiungo potremo trovare il centro di massa
### il corpo è soggetto a due forze
### la forza peso, che agisce nel centro di massa e la reazione vincolare del pendolo fisico, che non è semplice da individuare e potrebbe essere in vari punti su di un'asse
### ![Fisica_I/20240527/images/11.png](Fisica_I/20240527/images/11.png)
### per ottenere l'equilibrio statico sappiamo che dobbiamo avere $\vec F^{\text{esterne}}=0$ e $\vec M^{\text{esterne}}=0$
### allora dovremo essere sicuri che se agiscono due forze $\vec F_p = \vec R$ ovvero sono uguali ed opposte
### avremo quindi per l'equilibrio
### ![Fisica_I/20240527/images/12.png](Fisica_I/20240527/images/12.png)
### che in realtà presume che la forza peso e la reazione vincolare siano entrambe sulla stessa direzione e vorrebbe dire che il momento angolare di $\vec R$ sia nullo
### in realtà abbiamo visto prima che anche se abbiamo le forze parallele e con versi opposti, non è detto che vi sia una situazione di equilibrio
### prendendo $O$ come polo, vedremo che $\vec M-{\vec F_p}$ non sarà nulla
### ![Fisica_I/20240527/images/13.png](Fisica_I/20240527/images/13.png)
### ma perchè tali rette si incontrano e quindi le forze siano uguali ed opposte, esse dovranno essere la stessa
### avremo quindi che le uniche situazioni di equilibrio statico potranno essere quelle di seguito
### in cui per avere l'equilibrio si hanno $\vec F_p$ ed $\vec R$ allineate sulla stessa direzione
### ![Fisica_I/20240527/images/14.png](Fisica_I/20240527/images/14.png)

## Cosa accade ad un oggetto, come quello usato per il pendolo fisico, se lo iniziamo a far oscillare intorno all'asse di rotazione?
### per poter far oscillare un oggetto come quello visto (con assenza di attrito)
### basterà spostarlo di poco dalla sua situazione di equilibrio statico
### ![Fisica_I/20240527/images/15.png](Fisica_I/20240527/images/15.png)
### quindi per analizzare il problema dell'oscillazione dovremo mettere il corpo in una situazione in cui non è all'equilibrio
### vorremmo quindi trovare in qualche modo le equazioni del pendolo
### osserviamo le equazioni cardinali e vediamo che per la prima equazione cardinale abbiamo $\vec F^{\text{esterne}}\neq 0$
### ![Fisica_I/20240527/images/16.png](Fisica_I/20240527/images/16.png)
### vediamo infatti che la tensione del pendolo va verso il punto in cui esso è attaccato e potrà quindi essere scomposta la forza totale in una componente tangenziale ed in una radiale
### ![Fisica_I/20240527/images/17.png](Fisica_I/20240527/images/17.png)
### osserviamo quindi che la tensione va verso il cavo che tiene la massa
### inoltre abbiamo che la forza totale (e quindi le sue componenti $\nu$ e $\tau$) cambiano istante per istante
### potremo quindi scrivere per la seconda equazione cardinale $\vec M^{\text{esterne}}= \frac {d\vec L}{dt}$
### sapendo che $\vec L \neq c\cdot \vec \omega$ ovvero che il momento angolare non è proporzionale alla velocità angolare
### proiettando lungo l'asse $\hat z$ di rotazione il momento avremo $z: M_{O,z}^{\text{esterne}}= \frac {dL_z}{dt}=I_z\frac {d\omega} {dt}$
### e essa ricordiamo varrà solamente per la componente lungo l'asse di rotazione $\hat z$ quindi per essa __non vi dovrà essere il simbolo vettore__
### poniamo $\hat z \parallel \vec \omega$
### ora potremo integrare scegliendo $O$ come polo
### ottenendo $M_{O,z}^{\text{esterne}}= I_z\frac {d\omega}{dt}$
### andando a calcolare il momento della forza peso rispetto a $\hat z$ avremo $M_{O,z,F_p}=-d\cdot Mg\cdot \sin(\Theta)$
### e tale momento della forza peso (in particolare il suo segno) varrà per entrambe le situazioni, sia che il corpo è a destra che a sinistra dell'asse di rotazione
### potremo quindi riscrivere che $-dMg\sin(\Theta)=I_z\ddot\Theta$ che è praticamente come l'equazione del pendolo visto in precedenza

## Approssimazione per piccolo angolo del pendolo composto
### ricordiamo che per piccoli angoli possiamo approssimare $\sin(\Theta)\approx \Theta$
### allora potremo scrivere $\ddot \Theta \approx - \textcolor{red}{\frac {dMg} {I_z}} \cdot \Theta$ in cui $\textcolor{red}{\frac {dMg} {I_z}=\Omega^2}$ ovvero alla pulsazione del moto armonico
### ed otterremo quindi l'equazione differenziale di un oscillatore armonico
### $\ddot \Theta = -\Omega^2\Theta$
### e quindi $\Theta(t)=\Theta_0\cos(\Omega t+\phi)$ (_utilizzata in molti esercizi_)
### allora per calcolare il periodo ricordiamo che $T=\frac {2\pi}{\Omega}$ che nel nostro caso sarà $T=2\pi \sqrt{\frac {I_z} {d\cdot Mg}}$
### e per il Teorema di Huygens-Steiner sappiamo che $I_z=I_c+Md^2$ quindi $I_z$ è sempre la somma di una massa per una lunghezza al quadrato
### quindi $I_z=\dots=kMl^2+Md^2$ in cui $k$ è una costante
### avremo allora per il periodo $T=2\pi \sqrt{\frac {\text{lunghezza}}{g}} = 2\pi\sqrt{\frac {\text{lunghezza efficace}} {g}}$
### quindi un qualsiasi oggetto appesso sarà analogo ad un pendolo semplice con una lunghezza che dipende da come è posizionato il pendolo

## Esercizio d'esame del 6 settembre 2023, problema d'esame n.2
### ![Fisica_I/20240527/images/18.png](Fisica_I/20240527/images/18.png)
### svolgimento
#### ![Fisica_I/20240527/images/19.png](Fisica_I/20240527/images/19.png)

## Esercizi da svolgere dalla lista del Borghi
### n.9
### n.10

## Esercizio del Borghi dalla lista n.34 (svolto)
