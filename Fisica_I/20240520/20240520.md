---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# 20240520

## Riepilogo ed introduzione
### abbiamo osservato sino ad ora le equazioni cardinali per i sistemi di punti
### la prima equazione cardinale è $\frac {d\vec {\mathbb{P}}} {dt} = \vec F^{\text{esterne}}$ legata quindi alla quantità di moto e relativa alla sommatoria delle forze esterne applicate ad un punto
### la seconda equazione cardinale è invece $\frac {d\vec L} {dt} = \vec M^{\text{esterne}}$
#### in pratica la risultante dei momenti delle forze esterne
#### ricordiamo che prima si calcola il momento delle forze di un punto, e poi lo si somma a tutti gli altri del sistema
### osservando una sbarretta di lunghezza $d$, avevamo visto che inserendo due masse $m$ alla stessa distanza dal centro, con un asse $z$ centrale, avremo il momento angolare $\vec L$ parallelo alla velocità angolare $\vec \omega$
### ![Fisica_I/20240520/images/01.png](Fisica_I/20240520/images/01.png)
### quindi $\vec  L = \frac {md^2} 2 \cdot \vec \omega$
#### quindi il momento angolare è proporzionale a $\vec \omega$  
### quindi se $\vec \omega$ sarà costante avremo un moto circolare uniforme e il momento angolare sarà sarà proporzionale a $\vec \omega$
###  questa tipologia di modellazione è utile anche quando viene cambiata l'estensione della sbarretta $d$
### quindi dovendosi conservare il momento angolare e la quantità di moto, con una sbarretta di lunghezza maggiore, si avrà $\vec \omega$ maggiore
### il sistema ora visto ammette equilibrio dinamico ed è possibile osservarlo in natura, inoltre senza attriti il corpo visto potrebbe continuare a ruotare in eterno
### in questo caso avremo che $\vec M^{\text{esterne}} \equiv 0$ ed anche che $\vec \omega =0$

## Calcolo del momento angolare per una sbarretta con due palline attaccate agli estremi inclinata di un angolo $\Theta$
### osserviamo ora un altro caso, in cui il sistema non ammette una situazione di equilibrio dinamico con $\vec \omega$ costante
### ora il sistema per poter ruotare deve essere messo in rotazione
### ruotando su se stesso intorno al punto centrale della sbarretta
### ![Fisica_I/20240520/images/02.png](Fisica_I/20240520/images/02.png)
### osserviamo quindi che le due masse compieranno due circonferenze e potremo osservare che le velocità sono tangenziali a tali circonferenze
### ![Fisica_I/20240520/images/03.png](Fisica_I/20240520/images/03.png)
### quindi se la sbarretta è lunga $d$ avremo che il raggio sarà $\frac d 2\sin \Theta$
### ora ci concentriamo ad osservare la massa sopra (che chiamiamo per semplicità $m_1$)
### vedremo che $\vec L = \vec L_1 + \vec L_2= m\vec r_1 \times \vec v_1 + m \vec r_2 \times \vec v_2$
### quindi per comprendere la direzione risultante di $\vec L$ andremo a vedere dove sono applicate le velocità riconoscendo che il piano dal quale uscirà il vettore del momento angolare, sarà quello perpendicolare a $\vec v_1$ e $\vec r_1$
### notiamo, guardando meglio che la velocità è perpendicolare alla sbarretta
### ![Fisica_I/20240520/images/04.png](Fisica_I/20240520/images/04.png)
### quindi la direzione del momento angolare $\vec L$ sarà perpendicolare al piano su cui sono iscritti $\vec r_1$ e $\vec v_1$
### dato che $\vec L = + |\vec r_1|m\vec v_1 \sin(\frac {\pi} 2) \hat z'+\dots$
### ![Fisica_I/20240520/images/05.png](Fisica_I/20240520/images/05.png)
### osservando che i due momenti angolari hanno stessa direzione sull'asse $\hat z'$ il momento angolare complessivo sarà  $\vec L = + |\vec r_1|m\vec v_1 \sin(\frac {\pi} 2) \hat z'+|\vec r_2|m\vec v_2 \sin(\frac {\pi} 2) \hat z'$ ovvero sapendo che $|\vec r_1|= |\vec r_2|=\frac d 2$ e $|\vec v_1| = |\vec v_2| = \omega \frac d 2\sin(\Theta)$
### otterremo (dato che $\sin(\frac {\pi} 2)=1) $\vec L = 2 \frac d 2 m\omega \frac d 2 \sin(\Theta)\hat z'$
### mentre precedentemente veniva $\vec L = \frac {md^2} 2 \vec \omega$ ora potremo generalizzarlo, avendo come modulo $|\vec L| = m\frac {d^2} 2 \omega \sin (\Theta) \hat z'$
### in pratica staremo dicendo i due momenti angolari delle due masse sono paralleli e li potremo sommare ottenendo il momento angolare complessivo
### andando poi a sostituire il valore $\Theta = \frac {\pi} 2$ potremo avere il caso orizzontale già visto in precedenza, ovvero quando il momento angolare era parallelo all'asse $\hat z$
### staremo praticamente dicendo che ruotando la sbarretta di $\Theta$ staremo introducendo una dipendenza implicita da essa
### per $\Theta = \frac {\pi} 2 \Rightarrow |\vec L| = m\frac {d^2} 2 \omega \cdot 1 \hat z'$ quindi praticamente sarebbe come dire che $\hat z'$ sia completamente verticale
### ma __per $\Theta$ generico quindi $\Theta \neq \frac {\pi} 2$ non varrà mai $\omega \hat z'=\vec \omega$__
### quindi osservando come abbiamo sempre posizionato 3 assi per vederli bidimensionalmente con angoli a 90º fra loro
#### ![Fisica_I/20240520/images/06.png](Fisica_I/20240520/images/06.png)
### vedremo che la $\vec L$ sarà perpendicolare alla sbarretta, ovvero il momento angolare sarà inclinato di $\Theta$ rispetto al piano orizzontale
### ![Fisica_I/20240520/images/07.png](Fisica_I/20240520/images/07.png)
### riassumendo: _in generale il momento angolare non è parallelo alla velocità angolare $\vec \omega$

## Osservazioni sulle masse in rotazione
### quindi mentre il sistema con $\Theta =\frac {\pi} 2$ ammette un equilibrio dinamico
### il sistema con $\Theta\neq \frac {\pi} 2$ non ruotare da solo
### potremo inoltre osservare che il momento risultante trovato, lo potremo porre nel centro della sbarretta $d$ e potremo suddividerlo in componenti perpendicolari e parallele alla velocità angolare $\vec omega$
### ![Fisica_I/20240520/images/08.png](Fisica_I/20240520/images/08.png)
### se $\Theta \neq \frac {\pi} 2$ allora scomponendo il momento angolare $\vec L$, potremo comunque scomporlo in componenti e trovare una componente parallela a $\vec \omega$ ed una perpendicolare ad esso
### quindi ci ricorderemo sempre che $|\vec L| = \frac {md^2} 2 \omega \sin (\Theta)$
### dato che la direzione del momento angolare non è costante complessivamente, esso ruoterà insieme alla sbarretta
### nello specifico vedremo che la componente parallela a $\vec \omega$ non ruota e rimane costante $\vec L \parallel \vec \omega= \text{costante}$
### la componente perpendicolare a $\vec \omega$ ruota $\vec L \perp \vec \omega= \text{ruota}$ quindi tale componente andrà a descrivere una circonferenza, capiamo quindi che la componente perpendicolare in questo caso potrà cambiare direzione e verso
### ![Fisica_I/20240520/images/09.png](Fisica_I/20240520/images/09.png)


## Combinazione delle compoenti del momento angolare $\vec L$ per $\Theta \neq \frac {\pi} 2$ e osservazioni sulla II equazione cardinale dei sistemi di punti
### notiamo che per far ruotare tale sistema, bisognerà imprimere in qualche modo una forza, esso infatti non si muoverà da solo
### chiamando l'asse su cui giace la componente perpendicolare ${\nu}$ e l'altro parallelo alla velocità angolare $z$ proiettando lungo gli assi il momento angolare potremo studiare cosa accade per la seconda equazione cardinale in componenti
### ![Fisica_I/20240520/images/10.png](Fisica_I/20240520/images/10.png)
### vedremo che $\begin{cases}z: \frac {dL_z} {dt} = 0\cdot M_z^{\text{esterne}}\\ \nu:\frac {dL_{\nu}} {dt} = M^{\text{esterne}} \end{cases}$
### stiamo in pratica dicedno che per la componente perpendicolare, il momento angolare non si conserva, quindi i momenti lungo $\nu$ non si conserveranno
### vi sono quindi alcuni sistemi che esercitano un momento che permette di far ruotare la componente che giace su $\nu$
### si avrà quindi che $dL_\nu = M^{\text{esterne}}_{\nu}dt$ che potremo anche osservare dall'alto
### ![Fisica_I/20240520/images/11.png](Fisica_I/20240520/images/11.png)
### potremo quindi osservare che avremo un moto circolare del punto (osservando una delle due masse) e avremo necessità di un incremento perpendicolare al vettore stesso, per far si che il sistema si muova
### ciò lo potremo fare applicando una velocità tangenziale alla circonferenza (come per il moto circolare uniforme)
### ![Fisica_I/20240520/images/12.png](Fisica_I/20240520/images/12.png)
### vedremo che la velocità è perpendicolare a $\vec r$ per farlo ruotare
### quindi $\vec M^{\text{esterne}}_{\nu}dt$ dovrà essere perpendicolare a $\vec L_{\nu}$
### in pratica il momento delle forze esterne è tangente alla componente perpendicolare alla velocità angolare del momento angolare; ciò sarebbe uguale alla velocità del moto circolare uniforme, che è tangente al raggio della circonferenza

## Che forze dovremo applicare per avere un momento delle forze tangente alla circonferenza iscritta dalla massa in rotazione?
### osservando la massa $m_2$, ovvero quella in basso, ricordiamo che il momento della risultante delle forze applicata in tale punto sarà pari a $\vec M_2^{\text{esterne}} = \vec r_2 \times \vec F_2$
### ![Fisica_I/20240520/images/13.png](Fisica_I/20240520/images/13.png)
### quindi avrà un momento delle forze tangente alla circonferenza di $m_2$
### sappiamo che il vettore $\vec r_2$ congiunge la massa al centro di $d$ la proiezione di tale vettore sulla circonferenza che compie la massa, traccerà un'altro vettore radiale alla circonferenza e rivolto verso l'interno, permetterà insieme al momento delle forze, di ruotare sulla circonferenza
### osservando con $\vec \omega$ che ruota in senso orario potremo vederelo dall'alto
### ![Fisica_I/20240520/images/14.png](Fisica_I/20240520/images/14.png)
### e potremo vedere il momento delle forze anche lateralmente, come vettore entrante nel piano
### ![Fisica_I/20240520/images/15.png](Fisica_I/20240520/images/15.png)
### la forza da applicare, sarà come quella disegnata ed il vettore sarà entrante, vediamo infatti che andando a realizzare il prodotto vettoriale $\vec r_2 \times \vec F_2$ otterremo un vettore momento delle forze esterne, entrante (secondo la regola della mano destra)
### staremo in pratica indicando che il sistema non può essere isolato e ruotare se la sbarretta è storna, quindi la risultante delle forze esterne sarà sicuramente diversa da zero

## Dalle coppie di forze al momento angolare
### avendo trattato precedentemente le coppie di forze parallele opposte
### vedremo che possiamo raffiugurare e quindi imporre due forze parallele opposte nei punti in cui vi sono le due masse
### ![Fisica_I/20240520/images/16.png](Fisica_I/20240520/images/16.png)
### osserveremo che la risultante dei momenti si sommano, allora il momento angolare sarà pari alla coppia dei momenti
__inserire momento risultante coppia__

## Accelerazione e momento delle forze
### per comprendere l'accelerazione del centro della sbarretta $d$ osserviamo che $\vec M \cdot \vec a_c = \frac {d\vec {\mathbb{P}}} {dt} = \vec F^{\text{esterne}}$
### ciò equivale a dire che preso il centro di massa, per la seconda equazione cardinale, il momento delle forze esterne al sistema è dato dalla quantità di moto totale del sistema derivata, ovvero la variazione delle forze esterne al centro di massa
### quindi potremo inventarci una coppia di forze esterne fittizie che applicate agli estremi del sistema si annullano fra loro
### ![Fisica_I/20240520/images/17.png](Fisica_I/20240520/images/17.png)
### se volessimo che il momento della coppia di forze, fosse come quello del vero sistema, potremo vedere che il sistema di punti può essere approssimato ad un punto materiale nel centro di massa e la sbarretta di massa trascurabile con due masse uguali che ruotano come la sbarretta

## I Teorema di König
### quindi passando dal SDR all SDR' del centro di massa potremo vedere che $\vec {\mathbb{P}} = 0$
### ![Fisica_I/20240520/images/18.png](Fisica_I/20240520/images/18.png)
### dal secondo Teorema di König che esplicava che $K = K'+K_c$
### il primo Teorema di König afferma invec che $\vec L = \vec L ' + \vec L_c$

## Dimostrazione del I Teorema di König
### per poter affermare che $\vec L = \vec L ' + \vec L_c$
### osserviamo che $\vec L = \sum_{i=1}^N \vec r_i \times \vec v_im_i$
### ![Fisica_I/20240520/images/19.png](Fisica_I/20240520/images/19.png)
### ricordando che $\vec r_i = \vec r_c+\vec r'_i$ e che $\vec v_i = \vec v_c+\vec v'_i$
### avremo che cambiando la la velocità $v_i$ avremo nella sommatoria $\vec L = \sum_i(\vec r'_i+\vec r_c)\times m_i(\vec v'_i+\vec v_c)=\sum_i \vec r'_i\times m_i\vec v'_i+\sum_i \vec r'_i\times m_i \vec v_c+\sum_i \vec r_c \times m_i \vec v_i+\sum_i \vec r_c\times m_i \vec v_c$
### essa osservandola attentamente è $\vec L = \vec L'+ \frac {M(\sum_i m_i \vec r_i)} M \times \vec v_c+\vec r_c \times \frac {\sum_i m_i\vec v_i} M+\vec r_c\times \sum_i \vec v_c$
### dato che i due termini centrali sono pari a zero, si cancellano ed otterrmeo che $\vec L = \vec L'+\vec L_c$
### l'ultimo termine diviene $\vec L_c$ poichè sarebbe $\vec r_c\times \sum_i \vec v_c=\vec r_c\times M\vec v_c= \vec r_c\times {\mathbb{P}}= \vec L_c$
### i termini centrali si annullano perchè il secondo termine è la poszione del centro di massa che sappiamo essere zero, ed il terzo termine è il momento angolare del centro di massa che sappiamo stare fermo

## A cosa servono i teoremi di König?
### tali teoremi sono utili a decomporre degli elementi affrontando i probelmi
### potremo infatti sempre affermare (per il secondo teorema di König) che $K=K'+K_c$
### e quindi in presenza di un moto parabolico, ad esempio, potremo calcolare l'energia cinetica spezzandola nelle due energie cinetiche
### inoltre (per il primo teorema di König) potremo affermare che $\vec L = \vec L'+\vec L_c$
#### ed in genere, per i problemi visti sino ad ora, il momento angolare del centro di massa è stato nullo $\vec L_c=0$, ovvero sarebbe come dire che il centro di massa non ruota

## Osservazioni utili per gli esercizi: cambio di polo e sue ripercussioni sull'equilibrio statico
### per riassumere e proiettare la teoria sugli esercizi, ci chiediamo cambiando polo cosa cambi
### infatti ricordiamo cosa significa prendere le forze esterne di in punto rispetto ad un polo o rispetto ad un altro (il centro di massa può essre diverso da un polo imposto sull'origine ad esempio)
### ![Fisica_I/20240520/images/20.png](Fisica_I/20240520/images/20.png)
### utilizzando la definizione di momento delle forze $\vec r_i \times \vec F_i = \vec M_i$
### e sapendo che $\vec r_i = \vec r_{O'}+\vec r'_i=\vec OO'+\vec r'_i
### quindi il momento iesimo sarà come detto prima $\vec M_i = \vec r_i\times \vec F_i= (\vec OO'+\vec r'_i)\times \vec F_i=()\vec r'_i+\vec OO')\times \vec F_i$
### quindi __cambierà il polo, ma non la forza, che sarà sempre la stessa__
### il momento di una forza dipende dal polo e quindi vedremo che __con poli diversi e stessa forza, il momento sarà diverso__
### ciò non ci interessa molto per la dinamica, ma è più rilevante per la statica
### infatti realizzando un'analisi statica di un corpo in equilibrio, osserviamo che il corpo potrebbe avere alcuni poli per cui esso è in equilibrio, mentre vi possono essere altri poli per cui il corpo ruota
### osservando $\vec M^{\text{esterne}}=\sum \vec M_i$ ed andando a cambiare polo, avremo $\vec M^{\text{esterne}} = (\sum_i \vec r_i + \vec OO')\times \sum \vec F_i^{\text{esterne}}=\vec M'_{\text{esterne}}+\vec OO' \times \vec F^{\text{esterne}}$
### quindi cambiando polo, oltre alla risultante dei momenti delle forze esterne nel nuovo polo, avrò che mi si andrà a sommare un altro termine
### ma quando il sistema è isolato, potrà rispettare tale equazione?
### ad esempio potrebbe essere nullo il momento delle forze esterne per $O$ e per $O'$ (per entrambi i poli)?
### sapendo che $\vec OO' \neq 0$ allora ciò sarà vero solamente se $\vec F^{\text{esterne}}=0$
### anche se molto sottile, ciò significa che __se la risultante delle forze esterne è nulla, allora non comparirà l'ulteriore termine per il momento delle forze esterne sul nuovo polo, quindi il momento risultante delle forze esterne per il nuovo polo sarà $\vec M^{\text{esterne}} =\vec M'_{\text{esterne}}+\vec OO' \times \vec F^{\text{esterne}} =\vec M'_{\text{esterne}}+0$
### quindi osservando che la risultante delle forze esterne è nulla, potremo dire che è il momento delle forze esterne sarà indipendente dal polo preso
### quindi __considerando un sistema di punti, se l'equilibrio da imporre deve essere statico, allora significherà che bisognerà imporre che $\vec F^{\text{esterne}}=0$ per ottenere $\vec M^{\text{esterne}}=0$__
### in pratica stiamo dicendo che __non cambierà la quantità di moto e non cambierà il momento angolare e delle forze__, __se la risultante delle forze esterne al sistema è nulla__, ciò equivale a esprimere che __il sistema è indipendente dal polo scelto__ e potrà sempre rimanere in equilibrio statico
