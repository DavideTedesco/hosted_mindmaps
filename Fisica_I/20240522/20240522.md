---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# 20240522

## Corpo rigido, come svolgere gli esercizi di statica
### ![Fisica_I/20240522/images/01.png](Fisica_I/20240522/images/01.png)
### osservando un corpo rigido in equilibrio statico fra due piani inclinati
### dovremo imporre che la risultante dei momenti si annulli
### poichè se vi è una condizione di equilibrio, il corpo non deve ne ruotare ne traslare rispetto al centro di massa
### dovranno essere nulle tutte e due le equazioni cardinali
#### $\begin{cases}\vec F^{\text{esterne}} = 0 \text{ prima eq. cardinale} \\ \vec M^{\text{esterne}} = 0 \text{ seconda eq. cardinale}\end{cases}$
### avremo quindi in totale 6 equazioni
### considerando che dalla prima equazione cardinale avremo 2 equazioni $(x,y)$
### dalla seconda equazione cardinale avremo una sola equazione, dato che sugli altri assi non staremo supponendo rotazioni
### _per studiare le condizioni di equilibrio statico, dobbiamo quindi risolvere le equazioni_
### allora vedremo che la prima parte dell'esercizio, ovvero della prima equazione cardinale, è simile a quella di un punto materiale
### la seconda parte invece è più complessa, perchè si osserva che alcuni momenti saranno entranti ed altri uscenti
### ![Fisica_I/20240522/images/02.png](Fisica_I/20240522/images/02.png)
### ponendo il polo $\Omega$ nel centro di massa, avremo che il momento della forza per la massa 1, sarà $\vec r_1\times \vec R_1=.0$, poichè hanno la stessa direzione
### quindi prendendo un polo in cui si hanno tutti i momenti pari a 0, avremo equilibrio statico verificato
### osserviamo che $\vec M^{\text{esterne}} = \vec M_{Fp}+\vec M_{R1}+\vec M_{R2}=0+0+0=0$
### notiamo che prendendo un altro polo, differente dal centro di massa, l'analisi ci potrebbe cambiare di molto
### ricordiamo allora che per cambiare il polo, dovremo osservare che (avendo come polo originario $O$ e come nuovo polo $O'$) dovremo scrivere $\vec M_O = \vec M_{O'}+\vec OO'\times \vec F^\text{esterne}$
### ma i momenti rispetto a poli differenti, saranno comunque uguali a zero
### infatti per far valere l'equilibrio statico, dovrò sicuramente accertarmi che $\vec F^{\text{esterne}}=0$ potremo quindi scegliere un polo a piacere
### ricordiamo infatti che __quando siamo in equilibrio statico deve valere $F^\text{esterne}=0$
### scegliendo il polo $CDM$ avremo che il momento si annulla
### avendo la massa del corpo troveremo 2 equazioni nulle
### quindi dalla prima equazione cardinale postremo risolvere in funzione di $\vec r_1$ ed $\vec r_2$)
### _in caso non bastino le equazioni, converrà cambiare polo

## Osservazione sulla scelta del polo negli esercizi di statica
### avendo due forze applicate ad un corpo rigido, osservando le direzioni delle due forze e trovando il punto in cui esse si congiungono
### ![Fisica_I/20240522/images/03.png](Fisica_I/20240522/images/03.png)
### quando le due forze sono danno l'equilibrio statico, potremo scegliere il punto che congiunge le due direzioni come polo per osservare i momenti
### tracceremo quindi le posizioni rispetto al polo
### sarà facile osservare che i vettori posizione e le forze sono paralleli
### quindi sapendo $\vec F^{\text{esterne}} =0$ è soddisfatta
### osservo che avrò l'equilibrio statico se ho i vettori paralleli alle forze, quindi se il loro prodotto vettoriale sarà nullo
### esso è proprio il nostro caso, ed avremo $\vec M_{F1\Omega}=0=\vec M_{F2\Omega}$ infatti $\vec M_{F1\Omega}=\vec r_1 \times \vec F_1$ e $\vec M_{F2\Omega}=\vec r_2 \times \vec F_2$
### quindi la __dipendenza dal polo nel calcolo dei momenti sappiamo sia sul vettore posizione__ se esso è parallelo alla forza, avremo momento delle forze nullo poichè nel prodotto vettoriale avremo $\sin(0)=0$
### per avere quindi che entrambi i vettori facciano annullare il momento delle forze, il polo dovrà essere proprio quello che abbiamo scelto intersecando le due direzioni

## Come e dove mettere una forza che faccia mantenere l'equilibrio statico del masso?
### pensando allo stesso esempio, e ponendo che in basso a sinistra si sia erosa una parte del masso che abbiamo rappresentato come corpo rigido
### osserveremo che sarà necessaria un'altra forza in alto a destra (ad esempio) per permettere al corpo rigido di rimanere in equilibrio statico
### ciò equivale in genere a mettere un tirante laddove serve
### osserviamo inoltre che $vec F_2$ la potremo vedere come una forza peso che cerca di spingere il masso giù nella valle, essa non è applicata perfettamente al centro, perchè il masso potrebbe essere cavo o comunque di massa non omogenea
### $P_2$ ovvero il punto dove è applicata la forza peso, sarà quindi il centro di massa, mentre $P_1$ sarà il punto materiale su cui il masso poggia
### ![Fisica_I/20240522/images/04.png](Fisica_I/20240522/images/04.png)
### notiamo che siccome il masso non è più in equilibrio, probabilmente la parte in basso a sinistra si è erosa
### ![Fisica_I/20240522/images/05.png](Fisica_I/20240522/images/05.png)
### quindi inizialemente i momenti erano tutti nulli
### _consideriamo nel nostro esempio di non avere attrito_ (anche se ciò è poco veritiero)
### vedremo che realizzando un'analisi delle forze avremo su $y$ la forza $\vec F_2$ e la componente della forza $\vec F_1$ ed in $x$ avremo la sola componente della forza $\vec F_1$
### per un punto materiale la nostra analisi delle forze sarebbe terminata in questo modo
### ![Fisica_I/20240522/images/06.png](Fisica_I/20240522/images/06.png)
### in realtà, dato che è un corpo rigido, avremo bisogno di capire e sapere che il corpo non ruoti
### scegliamo quindi la $\vec F_3$ in modo da avere una risultante delle forze pari a 0
### ma la $\vec F_3$ dove la dovremo applicare?
### potremo applicare la forza utilizza per bilanciare il masso e farlo rimanere in equilibrio statico, direttamente sul polo $\Omega$ oppure sulla direzione in cui giace esso
#### vi saranno infatti infiniti punti dove potremo applicare la $\vec F_3$, ovvero tutti quelli individuati dalla sua direzione
### otterremo così $\vec M^{\text{esterne}}_{3\Omega} = \vec r_{3\Omega}\times \vec F_3=0$

## Corpo rigido, come svolgere esercizi di dinamica
### mentre per il punto materiale, potremo risolvere anche i problemi di dinamica con due equazioni
### per il corpo rigido dovranno valere $\frac {d\vec {\mathbb{P}}} {dt} = \vec F^{\text{esterne}}$ e $\frac {d\vec L} {dt} = \vec M^{\text{esterne}}$
### quindi la dinamica del corpo rigido sarà molto complicata per via della seconda equazione cardinale dei sistemi di punti
### potrebbe infatti succedere di poter vedere in maniera diversa la dinamica di un corpo rigido in base al polo che sceglo
### l'equilibrio statico, come abbiamo visto, sarà indipendente dal polo $\Omega$ scelto
### __in dinamica non si può prescindere dalla scelta del polo__
### per la seconda equazione cardinale, avremo dei momenti diversi in base al polo che sceglieremo ed in base al sistema di riferimento che sceglieremo di utilizzare

## Classificazione in 3 tipologie di moto della dinamica
### per riuscire a capire a livello dinamico cosa accade, proviamo a giungere al moto più complesso, realizzando una classificazione delle tipologie di moto che un corpo rigido può avere
### __moto traslatorio__
#### moto per il quale il corpo trasla senza ruotare
### __moto rotatorio__
#### #### moto per il quale il corpo ruota senza traslare
### __moto roto-traslatorio__
#### moto per il quale il corpo compie sia rotazioni che traslazioni

## Moto traslatorio
### in tale tipologia di moto tutti i punti di un corpo rigido descrivo traiettorie parallele
### ![Fisica_I/20240522/images/07.png](Fisica_I/20240522/images/07.png)
### il corpo trasla rigidamente perchè ad esempio un punto $P_1$ sull'apice della sbarretta non varia la distanza dal centro di massa (mentre lo potrebbe fare in un corpo non rigido)
### quindi ad ogni istante di tempo, conoscendo la distanza tra il punto e il centro di massa e la direzione della traslazione, sapremo dove si trovi il punto $P_1$
#### se avessimo avuto un corpo non rigido, allora il punto avrebbe potuto salire o scendere
### avremo quindi traiettorie parallele dei punti
### il centro di massa del corpo traslerà, ma il corpo non ruoterà
### prendendo come polo il centro di massa $\Omega \equiv C \Rightarrow \vec M^{\text{esterne}}_c=0$
### ovvero il momento delle forze esterne sarà pari a zero per il centro di massa perchè rispetto al centro di massa il corpo non sta ruotando
### invece sappiamo che $\vec {\mathbb{P}} \neq 0$ osserveremo ad esempio che con un moto rettilineo uniforme avremo $\vec {\mathbb{P}} = \text{costante}$
### ma traslando con una forza il punto, esso potrebbe accelerare
### ![Fisica_I/20240522/images/08.png](Fisica_I/20240522/images/08.png)
### osserviamo quindi che ogni punto avrà la stessa velocità e se la $\vec v=\text{costante}$ allora il momento angolare rimarrà costante

## Moto rotatorio
### esso è una tipologia di moto che comprende la pura rotazione del corpo rigido che stiamo analizzando
### ciò vorrà dire che il centro di massa resterà fermo, ma i punti che compongono il corpo ruoteranno
### ![Fisica_I/20240522/images/09.png](Fisica_I/20240522/images/09.png)
### ovviamente i punti più esterni della sbarretta percorreranno più spazio
### nel caso rotatorio, avremo che la velocità angolare di ciascun punto materiale sarà uguale
### quindi i punti percorreranno tutti angoli uguali in tempi uguali
### in pratica staremo dicendo che il centro di massa è fermo, ovvero che $\vec {\mathbb{P}}=0$
### ma per questo moto $\vec M^{\text{esterne}}\neq 0$
### potremo quindi scrivere per il primo teorema di König che $K=K'+K_c$
### quindi sapendo che $K_c=0$ otterremo che $K=K'=\frac 1 2 M v_c^2$ in cui $M$ è la sommatoria di tutte le masse che compongono il corpo
### usiamo il teorema di König per scrivere in maniera più agile l'energia cinetica
### che in realtà sarebbe stata $\int \frac 1 2 v^2 dm = K$
### quindi con $K_c=0$ avremo $K=K'$
### ma sappiamo che $K'$ è un'integrale, potremo quindi scrivere l'energia cinetica come $K=K'=\frac 1 2 I \omega^2$ in cui $I$ è il momento d'inerzia
#### si osserva che il momento d'inerzia in una sola direzione è uno scalare
### quindi nei i moti di pura rotazione la massa totale si scambia (nella scrittura dell'energia cinetica) con il momento d'inerzia
### tale analogia la si può trovare anche osservando che $\vec M \vec a_c=\frac {d\vec {\mathbb{P}}} {dt}=\vec F^{\text{esterne}}$ allora per il momento delle forze di un corpo rigido in rotazione avremo che $\underline{\underline {I}}\cdot \vec \alpha = \frac {d\vec L}{dt} = \vec M^{\text{esterne}}$ in cui \underline{\underline {I}} è il momento d'inerzia come matrice
### __attenzione perchè questa appena scritta con il momento d'inerzia e l'accelerazione angolare non è sempre valida, ma vale solo se il momento d'inerzia è parallelo all'accelerazione angolare__

## Moto roto-traslatorio
### ![Fisica_I/20240522/images/10.png](Fisica_I/20240522/images/10.png)
### in questa tipologia di moto del corpo rigido, il corpo sta sia ruotando che traslando
### è utile quindi decomporlo in una parte traslatoria ed in un'altra rotatoria
### quindi l'energia cinetica sarà $K=K'+K_c=\frac 1 2 I \omega^2+\frac 1 2 M v_c^2$

## Osservazione sulla seconda equazione cardinale dei sistemi di punti ed esempio
### siccome la seconda equazione cardinale dei sistemi di punti dipende dal polo
### allora a seconda del polo scelto potrà cambiare il modo in cui vediamo il moto
### ad esempio osservando una sbarretta attaccata ad un muro che ruota con velocità angolare $\vec \omega$
### osserviamo che scegliendo il punto in cui la sbarretta è attaccata e centro della circonferenza e scegliendo un punto della sbarretta come il centro di massa, osserveremo che essi sono due moti diversi
### ![Fisica_I/20240522/images/11.png](Fisica_I/20240522/images/11.png)
### scegliendo $O\equiv \Omega$ allora rispetto al polo centrale avremo un solo moto rotatorio
### quindi in tal caso l'energia cinetica sarà $K=\frac 1 2 I_O \omega^2$
### ![Fisica_I/20240522/images/12.png](Fisica_I/20240522/images/12.png)
### scegliendo il centro di massa come polo, avremo che $\Omega\equiv CDM$ allora descriveremo il moto del centro di massa, oltre il moto del centro di massa dovrò capire cosa accade al sistema
### _questa tipologia di problemi sarà in linea generale molto complicata_

## Casi più semplici: momento angolare del corpo rigido
### osservando un corpo rigido come un insieme di masse che può ruotare attorno ad un solo asse (per semplicità non ruoteremo intorno agli altri due perchè avremmo il momento d'inerzia che diviene un vettore in 3 dimensioni complicando di molto la trattazione)
### potremo vedere che il corpo avrà un momento delle forze
### ![Fisica_I/20240522/images/13.png](Fisica_I/20240522/images/13.png)
### osserviamo da ora in poi tale corpo, come un elemento unico che ruota attorno ad un asse con la velocità angolare $\vec \omega$

## Corpo rigido e rotazioni intorno ad un'asse fissato: il momento d'inerzia
### avendo un corpo rigido (come un blob) con un solo asse di rotazione
### sapendo che il centro di massa è fermo, avremo che $\vec {\mathbb{P}}=0$
### ![Fisica_I/20240522/images/14.png](Fisica_I/20240522/images/14.png)
### allora la prima equazione cardinale sarà nulla dato che $\vec {\mathbb{P}}=0$
### osservando la seconda equazione cardinale vedremo che $\vec L = \int d\vec L$
### ![Fisica_I/20240522/images/15.png](Fisica_I/20240522/images/15.png)
### la velocità $\vec v$ sarà entrante nel piano, mentre vediamo che l'asse $x$ è uscente dal piano
### allora se il corpo ruota in senso antiorario, ha quindi una velocità entrante nel piano
### calcolando $d\vec L$ e chiamando l'angolo tra il vettore posizione $\vec r$ e il cubetto individuato $\Theta$ rispetto all'asse $z$
### otterremo $d\vec L = \vec r \times dm\cdot \vec v$
### quindi in modulo $|d\vec L| =r\cdot dm\vec v \sin (\frac {\pi} 2)$ in cui sappiamo che $\sin (\frac {\pi} 2)=1$ quindi avremo $|d\vec L| =r\cdot dm\vec v \cdot 1= r\cdot dm\vec v$
### notiamo quindi che $\vec r$ è perpendicolare a $\vec v$
### la $R=r\sin(\Theta)$
### ![Fisica_I/20240522/images/16.png](Fisica_I/20240522/images/16.png)
### allora proiettando sulla direzione di rotazione, prenderemo $d\vec L$ e lo moltiplicheremo scalaremente per la componente di rotazione $\hat z$ ottenendo $dL_z=d\vec L\cdot \hat z$
### osserviamo (come avevamo visto per la sbarretta con due palline) che vi sarà una componente lungo $y$ che cambia e che non ci interessa in questo momento, ed una componente lungo $z$ che resta costante, ovvero proprio la $dL_z$ appena trovata
### proiettiamo quindi per vedere tale componente del momento angolare sull'asse $z$
### ![Fisica_I/20240522/images/17.png](Fisica_I/20240522/images/17.png)
### quindi $d\vec L \cdot \hat z = dL_x=dL\sin(\Theta)$ riconosciamo in essa $r\sin(\Theta)=R$ ed avremo che $dL_z = R^2 \omega dm$
### esso sarà il momento angolare di un elemento del sistema
### $R$ è la distanza di un punto particolare dall'asse di rotazione
### ![Fisica_I/20240522/images/18.png](Fisica_I/20240522/images/18.png)
### osserviamo allora l'integrale per il calcolo del momento angolare $L_z=\vec L\cdot \hat z=\int d\vec L \cdot \hat z = \int dL_z=\int R^2 \omega dm$
### vedremo che $dm=\rho (\vec r)dV$ e $dV=dx,dy,dz$, mentre $\omega$ non dipende dalla posizione
### quindi $L_z=\int \omega R^2d(\vec r)dV$ che sarebbe un integrale in 3 dimensioni
### ponendo $x$ e $y$ costanti, avremo che $\textcolor{red}{L_z=\omega \cdot I_z}$
### sapendo che $I_z$ è il momento d'inerzia rispetto all'asse di rotazione, allora osservando l'integrale sarebbe come dire che moltiplichiamo la massa per l'asse di rotazione
### quindi __il momento d'inerzia $I_z$ sarà uno scalare__
### in pratica $I_z$ è un modo strano di sommare in maniera pesata le masse del sistema in base alla distanza del punto che ruota rispetto all'asse di rotazione
### in linea generale vedremo che la formula generica del momento d'inerzia rispetto all'asse $z$ sarà $\textcolor{red}{I_z=\int R^2 dm}$
### la stessa massa prendendo punti diversi in cui posizionare il polo, avrà momenti d'inerzia diversi
### ricordiamo che per la sbarretta con due palline il momento d'inerzia parallelo alla velocità angolare era pari a $L=\frac {2md^2} 4 \omega$ molto simile al risultato trovato per $L_z$
### possiamo quindi giungere alla conclusione che __è più difficile mettere in rotazione un corpo rigido che ha massa più lontano dall'asse di rotazione__
### anche nella sbarretta con due palline agli estremi avevamo visto che potevamo scrivere una componente del momento angolare che era sempre parallela ad $\vec omega$
### ![Fisica_I/20240522/images/19.png](Fisica_I/20240522/images/19.png)
### __in generale non è vero che $\vec L = c\cdot \vec \omega$, questo caso è valido solo per la componente $z$
### nella realtà sappiamo che ci sono 3 assi e che tutti e tre potrebbero ruotare
