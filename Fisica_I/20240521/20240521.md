---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# 20240521

## Corpo rigido: introduzione
### esso è di fatto un esempio particolare di un sistema di punti
### dal nome attribuitogli è chiaro che stiamo parlando di un oggetto o pezzo di esso che non può esser pensato come un punto materiale singolo
### tale corpo ha quindi proprietà particolari
### il termine __rigido__ sta ad indicare che le distanze dei punti materiali sono fissate, e rimangono tali
### ![Fisica_I/20240521/images/01.png](Fisica_I/20240521/images/01.png)

## Corpo esteso
### prima di andare a formalizzare cosa voglia dire che un corpo sia rigido
### osserviamo che inoltre il numero di punti materiali che compongono un corpo rigido, se esso è dichiarato come __esteso__
### avrà un infinito numero di punti materiali
### capiamo quindi che tutte le somme e sommatorie diverranno degli integrali
### quindi un sistema di questo tipo non sarà numerabile
### ![Fisica_I/20240521/images/02.png](Fisica_I/20240521/images/02.png)
### prendendo il nostro corpo e dividendolo a quadretti, o meglio in 3 dimensioni a cubetti realizzeremo una discretizzazione dello stesso
### inoltre a tale discretizzazione potremo aggiungere un sistema di riferimento ed osservare che ogni quadretto (o cubetto) ha una distanza dall'origine del sistema di riferimento ed ogni punto materiale che lo compone è identificabile come il proprio centro
### ![Fisica_I/20240521/images/03.png](Fisica_I/20240521/images/03.png)
### capiamo quindi che per un corpo esteso, i quadretti sono densi e dovremo discretizzare
### ogni singolo quadratino sarà quindi un __elemento di massa__ che ha una massa infinitesima
### quindi come per l'integrale con la variabile $x$ vedevamo che un intervallo infinitesimo diveniva $dx$
### ![Fisica_I/20240521/images/04.png](Fisica_I/20240521/images/04.png)
#### nel quale la sommatoria $\sum_{i=1}^{\infty} f(x)\cdot \Delta x$ diviene per $\Delta x\rightarrow 0$ $\int f(x) dx$
### vedremo che ragionando i termini di punti materiali, identificheremo ogni punto materiale come elemento di massa $dm$, o per essere più precisi come __elementi di volume__ $dV$
### ![Fisica_I/20240521/images/05.png](Fisica_I/20240521/images/05.png)
### quindi ogni striscia che individuiamo sarà un intervallo per ogni punto

## Esempi della sbarretta
### osserveremo nello specifico una sbarretta, la quale, pur essendo tridimensionale, la approssimiamo a un'unica dimensione
### avremo quindi un segmento lungo $L$ unidimensionale
### ![Fisica_I/20240521/images/06.png](Fisica_I/20240521/images/06.png)
### esso lo discretizziamo, quadrettandolo e lo inseriamo su di un asse
### ![Fisica_I/20240521/images/07.png](Fisica_I/20240521/images/07.png)
### ogni elemento che compone la sbarretta sarà $dm$ o meglio nel nostro caso unidimensionale $dx$
### quindi in pratica se il cubetto visto è su una sola linea, lo posso approssimare a un segmento, in pratica un elemento di volume lo staremo approssimando ad un __elemento di lunghezz $dx$__ che si misurerà a questo punto in metri e non in metri cubi come il volume
### avremo in 3 dimensioni che $dV= dx\cdot dy\cdot dx$
### ciò sarebbe il prodotto degli infinitesimi del cubetto individuato, che sono di fatto i vari lati del cubetto
### ![Fisica_I/20240521/images/08.png](Fisica_I/20240521/images/08.png)

## Qual'è la massa di $dx$
### osserveremo che la quantità di massa per un singolo segmento, sarà pari alla densità di massa di ogni singolo altro segmento che compone la nostra sbarretta
### nel caso della superficie, ogni quadratino può essere approssimato a un punto materiale posto al centro del quadratino
### ![Fisica_I/20240521/images/09.png](Fisica_I/20240521/images/09.png)

## Densità
### osserviamo che la massa e1 diversa a parità di volume negli oggetti di tutti i giorni
### dovremo quindi introdurre la __densità__, che ci permette di legare massa e volume
### $\frac M V = \rho$ essa è quella che chiamiamo __densità volumica__ e chiarisce, avendo il volume e la massa di un oggetto, quale sia la densità del materiale con cui l'oggetto è costruito
### analogamente ad essa individuiamo la __densità lineare__, ovvero la densità di un segmento $\frac M L = \lambda$
### e la __densità superficiale__ ovvero $\frac M S = \sigma$
### la densità è la quantità di massa che trovo nel sistema rispetto all'origine, infatti ad esempio, avendo come centro di massa $x$ avremo che $\rho = c\cdot x$
### in generale, la densità potrà dipendere da $\rho (\vec r)$ ovvero dal vettore posizione che collega il punto materiale osservato all'origine
### ogni elemento di volume è quindi rappresentato da $\vec r$ e per ogni punti vi potrà essere una densità diversa
### prendendo un punto qualsiasi, vedremo che $\vec \rho \Rightarrow \rho(\vec r)$
### staremo quindi dicendo che per calcolare la massa infinitesima di un cubetto $dm= \rho (\vec r)dV$
### in pratica ogni punto materiale avrà una sua massa $dm$ che dipende da $\vec r$
### avremo quindi $dm(\vec r)=\rho(\vec r)dxdydz$ e nel particolare avremo $dm(\vec r)=\rho(x,y,z)dxdydz$
### osserviamo inoltre che la densità può dipendere da dove ci si trova (ad esempio da una data altitudine), quindi in base a dove ci si trova in montagna, ad esempio dovremo valutare se utilizzare la stessa densità od un'altra
### in due posizioni di altezza diversa $A,B$ potremo avere due densità diverse
### per il nostro caso generale, utilizzeremo un unica dimensione, quindi $dm=\rho(x)dx$

## Come procediamo dopo aver osservato che $dm=\rho (x) dx$?
### ogni punto, se visto unidimensionalmente avrà una sua densità quindi con $dm(x)=\rho(x)dx$
### varranno le osservazioni viste precedentemente per cui $\frac {d\vec {\mathbb{P}}} {dt}=\vec F^{\text{esterne}}$ ed inoltre $\frac {d\vec L} {dt} = \vec M^{\text{esterne}}$

## La rigidità
### abbiamo quindi diviso in blocchetti un corpo rigido, osservandolo in una sola dimensione per semplicità
### ma vorrei evitare di studiare sistemi diversi e osservo che se il corpo è rigido, allora i punti che vi sono al suo interno saranno a distanza costante gli uni dagli altri
### ![Fisica_I/20240521/images/10.png](Fisica_I/20240521/images/10.png)
### infatti perchè sia un corpo rigido __la distanza fra i punti deve essere costante__
### quindi per semplicità consideriamo un sistema continuo con queste caratteristiche
### sapendo che esso è un sistema di punti, dovremo scrivere il moto di ciascun punto
### descrivendo quindi le loro posizioni $\vec r_i$ e le loro velocità $\vec v_i$
### in 3 dimensioni avremo $3N+3N=6N$ variabili a cui si andrebbero a sommare le variabili viste per $\frac {d\vec {\mathbb{P}}} {dt} = \vec F^{\text{esterne}}$ che sono altre 3 variabili e quelle di $\frac {d\vec L} {dt} = \vec M^{\text{esterne}}$ che sono altre 3 variabili
### ma per un corpo rigido, dato che non si modificano le distanze fra i vari punti, invece di considerare 12 variabili complessive, considereremo le sole ultime 6
### vedremo infatti che assumendo che le distanze fra i punti siano costanti, servirà solamente sapere cosa fanno i vari punti rispetto al centro di massa (che ricordiamo avere quindi anche distanta costante dal centro di massa)
### ![Fisica_I/20240521/images/11.png](Fisica_I/20240521/images/11.png)
### riusciremo quindi a descrivere con il corpo rigido il moto tramite 6 equazioni
#### 3 equazioni del moto del centro di massa (rispetto al sistema fisso)
#### 3 equazioni del moto di un punto rispetto al centro di massa
### quindi per il fatto che i punti hanno distanze fissate fra loro, allora avremo che $\vec r_{ij}=\vec r_i-\vec r_j=\text{costante} \quad \forall i,j = 1,\dots,N$
### allora andando a calcolare la distanza di un punto dal centro di massa potremo scrivere $\vec r_c - \vec r_j = \sum_{i=1}^N \frac {m_i-\vec r_i} {M}-\vec r_j \frac {\sum_{i=1}^N m_i} {M}$ potendo quindi semplificare al secondo termini la sommatoria delle masse con la massa totale, otterremo sapendo che la differenza fra le due posizioni è costante $\vec r_c - \vec r_j = \frac 1 M \sum_i m_i (\vec r_i- \vec r_j)$, anche $\vec r_i - \vec r_j$ sappiamo essere costante
### quindi __richiedendo che le distanze fra due punti siano costanti, sto assumendo implicitamente che anche le distanze di tali punti dal centro di massa siano costanti__

## Esempio di un oggetto rigido e del suo centro di massa
### prendendo un oggetto rigido, la distanza dal centro di massa non cambierà (a meno che l'oggetto non si rompe)
### potremo quindi descrivere il moto di un corpo rigido comeil moto del cetnro di massa.
### è inoltre utile notare che dato che i punti rimangono alla stessa distanza dal centro di massa essi potranno solamente ruotare attorno al centro di massa e mai traslare, il loro moto sarà descritto dalla seconda equazione cardinale dei sistemi di punti
### ![Fisica_I/20240521/images/12.png](Fisica_I/20240521/images/12.png)

## Variazione di energia cinetica di un corpo rigido
### osserviamo che per il sistema di punti, continua a valere la conservazione dell'energia
### quindi dovremo essere in grado di calcolare l'energia cinetica e l'energia potenziale
### $L_{NC} = \Delta E \equiv \Delta K+\Delta U$
### ma per un corpo estese l'energia cinetica (dato che è continuo) sarà $\int \frac 1 2 dm v^2 = K$
### osservando che $dm=\rho dx$
### potremo vedere che l'energia cinetica sarà $K = \int \frac 1 2 \rho dx v^2$

## Calcolo del centro di massa a partire da $O$ per un sistema esteso
### è importante cercare di ricordarsi per gli oggetti estesi quale sia il momento d'inerzia

## Esempi di calcolo del centro di massa di corpi rigidi
### Esempio della sbarretta omogenea
#### avendo una sbarretta omogenea di lunghezza $L$ e volendone calcolare il centro di massa
#### osserviamo che essa la possiamo vedere come un insieme di segmenti con coordinate $x,dx,dm$
#### ![Fisica_I/20240521/images/13.png](Fisica_I/20240521/images/13.png)
#### otterremo quindi che la posizione del centro di massa sarà $\frac 1 M \int_{\text{sbarretta}} dm$
#### ricordando ancora che $dm=\rho dx$
#### osserveremo che la posizione del centro di massa sarà $x_c=\frac 1 M \frac {\int_0^L \rho dx} M-\frac {\rho} M \cdot \int_0^L x dx$
#### che sappiamo essere pari a $x_c = \frac {\rho} M \cdot \frac {L^2} 2$
#### e potremo ottenere un risultanto indipendente da $\rho$, dato che è costante
### avremo quindi $M=\int_{\text{sbarretta}} dm = \int_0^L \rho dx= \rho L$ e sostituendo a $\rho =\frac M L$ avremo che $x_c = \frac {\rho} M \cdot \frac {L^2} 2 = \frac L 2$
#### abbiamo quindi trovato, come era prevedibile, che il centro di massa sarà proprio al centro della sbarretta
#### ![Fisica_I/20240521/images/14.png](Fisica_I/20240521/images/14.png)
#### si potrebbe calcolare anche il centro di massa per mezzo dell'induzione andando coppie a coppie di masse da quelle più esterne a quelle più interne

### Esempio del semianello omogeneo
#### ora cerchiamo di calcolare il centro di massa di un corpo rigido realizzato come un semianello
#### ![Fisica_I/20240521/images/15.png](Fisica_I/20240521/images/15.png)
#### considerando l'anello come una lunghezza, dato che è realizzato di elementi materiali che sono punti (abbiamo quindi un sistema con masse omogenee), avremo che la lunghezza del semianello sarà $L = \pi \cdot R$
#### avremo quindi un oggetto unidimensionale immerso in uno spazio bidimensionale
#### non potremo quindi essere sicuri che il centro di massa venga perfettamente nel centro di quella che sarebbe l'ipotetica circonferenza del semianello
#### osserviamo che uno dei segmentini che ci permettono di discretizzare l'anello è $dl$
#### ![Fisica_I/20240521/images/16.png](Fisica_I/20240521/images/16.png)
#### quindi sapendo che $\vec r_c = \frac 1 M \int dm\vec r$
#### e scrivendo $\vec r$ in componenti, dato che siamo in uno spazio bidimensionale
#### $\vec r= R\cos(\Theta)\hat x+ R\sin(\Theta)\hat y$
#### in questo caso potremo scrivere l'elemento di massa come $dm=\rho dl$ in cui $dl$ è la lunghezza infinitesima del segmento preso che vale $dl=Rd\Theta$ (similmente a come abbiamo calcolato $L$)
#### quindi $\vec r_c = \frac 1 M \int_0^{\pi} \rho R d\Theta \vec r$
#### ovvero riscrivendola $x_c = \frac {\rho} M R \int_0^{\pi}x(\Theta)dx=\rho \frac {R^2} M \int_0^\pi \cos (\Theta) d\Theta$
#### per calcolare l'ordinata del centro di massa avremo quindi $y_c = \frac {\rho R} M \int_0^{\pi} y(\Theta)d\Theta = \rho \frac {R^2} M \int_0^{\Theta} \sin(\Theta)d\Theta$
#### vedremo che $x_c=0$ dato che $\cos(\pi)-cos(0)=0$ e su ciò non ci sono dubbi poichè lo potremo vedere anche facendo la media di tutte le coppie intorno al semianello
### per l'ordinata svolgendo l'integrale vedremo $y_c=\frac {\rho R^2} M (-\cos(\Theta))\Big|^{\Theta=\pi}_{\Theta=0}=\rho \frac {R^2} M(+1-1) =2\rho\frac {R^2} M$
#### sapendo che $\rho=\frac M {\pi R}$ potremo inserirlo in $y_c$ ed ottenere $y_c = \frac {2R^2} M \cdot \frac M {\pi R} = \frac {2R} {\pi}$
#### allora vedremo che il centro di massa non sarà sicuramente sull'asse $x$, ma sarà sospeso poco sotto il semianello sull'asse $y$
#### ![Fisica_I/20240521/images/17.png](Fisica_I/20240521/images/17.png)
#### anche per un oggetto rettangolare, il centro di massa sarebbe a mezz'aria come visto ora
#### ciò significa che si ha più massa concentrata nella parte alta del corpo
#### ![Fisica_I/20240521/images/18.png](Fisica_I/20240521/images/18.png)

### Altro svolgimento del semianello (esercizio per casa)
#### l'esercizio del semianello lo si poteva svolgere osservando che possiamo dividerlo a coppie di masse e calcolare il centro di massa dei pezzi dell'anello
### poi la massa dipenderà dall'altezze in cui si trova
### troveremo quindi lo stesso risultato

## Esercizio del semianello pieno (per casa)
### più difficile da comprendere è l'esercizio in cui il semianello è pieno e dovremo modellarlo come sbarrette sempre più corte andando verso l'apice del semianello
### ![Fisica_I/20240521/images/19.png](Fisica_I/20240521/images/19.png)
### notare che il centro di massa di ciascuna barretta è al centro delle sbarrette
### il risultato è $y_c=\frac {4R} {3\pi}$
### la $x_c$ è ovviamente centrale

## Casi particolari di corpo rigido e osservazioni
### affronteremo i corpi rigidi in maniera completa per la statica, mentre vedremo solamente il caso delle piccole oscillazioni per quanto riguarda la dinamica
### avevamo osservato che $\vec M^{\text{esterne}} \neq \vec r_i\times \vec F^{\text{esterne}}$
### e avevamo visto che $\vec M^{\text{esterne}}=\sum_i \vec r_i\times \vec F_{\text{esterne}}$
### ma potremo osservare che questa M^{\text{esterne}} = \vec r_i\times \vec F^{\text{esterne}}$ verificata per un punto materiale specifico e quando le forze $\vec F_i$ sono costanti
### quindi genericamente quando le forze di ogni punto sono parallele
### ma la forza potrebbe dipendere dalla posizione in modulo
### ad esempio $\vec F =F_i\cdot \hat y$
### ![Fisica_I/20240521/images/20.png](Fisica_I/20240521/images/20.png)
### avendo quindi un campo di forze parallele, la forza osservata avrà una direzione fissata
### quindi $\vec M^{\text{esterne}}=\sum_i \vec r_i \times F_i \hat y$
### potremo qundi osservare che $\vec M^{\text{esterne}} = \sum_i (\vec F_i \vec r_i)\times \hat y$ perchè $F_i$ non dipende dalla direzione ma è solo uno scalare
### quindi $\vec F^{\text{esterne}} = \textcolor{red}{}\sum_i F_i}\hat y= \textcolor{red}{F}\cdot \hat y$
### quindi può avere per ogni punto la forza un valore diverso, come ad esempio la forza peso
### quindi $\vec M^{\text{esterne}} = \sum_i \frac {F_i \vec r_i} F \times \hat y \cdot F= \vec r_F\times \vec F^{\text{esterne}}$
### chiameremo il centro individuato dal vettore $\vec r_F$ __centro delle forze__ ovvero quel punto in cui si concentrano tutte le forze esterne al sistema
### ma per un campo di forze paralelle avremo quindi che $\vec M^{\text{esterne}} = \vec r_F\times \vec F^{\text{esterne}}$
### differenziamo tale centro dal centrod i massa, perchè è un altro centro in cui si concentrano le forze e non le masse

## Esempio della sbarretta come corpo rigido e momento calcolato nel centro di massa
### osservato teoricamente cosa sia un centro delle forze
### vediamo che prendendo una sbarretta appoggiata ad un piano inclinato
### essa risentirà di attrito statico
### per capire dove sarà il centro delle forze per un sistema omogeneo, osserviamo che esso ha ad ogni sua massa che lo compone, una forza peso applicata
### quindi $F_i = m_i g$
### quindi $\vec r_F = \sum_i \frac {m_i \cdot g \vec r_i} {\sum_i m_ig} = \vec r_c$ semplificando avremo che in questo caso specifico il centro delle forze combacia con il centro di massa
### quindi potremo segnare la forza del sistema nel centro di massa come $-Mg$ ovvero sommando tutte le forze nel centro delle forze (in questo caso nel centro di massa)
###  ![Fisica_I/20240521/images/21.png](Fisica_I/20240521/images/21.png)
### anche la reazione vincolare (che ricordiamo essere una forza), in questo caso, potremo porla sul centro di massa
