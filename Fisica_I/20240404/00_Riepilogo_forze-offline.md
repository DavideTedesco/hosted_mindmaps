---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# Riepilogo sulle forze

## Classificazione dei problemi
### ![./images/01.jpg](./images/01.jpg)

## Abbiamo osservato che esitono problemi di:
### Punto materiali
#### Equilibrio Statico
##### relativi al primo principio della dinamica
#### Dinamica
##### relativi al secondo principio della dinamica
##### Equilibrio dinamico di cui vi sono due casi
###### caso che rispetta il primo principio della dinamica
###### caso che prevede il moto circolare
### Sistemi di Punti

## Tre principi delle Dinamica
### Primo principio

#### $\vec F = 0 \text{ se e solo se } \vec v =\text{costante} \text{ oppure se il corpo è in quiete}$
### Secondo principio
#### $\vec F = m \cdot \vec a$
### Terzo principio
#### ad ogni azione corrisponde una reazione uguale o contraria
### per poterli applicare è necessario svolgere molti esercizi
