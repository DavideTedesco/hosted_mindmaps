---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# Forza peso

## Introduzione
### Dalla legge generale della Forza di gravità, osservando una situazione con una massa $m_2$ molto più grande di una massa $m_1$
#### ![./images/03.jpg](./images/03.jpg)
#### potremo affermare che nella formula $\vec F_G = \frac {G\cdot m_1 \cdot m_2}{D^2} \approx \frac {G \cdot m_2}{R^2} \cdot m_1$
##### con $R$ raggio del corpo con massa $m_2$
##### perchè possiamo considerare  $\frac {G \cdot m_2}{R^2}$ costante
##### anche tale formula fu trovata da Newton (oltre a $\vec F= m\cdot \vec a$)
#### Considerando il corpo con massa $m_2$ come la terra e il corpo con massa $m_1$ a distanza $H$ trascurabile (rispetto al raggio della terra) otterremo la seguente approssimazione
##### $\frac {G \cdot m_2}{R^2} \approx \vec g = 9,81 \frac m {s^2}$
###### $g$ è quella che chiamiamo in genere accelerazione gravitazionale
###### Tale valore è dipendente dal pianeta; cambiando pianeta il valore sarà diverso

## Osservazione sulla forza di gravità
### La formula vista $\vec F_G = \frac {G \cdot m_2}{R^2} \cdot m_1$
#### Deriva da un esperimento di Galileo in cui vengono utilizzate masse diverse lanciate da una stessa altezza
### ![./images/04.jpg](./images/04.jpg)
#### in questo caso i tre corpi di massa $m_1$, $m_2$ ed $m_3$ sono lanciati dalla stessa altezza $h$
##### essi hanno lo stesso tempo di caduta in assenza di attrito
##### quindi se le forze di attrito sono trascurabili le durate del tempo di caduta saranno analoghe per tutte le masse
###### quindi $t_1=t_2=t_3$ come osservazione sperimentale __il tempo di atterraggio dei corpi e quindi l'accelerazione è indipendente dalla massa__
##### Come è possibile notare in questi video:
###### [link a un esperimento dimostrativo dell'indipendenza del moto dei gravi dalla massa (a partire dal minuto 3)](https://www.youtube.com/watch?v=E43-CfukEgs)
###### [video originale dell'esperimento svolto sulla Luna dal Comandante David Scott](https://www.youtube.com/watch?v=ZVfhztmK9zI)
### scendendo l'oggetto aumenterà la sua velocità, implicando quindi che vi sia accelerazione
#### ottenendo un moto con accelerazione costante
##### ovvero il moto di caduta di un grave per il quale ricordiamo
###### $\vec a (t)= \text{costante}$
###### $\vec v (t) = v_0 +a\cdot t$
###### $\vec y(t) = y_0 + v_0 \cdot t + \frac a 2 \cdot t^2$ e nello specifco $\vec a = - \vec g$ ed $y_0 = h$
### Potremo verificare per mezzo di calcoli che l'accelerazione in caduta libera per due oggetti diversi, non dipende dalla massa degli stessi
#### ma è dipendente dall'accelerazione gravitazionale in assenza di attrito
#### lasciando un oggetto cadere avremo sempre un'accelerazione negativa
#### se i tempi di caduta di due oggetti sono $t_1=t_2$ allora $a_1=a_2$

## Specifiche per la Forza peso
### bisogna specificare che
#### 1) l'accelerazione di un corpo in caduta libera non dipende dalla massa
#### 2) osservando la legge di Newton $\vec F= m_{inerziale}\cdot \vec a$ potremo ricavarci l'accelerazione con la formula inversa
##### $\vec a = \frac {\vec F_P} {m_{inerziale}}$
##### ma se abbiamo detto che l'accelerazione non dipende dalla massa, allora dovremo cercare di farla scomparire da tale formula
##### ricordiamo inoltre che la Forza Peso è la risultante delle forze applicate al grave
###### e sapremo che ogni corpo ha Forza Peso diversa
### per poter semplificare la massa al denominatore osserviamo che $\vec F_p = \vec c \cdot m_{inerziale}$
#### quindi potremo osservare che $\vec a = \frac {\vec c \cdot m_{inerziale}} {m_{inerziale}}$
##### andandosi a semplificare le masse, otterremo che $\vec a = \vec c$
### Osserviamo però che abbiamo usato sino ad ora la massa inerziale, diversa da quella gravitazionale
#### però è stato verificato sperimentalmente che sono uguali, ed al massimo potrebbe essere che $m_{gravitazionale} = K \cdot m_{inerziale}$, ovvero sono proporzionali
### Quindi potremo affermare che
#### $\vec F_P = \vec c \cdot m_{inerziale}$
#### ed inoltre che $\vec c = \vec a = \vec g$
#### quindi se $\vec a = \frac {\vec c \cdot m_g}{m_i} = \vec c \cdot K$
##### lasciando $\frac {m_g} {m_i} = K$
###### in genere prendiamo $K=1$
### in breve osservando l'accelerazione gravitazionale di un corpo essa non dipende dalla massa e tramite il secondo principio della dinamica otterremo la Forza Peso
### il fatto che $m_{gravitazionale} = m_{inerziale}$ è stato verificato fino ad una precisione di $10^{-15}$
### La Forza Peso di cui un corpo risente in prossimità della terra è pari a $\vec F_P = - m \cdot \vec g$
#### ciò è dimostrabile in due modi
##### in via sperimentale
##### per mezzo della forza di gravità

## Specifiche su massa e peso
### in genere in Fisica
#### la massa è la sola $m$
#### mentre per peso si intende la Forza Peso $\vec F_P = - m \cdot \vec g$ in cui
##### $\vec F_P$ dipende dal pianeta
##### $m$ dipende solo dal corpo
##### ![./images/05.jpg](./images/05.jpg)
###### la Forza Peso é dipendente dal pianeta

## Misurare la forza peso
### la forza si misura in Newton
### in cui $1 N = 1Kg\cdot 1 \frac m s$
### osservando che misurando gli atomi troveremo una forza tra di essi che li tiene
### la forza è quindi una grandezza fisica derivata e si misura sempre in Newton
