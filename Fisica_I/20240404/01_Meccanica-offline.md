---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---
# Meccanica

## Analizzeremo le forze in sistemi meccanici
## Non osserveremo forze di tipo magnetico ed elettromagnetico
## Ogni sistema avrà delle forze particolari
## Dovremo distinguere i principi cardine e le applicazioni legate alla meccanica
