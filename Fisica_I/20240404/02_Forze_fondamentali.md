---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# Forze fondamentali

## Esse sono
### Forza di gravità
#### avendo delle  masse con due forze esse saranno attratte l'una all'altra per via della forza di gravità
##### ![Fisica_I/20240404/images/02.jpg](Fisica_I/20240404/images/02.jpg)
##### $|\vec F_{12}|=|F_{21}|=\vec F_G = \frac {G\cdot m_1 \cdot m_2}{D^2}$
###### con $G$ costante di gravitazione universale (diversa da $\vec g$)
#### essa può essere approssimata con la forza peso $\vec F_P$ in prossimità di un pianeta
### Forza elettromagnetica
#### essa è simile alla forza di gravità come formula, ma vi sono in  essa cariche attrattive e repulsive
### Forza nucleare "forte"
#### tiene insieme gli atomi
#### per essa cambia la distanza rispetto alla forza di gravità e a quella elettromagnetica
### Forza nucleare "debole"
#### tiene insieme gli atomi
#### per essa cambia la distanza rispetto alla forza di gravità e a quella elettromagnetica

## Tali forze sono scritte tutte in maniera simile

### ma osserveremo solamente la prima
### tutte le altre forze all'infuori di quelle qui riportate possono essere ricondotte a quelle fondamentali
