---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# Vincoli

## Introduzione ai vincoli
### osservando un oggetto in caduta
#### vedremo che quando esso é in aria é soggetto ad una Forza Peso
#### quando esso atterra sarà in una sorta di equilibrio
### la reazione vincolare è la forza che tiene fermo un oggetto appoggiato su di un piano
#### essa lo mantiene in un equilibrio chiamato statico
#### ![./images/06.jpg](./images/06.jpg)
### Esse sono approssimazioni di forze elettromagnetiche osservate a livello macroscopico (in maniera sperimentale)

## Reazione vincolare
### essa é la forza causata da un vincolo su un punto materiale che impedisce il movimento del punto
### quindi il vincolo vieta al corpo di sprofandare nel piano
### quindi ogni volta che vi é un vincolo vi é una reazione vincolaree
### si può osservare che a livello atomico vi é un reticolo con atomi posizionati in un certo modo all'interno di un solido
#### essi legano il corpo in maniera tale da divenire un vincolo per un punto materiale (vi sono in genere distante fisse tra gli atomi)
#### ![./images/07.jpg](./images/07.jpg)
#### per i liquidi vi saranno tante molecole legate da interazioni deboli
### sia i solidi che i liquidi con forza peso applicata su di essi tenderanno a deformarsi
#### ![./images/08.jpg](./images/08.jpg)
#### staremo quindi sfruttando il terzo principio della dinamica
### la reazione vincolare ha un massimo dopo il quale l'oggetto (se solido) si romperà ovvero quando $\vec F_P > \text {Reazione Vincolare}$

## Esempio di reazione vincolare (sperimentale sul piano)
### ![./images/06.jpg](./images/06.jpg)
### quella precedentemente vista è una reazione vincolare unidirezionale

## Classificazione dei vincoli (teorica)
### Vincoli unidirezionali
#### fino ad ora abbiamo osservato reazioni vincolari unidirezionali ovvero vincoli che esercitano una reazione vincolare in una sola direzione, come ad esempio un oggetto appoggiato su di un piano
#### ![./images/09.jpg](./images/09.jpg)
### Vincoli bidirezionali
#### un vincolo bidirezionale, è vincolato ad un piano in due direzioni diverse, ad esempio come una sbarretta metallica fissata al terreno
##### tirando l'oggetto esso rimane attaccato al terreno
##### spingendo l'oggetto verso il terreno esso rimane comunque attaccato
#### abbiamo quindi descritto una reazione vincolare che in due direzioni diverse, vincola comunque i due oggetti al terreno (omettendo la forza peso)
#### in realtà considerando la Forza Peso, avremo $\vec R_1 = \vec F_1$ e nell'altra direzione $\vec F_2 = \vec F_P + \vec R_2$   
### ![./images/10.jpg](./images/10.jpg)
### in pratica con vincolo bidirezionale posso sia spingere che tirare
### un'altro esempio sempre bidirezionale è un cardine il cui angolo $\Theta$ può variare e la distanza $R$ rimane costante
#### ![./images/11.jpg](./images/11.jpg)


## Esempio dell'ascensore (reazione vincolare unidirezionale)
### ascensore fermo ( $\vec a = 0$)
#### avendo un'ascensore con una persona all'interno, ed essendo l'ascensore fermo, osserviamo che la persona non cade, vi sarà quindi una reazione vincolare
#### $\vec F_P + \vec R = 0$ in maniera vettoriale
##### equilibrio statico
#### osserviamo che $y : - mg + R = 0$ in maniera scalare
##### vediamo che qui $\vec R$ è positivo
#### quindi $R = mg$ ovvero il verso della reazione vincolare sarà positivo
#### ![./images/12.jpg](./images/12.jpg)
### ascensore che sale ( $\vec a > 0$)
#### quando l'ascensore inizia a salire, ovvero è presente accelerazione
##### cambierà lo stato del moto, avremo quindi una reazione vincolare diversa rispetto all'equilibrio
#### osserviamo che $\vec F_P + \vec R = m\vec a$
##### proiettando lungo la $y$  avremo che $R- mg = m a$
##### se $a= 0$ allora avremo che $ R = m g$ caso dell'equilibrio statico
##### con $a\cancel= 0$ allora $R =  ma+ m g = m( a+ g)$
###### avremo un'altra reazione vincolare
###### osserviamo che __la sensazione di peso in ascensore è quindi data dalla reazione vincolare__
#### riassumendo osserviamo che
##### $\vec a > 0$ allora $\vec R > m \vec g$
###### mi sento più pesante quando l'ascensore sale
#### ![./images/13.jpg](./images/13.jpg)
### ascensore che scende ( $\vec a < 0$)
#### proseguendo il ragionamento ma con l'accelerazione negativa vedremo che
##### $-\vec g < \vec a < 0$ allora $0< \vec R < m \vec g$
###### l'ascensore scende e mi sentirò più leggero
##### $a = -g$ allora $R=0$
###### sarà in caduta ilbera e non senirò più il peso del mio corpo
##### $a < -g$ allora $R<0$
###### avremo una reazione vincolare negativa, ovvero sarebbe come se avvenisse un distacco dal pavimento dell'ascensore
###### è importante notare che se il vincolo è bidirezionale allora si potrà avere reazione vincolare negativa (ad esempio essendo legati con una corda all'ascensore) invece se il vincolo è unidirezionale avverrà un distacco e non vi sarà più alcuna reazione vincolare

## Esempio degli astronauti sulla Stazione Spaziale Internazionale
### se $R=0$, ovvero con reazione vincolare nulla si fluttuerà come se si fosse in orbita
#### quindi si verificherà la situazione in cui $a = - g \Rightarrow R = 0$
#### ![./images/14.jpg](./images/14.jpg)
#### ricordando che $\vec a_c = \frac { \vec v^2}{(R_T+H)}$
#### e sapendo che $\vec  F = m \vec a_c$
#### quindi la forza di gravità ha la stessa direzione dell'accelerazione centripeta
### in breve $\vec  F =\frac { G M_T m_P}{(R_T+H)^2}$ in cui la $M_T$ é la massa della terra e $m_P$ é la massa della persona nella stazione spaziale internazionale
### abbiamo quindi un equilibrio dinamico in orbita, che permetterà alla navicella di rimanere in orbita in moto circolare uniforme
