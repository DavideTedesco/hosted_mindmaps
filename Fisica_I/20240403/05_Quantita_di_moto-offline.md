---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# Quantità di moto

## Grandezza fisica
### essa è un'altra grandezza fisica
### è espressa come $\vec P = m_{inerziale} \cdot \vec v$
### quantifica quanto moto ha un corpo

## Esempio di quantità di moto
### è difficile cambiare moto di un oggetto con una grande massa
#### piccola quantità di moto

## Considerazioni sulla quantità di moto
### se la massa in funzione del tempo è costante (lo specifichiamo poichè non è sempre valida tale ipotesi)
#### $m(t) = \text{costante}$ allora
##### $m \cdot \vec a = m \cdot \frac {d\vec v} {dt} = d\frac {(m\vec v)} {dt} = \frac {d\vec p} {dt}$
###### derivata della quantità di moto rispetto al tempo
### quindi per il secondo principio della dinamica potremo scrivere
#### $\vec F = \frac {d\vec p} {dt}$
##### abbiamo quindi portato la massa dentro la derivata
### inoltre tale formula coprirà anche i casi in cui la massa cambia
#### come ad esempio un corpo che esplode
#### ![./images/16.png](./images/16.png)
### ad ogni modo se la massa è costante otterremo
#### $\vec F = m \cdot \vec a$

## Considerazioni di Einstein sulla formula $\vec F = \frac {d\vec p} {dt}$
### Einstein osservò che tale formula è sbagliata e per poter far tornare i calcoli si ha bisogno di un fattore
### la formula complessiva con tale fattore sarà $\vec F = \frac {d} {dt} \cdot \Big{(} \frac P {\sqrt{1-\frac {\vec v^2} {c^2}}}\Big{)}$
### e osserviamo come esso vada applicato alla $\frac {d}{dt} \Big{(}\frac {m\vec v} {\sqrt {1-\frac {\vec v^2} {c^2}}} \Big {)}$
#### sapendo che $m\cdot \vec v$ è la quantità di moto
#### otteniamo il fattore $\frac {m\vec v} {\sqrt {1-\frac {\vec v^2} {c^2}}}$ moltiplicato per la velocità che viene chiamato _massa efficace_ ed a velocità molto grandi modificherà la massa
### quindi $\vec F = m \cdot \vec a$ è una versione molto scolastica della legge di Newton (secondo principio della dinamica)


## Cosa accade integrando: impulso
### osserviamo ora cosa accade integrando per separazione le variabili
### $\vec F = \frac {d\vec p} {dt}$
### quindi $\int_{t_0}^{t} F dt = \int_{\vec P(t_0)}^{\vec P(t)} d \vec P$
### integrando la forza vista con la quantità di moto
#### troveremo quello che è chiamato __impulso della forza__
##### $J = \Delta \vec P$ essa è quella che chiamiamo forma differenziale
###### nei libri inglesi la $\vec P$ viene indicata spesso con $\vec q$
### si potrebbe dire che in __forma integrale__ _la variazione della quantità di moto è data da un impulso della forza_
### in __forma differenziale__ _la variazione istantanea di quantità di moto nel tempo è uguale alla risultante delle delle forze_
### osserviamo che essendo l'integrale un'operazione lineare avremo ad esempio che
#### $\vec J = \int {\vec F_1+ \vec F_2}dt = \vec J_1 + \vec J_2$
##### quindi l'impulso delle forze è uguale alla risultante degli impulsi delle forze
