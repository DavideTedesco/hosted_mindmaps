---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---
# Primo principio della dinamica (principio d'inerzia)
## Essa è apparentemente la più semplice tra le tre leggi e esprime che su di un corpo non agiscono delle forze
## essa afferma che
### _se la forza risultante che agisce su un oggetto è nulla, la velocità del sistema è costante (quindi il corpo rimane in quiete o procede di moto rettilineo uniforme)_
## essa è verificabile per mezzo di numerosi esperimenti
## essa presume che si sappia cosa si esprima quando si parla del concetto di _forza_
## in formule
### $\vec F = 0 \leftrightarrow \begin{cases} \vec v_0 =  \text{costante} = \vec v\\ \text{quiete} \end{cases}$
## quindi se la forza risultante è nulla il moto si muoverà con velocità costante (pari a quella iniziale)
### come ad esempio nel moto circolare uniforme, in cui cambia solamente la direzione
## oppure il corpo resterà in quiete
## essa viene detta __principio d'inerzia__
### perchè se non agiscono forze altre, il corpo si muoverà per inerzia
## non vi è una dimostrazione dato che è stato osservato per via sperimentale
