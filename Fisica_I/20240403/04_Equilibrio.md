---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# Equilibrio

## Statica o dinamica?
### il primo principio della dinamica stabilisce cosa si definisce per __statica__
#### ovvero la situazione in cui la risultante delle forze è pari a zero
##### $\vec F = 0$
##### $\vec v = \text{costante}$
##### moto banale
### il secondo principio invece stabilisce cosa si intende per __dinamica__
#### la risultante delle forze è diversa da zero
##### $\vec F \neq 0$
#### la Forza risultante seguendo il secondo principio sarà pari a
##### $\vec F = m\cdot \vec a$
### è importante notare che
#### per la risoluzione dei problemi di statica si devono studiare e saper manipolare bene i vettori
#### per la risoluzione dei problemi di dinamica  bisogna studiare molti casi particolari

## Equilibrio statico
### la situazione in cui
#### $\vec F = 0$
#### $\vec {v_{iniziale}} = 0$
###  viene chiamata __equilibrio statico__
### in pratica stiamo affermando che dobbiamo trovare le soluzioni necessarie per mantenere il corpo fermo

## Problema inverso rispetto alla cinematica
### stiamo quindi dicendo che dobbiamo risolvere il problema inverso rispetto a prima
### $\vec a \rightarrow \vec v \rightarrow \vec r$

## Esempio sul primo principio della dinamica ed equilibrio statico
### con 1 sola forza pari ad $\vec F_1$per avere tale equilibrio è necessario avere
#### $\vec F_1 = 0$

## Esempio sul secondo principio della dinamica ed equilibrio statico
### con 2 forze seguendo il secondo principio della dinamica
#### $\vec F_1+\vec F_2 = 0$
#### quindi $\vec F_1 = - \vec F_2$
#### ![Fisica_I/20240403/images/12.png](Fisica_I/20240403/images/12.png)
### con 3 forze seguendo il secondo principio della dinamica
#### $\vec F_1 + \vec F_2 + \vec F_3 = 0$
#### ![Fisica_I/20240403/images/13.png](Fisica_I/20240403/images/13.png)
#### avendo 3 forze, per il calcolo della risultante trasliamo i vettori mettendoli in punta-coda
##### per avere la risultante nulla,  osserviamo che avremo 3 forze che costruiscono vettorialmente un triangolo
### con $n$ forze avremo quindi un poligono irregolare
#### ![Fisica_I/20240403/images/14.png](Fisica_I/20240403/images/14.png)
#### per un problema di statica bisognerà quindi capire quali forze agiscono sul moto
##### scomponendo le equazioni in un sistema di riferimento

## Condizione dell'equilibrio dinamico
### in realtà come specificato precedentemente, la condizione di equilibrio non è solamente statica
### infatti può capitare che l'equilibrio sia dinamico, ovvero se
#### primo caso
##### $\vec F = 0$
##### $\vec v  \neq 0$
##### ciò vuol dire che quando un corpo non è soggetto a forza, continua ad andare a quella velocità a cui andava (senza attrito)
##### ![Fisica_I/20240403/images/15.png](Fisica_I/20240403/images/15.png)
#### secondo caso
##### $\vec v = \text {costante}$
##### il corpo ruota con velocità costante attorno a un centro come con un moto circolare uniforme
###### ![Fisica_I/20240403/images/01.png](Fisica_I/20240403/images/01.png)
##### questo è infatti una sorta di equilibro in cui ricordiamo che
###### $\vec a = \vec a_{centripeta} \rightarrow \vec a = \frac {\vec v^2} {R} = costante$
###### quindi $\vec F = \vec m \cdot \vec a_c \rightarrow = m \cdot \frac {\vec v^2}{R}$
##### in pratica stiamo affermando che in questo particolare caso _l'accelerazione non fa cambiare la velocità, ma la mantiene costante_ ciò significa che si mantiene l'equilibrio, ovvero cambia la direzione ed il verso del della velocità del moto circolare, ma non il modulo
