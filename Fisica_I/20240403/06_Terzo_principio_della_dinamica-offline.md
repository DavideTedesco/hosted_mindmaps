---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# Terzo principio della dinamica

## Enunciato
### _ad ogni azione corrisponde una reazione uguale e contraria_
### ciò significa che le forze mutue esercitate sono sempre uguali in modulo e direzione, ma di verso opposto
### quindi esso ci fa comprendere come le forze agiscano tra due corpi

## Esempio di una massa tirata verso un'altra massa
### nel caso vi siano due corpi
### tirando il corpo 1 verso il corpo 2
### allora vi sarà una forza uguale in modulo a quella sul corpo 1 ma di verso opposto sul corpo 2
### ![./images/17.png](./images/17.png)
### quindi $\vec F_{12}$ ovvero la forza con direzione uscente dal corpo con massa $m_1$ sarà di verso opposto a $\vec F_{21}$
#### ovvero $\vec F_{12} = - \vec F_{21}$
### potremo quindi mettere insieme i corpi e le loro forze osservando che le forze risultanti debbano divenire nulle per ottenere un equilibrio statico

## Esempio di un oggetto appeso ad un cavo
### avendo un oggetto con una massa $A$ appeso al soffitto $M$ per mezzo di un cavo
#### ![./images/18.png](./images/18.png)
### osserviamo che esso ha la forza peso $\vec F_p$ che lo porta verso il basso
### vediamo quindi che ad essa si contrappone una forza uguale in modulo ma con verso opposto $\vec F_{AM}$
### inoltre dal soffitto uscirà una forza con verso opposto ma modulo uguale a $\vec F_{AM}$ che chiamiamo $\vec F_{MA}$
### quindi il soffitto risentirà di una forza uguale e opposta a quella dell'oggetto attaccato al cavo e resisterà fino ad un punto limite
