---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# Secondo principio della dinamica (o legge di Newton)

## Enunciato
### sapendo dal primo principio che il corpo non cambia la sua velocità in assenza di forze
### osserviamo il secondo principio che afferma che
#### _la forza che agisce su un corpo è direttamente proporzionale alla massa del corpo e all'accelerazione, e ha stessa direzione e verso. Quindi, l'accelerazione è proporzionale alla forza e inversamente proporzionale alla massa._
### in formule $\vec a = c \cdot \vec F$
### ovvero che la variazione di una quantità di moto la vediamo come una velocità e con il termine _variazione di quantità di moto_ intendiamo che cambia l'accelerazione del corpo in esame in base alla sua velocità
### osserviamo che stiamo dicendo che l'accelerazione è proporzionale ad una forza e quindi stiamo dicendo che la forza è un vettore, perchè per essere proporzionale all'accelerazione deve essere un vettore

## Esempio su cosa sia una Forza
### avendo un piano con un punto materiale su di esso
### muovendo l'oggetto a velocità costante, non vi saranno forze (per ora)
### ![Fisica_I/20240403/images/08.png](Fisica_I/20240403/images/08.png)
### avendo una forza applicata all'oggetto il moto cambierà (come visto con il secondo principio)

## Cosa è la costante  $c$ utilizzata nel secondo principio?
### Essa è una costante inversamente proporzionale alla massa dell'oggetto in esame
### $m= \frac 1 c$
### essa è chiamata _massa inerziale_
#### con essa si indica quanto il corpo si può opporre al moto
### quindi se il corpo è fermo rimarrà fermo
### se il corpo si muove vorrà continuare a muoversi
### inoltre ritornando all'esempio che descriveva il moto circolare di un corpo, osserveremo che serve una forza per farlo curvare (avevamo visto che serviva l'accelerazione centripeta), ed essa osserviamo che dovrà essere proporzionale all'accelerazione
### possiamo sperimentalmente notare che con la stessa forza ma una massa molto grande faremo più fatica a far curvare un corpo
### possiamo quindi riscrivere l'equazione che descrive come sia la massa inerziale in relazione con la costante $c$ in questo modo
#### $m_{inerziale}= \frac 1 c$

## Massa ($m$)
### essa è il nome abbreviato per quella che viene in genere descritta come _massa gravitazionale_ ovvero quella erroneamente chiamata peso di un corpo
### tale costante determina quanto un oggetto è attratto dal centro della terra
### essa andrà a determinare la Forza peso
### infatti essa è pari a $\vec F_{peso} = m_{gravitazionale}\cdot \vec g$
#### nella quale $\vec g$ è la solita accelerazione gravitazionale
##### pari a circa $\vec g \approx 9,8 \frac m {s^2}$
### inoltre è importante notare che $m_g \neq m_i$
#### come ad esempio la costante elastica o i coefficienti di attrito sono diversi da $m_g$
### indipendentemente dalla forza peso che si ha, l'accelerazione che il corpo subisce sarà calcolata con la $m_i=\frac 1 c$
#### ciò perchè è stato sperimentalmente osservato che $m_i = m_g$

## Forza di gravitazione universale
### potremo quindi osservare che $\frac {GM}{R^2}$ presente all'interno della forza di gravitazione universale
#### $|\vec F_g| = \frac {GMm}{R^2}$ è pari a $g$ ovvero il modulo dell'accelerazione gravitazionale
### quindi si avrà $|\vec F_g| = g \cdot m$
### quindi avendo visto che $m_i = m_g$ e osservata la $\vec F_g$
#### possiamo affermare che la massa che utilizzeremo per la forza peso sarà sempre la massa inerziale ovvero quanto il corpo si oppone al cambiamento di moto

## Esempio con due corpi di massa diversa
### ![Fisica_I/20240403/images/09.png](Fisica_I/20240403/images/09.png)
### avendo due corpi con massa diversa $m_1<m_2$
### tirandoli con la stessa forza $\vec F$
### osserviamo che l'accelerazione del primo sarà maggiore di quella del secondo
### infatti $m_2 \cdot \vec a_2 = \vec F = m_1 \cdot \vec a_1$
#### quindi per forza di cose avremo $\frac {a_2} {a_1} = \frac {m_1} {m_2}$
### quindi ad esempio se il corpo $m_2$ ha massa doppia rispetto ad $m_1$ allora l'accelerazione $\vec a_1$ sarà il doppio di $\vec a_2$
### osservando l'esempio opposto, potremo verificare che faremo più fatica a spingere un corpo con massa doppia rispetto ad un altro

## Osservazione sul secondo principio della dinamica
### trattando del secondo principio della dinamica, sarà importante ricordare sempre che $\vec F = m_i\cdot \vec a$
### quindi che la $\vec F$ risente delle forze applicate al corpo
### quindi avendo un corpo tirato da $\vec F_1$, tirato da $\vec F_2$ e tirato da $\vec F_3$ la legge del corpo sarà come descritto in figura:
#### ![Fisica_I/20240403/images/10.png](Fisica_I/20240403/images/10.png)
### quindi la risultante sarà $\vec F = \vec F_1 + \vec F_2 + \vec F_3$
### e per analizzare tale moto dovremo osservare questa forza risultante
### quindi per ogni forza avremo un contributo all'accelerazione
### potremo quindi ottenere un sistema di equazioni $\begin{cases}\vec F_x = \dots \\ \vec F_y = \dots \end{cases}$

## Metodo di risoluzione degli esercizi con le forze
### 1. disegno le forze che agiscono sul corpo
### 2. scrivo la somma delle forze
### 3. scompongono in componenti
### inoltre se abbiamo un altro corpo non attaccato al primo, le forze applicate all'altro corpo non avranno ripercussioni sulle forze del nostro primo corpo
### ![Fisica_I/20240403/images/11.png](Fisica_I/20240403/images/11.png)

## Conseguenze del secondo principio della dinamica
### da ora in poi ci baseremo sull'interpretazione della legge  $\vec F = m \cdot \vec a$ e della cinematica
### potremo infatti così comprendere il moto di un corpo
#### osservando il perchè tramite la dinamica
#### osservando il come tramite la cinematica
### prendendo quindi le equazioni
#### $\vec a = \dot {\vec v} = \ddot {\vec r}$
#### $\vec F = m \cdot \ddot {\vec r}$
### le forze in generale si potranno scrivere come in funzione della posizione, della velocità e dell'accelerazione
#### $F(\vec r, \dot {\vec v}, \ddot {\vec r})=  m \cdot \ddot {\vec r}$
##### ciò si ottiene scomponendo un certo numero di equazioni scalari
##### ottenendo da ultimo
###### $\begin{cases} F(\vec r, \dot {\vec v}, \ddot {\vec r})=  m \cdot \ddot {\vec r}\\ \vec r (0) = \vec r_0 \\ \dot {\vec r (0)} = \vec v_0 \end{cases}$
###### abbiamo quindi la forza in funzione della posizione, della velocità e dell'accelerazione
###### ed inoltre abbiamo le condizioni iniziali per risolvere il problema
###### esso è quindi un problema di Cauchy
