---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---
# Introduzione alla dinamica

## Cinematica
### la cinematica si occupa di descrivere come avviene il moto dei corpi
### ovvero esprimendo le grandezze del moto
#### legge oraria (posizione)
#### velocità
#### accelerazione
### osservando sperimentalmente il moto
#### come la caduta di un grave

## La dinamica si pone la domanda del perchè avviene il moto
### come ad esempio: perchè un oggetto si muove di moto circolare o parabolico?
### ad esempio nel moto circolare, vi sono un accelerazione tangenziale ed una centripeta
#### ![./images/01.png](./images/01.png)
#### quindi con la dinamica ci chiediamo: perchè l'accelerazione centripeta fa cambiare la direzione?
### una freccia in aria perchè si muove e crea un moto parabolico dopo esser stata scoccata?
#### ![./images/02.png](./images/02.png)
#### la freccia in aria ha una forza di attrito viscoso che la frena

## Attrito
### Inizialmente osserveremo esempi e problemi senza gli attriti
### Successivamente inseriremo l'attrito, che può essere di vario tipo

## Esempio del piano inclinato e della pallina
### ![./images/03.png](./images/03.png)
### In tale piano abbiamo una pallina che vogliamo far cadere, ma dove si andrà a fermare?
### Essa dovrà arrivare indipendentemente dall'angolo, alla stessa altezza da cui è partita realizzando un'oscillazione
#### ![./images/04.png](./images/04.png)
### Ma cosa accadrà tenendo un piano fisso e muovendo l'altro?
#### ![./images/05.png](./images/05.png)
#### ![./images/06.png](./images/06.png)
### In caso il piano mobile sia orizzontale e in assenza di attrito la pallina continuerà a rotolare all'infinito con una velocità costante
#### ![./images/07.png](./images/07.png)
### Sapendo l'inclinazione iniziale del piano mobile (oltre a quella di quello fisso) e la velocità iniziale, potremo capire dove la pallina arriverà al massimo con l'oscillazione
