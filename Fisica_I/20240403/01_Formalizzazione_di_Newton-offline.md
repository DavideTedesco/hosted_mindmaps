---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---
# Formalizzazione di Newton

## Newton realizzò una formalizzazione per mezzo di tre leggi (o principi) che descrivo il perchè dei comportamenti di alcuni moti, ovvero quelli chiamati __tre principi della dinamica__
### esse sono tre leggi molto importanti in Fisica

## essi non sono dimostrabili ma sono stati verificati sperimentalmente (e sono validi in prima approssimazione per $\vec v << v_{luce}$ in cui $v_{luce}$ è la velocità della luce)

## Primo principio

## Secondo principio

## Terzo principio
