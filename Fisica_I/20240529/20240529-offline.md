---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# 20240529

## Calore
### dopo aver introdotto il concetto di temperatura, osserviamo il __calore__
### essa è una forma di energia indicata con $\displaystyle Q$ e misurata in Joule $\displaystyle J$
### essa è più simile alle forze non conservative rispetto alla variazione di energia
### le forze non conservative dissipano infatti energia sotto forma di calore, esso è infatti simile all'attrito
### osserviamo che avendo un corpo che scivola su di un piano senza attrito, esso arriverà su un altro inclinato dello stesso angolo, alla stessa altezza di dove esso è partito
### ![./images/01.png](./images/01.png)
### nel caso in cui i due piani siano con attrito allora il corpo arriverebbe sul secondo piano inclinato ad un'altezza minore rispetto a quella di partenza
### ![./images/02.png](./images/02.png)
### quindi $\displaystyle \Delta U_{\text{persa}}=W_{NC}$
### quindi l'energia persa si è dissipata in calore, facendo scaldare la superficie
### da un punto di vista del corpo (microscopico) osserveremo che la superficie (modellizzata come un insieme di masse e molle in cui le masse sono le molecole e le molle sono i legami intermolecolari) dopo che la pallina è passata al di sopra di essa vibrerà più velocemente rispetto a prima
### essendo cambiata la temperatura $\displaystyle T_{\text{ambiente}}\rightarrow T_{\text{ambiente}}+\Delta T$, allora vedremo che essendo la temperatura finale maggiore della iniziale, a livello microscopico significherà che le masse oscillano maggiormente rispetto a prima
### ![./images/03.png](./images/03.png)
### abbiamo quindi descritto il piano come un sistema di masse e molle
### quindi la temperatura è una visione macroscopica del fenomeno di vibrazione microscopico descritto
### _in pratica la pallina, passando sopra alla superficie cede al reticolo un po' di vibrazione_

## Cosa si penseva nell'Ottocento
### nell'Ottocento si pensava (non conoscendo gli elementi a livello microscopico)
### che il calore fosse veicolato tramite un fluido, definito _fluido calorico_
### in pratica si pensava che vi fosse il passaggio di un fluido da un corpo ad un'altro che permetteva di far cambiare la temperatura dei corpi
### ![./images/04.png](./images/04.png)
### osservando quindi una situazione in cui il corpo $\displaystyle A$ ha una temperatura maggiore del corpo $\displaystyle B$, vedremo che dopo un po' di tempo sarà avvenuto uno __scambio di calore__
### in pratica se $\displaystyle T_A>T_B$ allora dopo il passaggio di un certo tempo, otterremo (se i due corpi sono a contatto) che $\displaystyle T_{A\text{finale}}=T_{B\text{finale}}=T_{\text{finale}}$
### vedremo attraverso un grafico riportante il tempo sulle ascisse e la temperatura sulle ordinate, che raggiungeremo l'equilibrio termico nella temperatura finale
### ![./images/05.png](./images/05.png)
### potremo infatti osservare a livello microscopico che gli atomi del corpo $\displaystyle A$ sbattendo con quelli del corpo $\displaystyle B$ cedono una quantità di moto
### avviene in tal modo lo scambio di calore

## Osservazione del calore da uno stato iniziale ad uno stato finale
### avendo quindi nozioni di meccanica, e sapendo cosa sia l'energia
### osserveremo lo stato iniziale e lo stato finale del sistema
### potremo quindi valutare quanto sia cambiata l'energia del sistema
### invece a livello dinamico potremo dire veramente poco, poichè i corpi sono rimasti fermi in un sistema fermo
### quindi sembra che l'energia meccanica si conservi
### sarà dunque avvenuto qualcosa, ma con la nuova grandezza (calore) appena introdotta, potremo quindi riassumere queste considerazioni attraverso quello che chiamiamo __scambio di calore__
### in pratica __quando vi è una variazione di temperatura avviene uno scambio di calore__

## Collegamento tra la variazione di temperatura e lo scambio di calore
### ad un certo punto nell'Ottocento gli studiosi si accorsero che era errato pensare lo scambio di calore tramite un fluido calorico
### infatti si iniziò a vedere il calore come variazione di energia
### quindi si iniziò a misurare in Joule ($J$) come il lavoro $\displaystyle W$ e le energie

## Calore e forze non conservative
### il calore è quindi una funzione della variazione di temperatura
### e potremo definirlo come $\displaystyle Q=f(\Delta T)$
### che è simile a $\displaystyle W_{NC}=\Delta E$
### avendo due corpi $\displaystyle A$ e $\displaystyle B$ potremo osservare nel grafico $\displaystyle P,V$ una trasformazione termodinamica
### ![./images/06.png](./images/06.png)
### quindi come per il lavoro $\displaystyle W\neq W_B-W_A$ e quindi non possiamo definire $\displaystyle \Delta W$ anche per il calore non potremo definire $\displaystyle \Delta Q$ ovvero la differenza tra calori
### invece per l'energia e la temperatura (essendo funzioni di stato) potremo definire la variazione $\displaystyle \Delta T=T_B-T_A$

## Calore: definizione
### ![./images/07.png](./images/07.png)
#### come visto per i gradi nella misurazione degli angoli realizziamo un passagio da gradi Celsius a gradi Kelvin, ovvero la scala di misurazione di temperatura più comunemente utilizzata in ambito scientifico
#### per i gradi ad esempio $\displaystyle 60^{\circ}\rightarrow {\pi \over 3}$
#### per i Kelvin $\displaystyle 0^{\circ } C\rightarrow {273.15^{\circ} K}$
### abbiamo quindi aumentato la temperatura di un $\displaystyle \Delta T$ ed il calore indica la quantità di energia da fornire al sistema per aumentare la temperatura di un $\displaystyle \Delta T$
### quindi __$Q$ è la quantità di energia per aumentare $\displaystyle T$ di $\displaystyle \Delta T$__

## Caloria
### è l'unità di misura che mi permette di misurare il calore
### essa è pari a: __il calore fornito per aumentare di $\displaystyle 1^{\circ}C$ la temperatura di $\displaystyle 1g$ di acqua ($H_2O$)__
### $\displaystyle 1 \quad cal := Q \text{ fornito per aumentare di } 1^{\circ} C\quad T \text{ di } 1g \text{ di }H_2O$
### viene scelta l'acqua perchè molto reperibile
### tale misura viene ottenuta sperimentalmente osservando quanto si è speso per far innalzare il grammo d'acqua di 1 grado Celsius
### avremo quindi con la caloria un riferimento che descrive una quantità di calore utilizzato per riscaldare il grammo d'acqua
### solo successivamente si collegò tale discorso al Joule
#### ricordiamo che il Joule è calcolato come $\displaystyle \text{Newton}\cdot\text{metri}$

## Esempio generico di applicazione per $\displaystyle Q=f(\Delta T)$
### potremo quindi misurare materiali diversi e scrivere $\displaystyle Q=c \space m\Delta T$
### in cui la $\displaystyle c$ identifica il __calore specifico__ del materiale che stiamo utilizzando
#### il calore specifico è un coefficiente relativo al materiale che stiamo osservando che indica _la quantità di calore necessaria per innalzare, o diminuire, di un Kelvin la temperatura di una unità di sostanza_
#### esso è quindi relativo alla massa utilizzata nel sistema
### si definisce inoltre un'altra quantità $\displaystyle C=c \space m$ chiamata __capacità termica__
### la capacità termica misura quanto è difficile far cambiare la temperatura di un corpo
### essa la potremo anche vedere $\displaystyle Q=C\Delta T$ e quindi $\displaystyle C= {Q \over \Delta T}$
### quindi _se abbiamo due corpi con capacità termica $\displaystyle C$ diversa, quello con capacità termica maggiore, avrà una maggiore necessità di calore per arrivare alla stessa temperatura di quello con capacità termica inferiore_
### __come il momento d'inerzia ci diceva per corpi che ruotano quanto il corpo resiste alle rotazioni, allo stesso modo, la capacità termica, ci indica quanto un corpo resiste alla variazione di temperatura__

## Capacità termiche
### tali capacità termiche non sono delle costanti, infatti dipendono dalla temperatura dello stesso corpo soggetto a calore
### inoltre esse sono dipendenti anche dal tempo e da altre grandezze fisiche
### $\displaystyle C=C(T,P,V)$

## Legame tra temperatura e calore da $\displaystyle Q=f(\Delta T)$
### come visto, essendo una funzione di stato, per la temperatura possiamo definire una variazione di temperatura
### per il calore, non potremo definire una variazione di calore, infatti __il calore non è una funzione di stato__
### possiamo infatti ricollegarci al lavoro $\displaystyle W$ il quale anch'esso non è una funzione di stato (mentre lo è l'energia potenziale)
### un esempio si può osservare prendendo una trasformazione termodinamica
#### ![./images/08.png](./images/08.png)
#### prendendo la traiettoria di tale trasformazione, osserveremo come cambia la temperatura di un corpo da uno stato $\displaystyle A$ ad uno stato $\displaystyle B$
#### vediamo che prendendo un infinitesima variazione di temperatura essa sarà definita come $\displaystyle dT$
#### quindi sarà avvenuto uno scambio di calore infinitesimo pari a $\displaystyle dT$
#### non potremo però chiamare $\displaystyle \delta Q$ un differenziale della temperatura $\displaystyle dQ$ (ciò è sbagliato) perchè dovremo prendere per $\displaystyle Q$ un differenziale inesatto
### dall'esempio capiamo che $\displaystyle \int_A^B \delta Q \neq Q_B-Q_A$ mentre sappiamo che per un differenziale esatto potremo trovare una primitiva $\displaystyle \int_A^B dT = T_B-T_A$ ciò perchè il calore non è conservativo
### infatti __in fisica il calore dipenderà dal percorso svolto per arrivare in un certo stato__ (come per il lavoro)

## Come legare $\displaystyle \delta Q$ e $\displaystyle dT$ ?
### potremo però affermare che $\displaystyle \delta Q= cmdT$ e quindi ricondurci a un differenziale esatto da uno inesatto
#### come avevamo fatto anche per il lavoro, il quale avevamo visto essere $\displaystyle \delta W = d K$
### potremo quindi ora integrare il differenziale esatto  e scrivere lo scambio di calore infinitesimo
### avremo quindi che $\displaystyle Q=\int\delta Q= \int CdT$
### osserviamo che abbiamo $\displaystyle C$ all'interno dell'integrale, se $\displaystyle C$ non dipende dalla temperatura, allora potremo portarlo fuori dall'integrale ed esso sarà costante
### altrimenti la $\displaystyle C$ resterà nell'integrale

## Esempio di capacità termica dipendente dalla temperatura
### osserviamo che se $\displaystyle C(T)=fT$ allora avremo che $\displaystyle T_B-T_A=\int_A^B dT$
### inoltre $\displaystyle Q=f\int_A^B TdT=f({T_B^2 \over 2}-{T_A^2 \over 2})$
### quindi osserveremo che il calore necessario dipenderà dal percorso effettuato

## Esempio macroscopico (esercizio)
### ![./images/09.png](./images/09.png)
### abbiamo due corpi a contatto con temperature $\displaystyle T_A>T_B$
### sappiamo che dopo un po' di tempo la temperatura finale dei due corpi sarà la stessa
### dati $\displaystyle m_A,m_B,c_A,c_B$
### ci viene chiesto di trovare la temperatura finale $\displaystyle T_f$
### quindi come affermato precedentemente, diremo che la temperatura finale la potremo raggiungere quando vi sarà equilibrio termico
### vediamo che $\displaystyle A$ cede calore a $\displaystyle B$, quindi una quantità di calore $\displaystyle Q_A$ sarà ceduta da $\displaystyle A$ mentre una quantità di calore $\displaystyle Q_B$ sarà ricevuta da $\displaystyle B$
#### in pratica sarebbe come vedere che _da $\displaystyle A$ sta uscendo qualcosa_ mentre _in $\displaystyle B$ sta entrando qualcosa_
### osserviamo che il __sistema è isolato__ rispetto all'esterno, ovvero ha delle __pareti adiabatiche__ (con le quali non avvengono scambi di calore con l'esterno)
### quindi tutti i possibili scambi di calore saranno interni al sistema
### in pratica osserveremo che $\displaystyle Q_{\text{totale}}=0$ e quindi in pratica $\displaystyle Q_{\text{totale}}=Q_A+Q_B$
### quindi se $\displaystyle A$ perde qualcosa, allora $\displaystyle B$ acquisirà la quantità persa da $\displaystyle A$
### quindi $\displaystyle Q_A=-Q_B$
### __convenzione sui segni del calore__
#### da ora in poi utilizzeremo una convenzione sui segni del calore
#### sarà per noi __positivo__ il calore se è __ricevuto__ ovvero $\displaystyle Q>0$ se la temperatura si alza nel corpo in cui viene ricevuto il calore
#### sarà __negativo__ il calore se è __ceduto__ ovvero $\displaystyle Q<0$, ovvero se la temperatura si abbassa nel corpo da cui viene ceduto il calore
### vedremo quindi che il calore fluisce da $\displaystyle A$ a $\displaystyle B$
### quindi avremo che $\displaystyle Q>0$ per $\displaystyle B$ che riceve calore
### invece $\displaystyle Q<0$ per $\displaystyle A$ che cede calore
### la somma totale è per tale motivo nulla
### usando quindi la formula del calore vista precedentemente $\displaystyle Q=cm\Delta T$, potremo scrivere $\displaystyle c_Am_A\Delta T_A=-c_Bm_B\Delta T_B$
### che sarebbe anche come scrivere $\displaystyle C_A(T_f-T_A)=C_B(T_f-T_B)$
### vediamo che essa è un'equazione lineare in $\displaystyle T_f$ e sarà pertanto risolvibile come $\displaystyle (C_A+C_B)T_f=C_BT_B+C_AT_A$
### quindi la temperatura finale sarà $\displaystyle T_f={C_BT_B+C_AT_A \over C_A+C_B}$

## Osservazioni applicando la convenzione vista
### potremo quindi osservare che applicando la convezione vista per i segni del calore, avremo
### per il calore ricevuto $\displaystyle Q=cm\Delta T$
### per il calore ceduto $\displaystyle -Q=-cm\Delta T$

## Calore investito in un cambiamento di stato (calore latente di fusione)
### _è importante notare che alle volte fornendo calore al sistema, non si riesce a far cambiare la temperatura dello stesso_ ciò in alcuni specifici casi
### ![./images/10.png](./images/10.png)
### ad esempio avendo una bacinella con del ghiaccio, quindi a $\displaystyle T=0^{\circ}C$
### fornendo calore alla bacinella, riusciremo solamente a far sciogliere i cubetti di ghiaccio per un certo tempo, mantenendo però la temperatura costante a $\displaystyle T=0^{\circ}C$
### in pratica il calore fornito servirà in prima istanza, a far cambiar stato al ghiaccio per farlo divenire acqua, quindi per realizzare il passaggio da un solido ad un liquido
### ![./images/11.png](./images/11.png)
### dal punto di vista microscopico sarebbe come vedere il solido con delle molecole collegate da molle le une alle altre
### il liquido avrà invece dei legami molto meno forti, quindi servirà un __calore latente__ o __calore di fusione__ per poter spezzare i legami forti e far divenire il ghiaccio liquido
### a livello energetico, sarebbe come essere all'interno di una cunetta che descrive il fatto che si abbia un solido, se si riescono a scollegare i legami che rendono solido un corpo, allora esso cambierà stato
### si dovrà quindi fornire un certo calore per poter permettere al ghiaccio di cambiare stato

## Differenza tra sistemi aperti, sistemi chiusi e sistemi isolati
### __sistema aperto__
#### è ad esempio una pentola con acqua che bolle ed evapora
#### __vi sarà scambio di materia con l'esterno__
#### __vi può essere scambio di energia__
#### ![./images/12.png](./images/12.png)
### __sistema chiuso__
#### un sistema chiuso è ad esempio una pentola con il coperchio chiuso
#### osserveremo che mentre l'acqua bolle, __non vi sarà scambio di materia con l'esterno__
#### ma dato che per far bollire l'acqua abbiamo messo la pentola su una fonte di calore, __vi può essere scambio di energia__
#### ![./images/13.png](./images/13.png)
### __sistema isolato__
#### in esso __non vi sarà scambio di materia con l'esterno__
#### inoltre __non vi sarà scambio di energia con l'esterno__
#### ![./images/14.png](./images/14.png)
#### esso viene in genere rappresentato raffigurando delle doppie pareti (come per i doppi vetri)

## Relazione tra sistema ed ambiente
### il sistema è quindi il blocco che osserviamo
### l'ambiente è tutto il resto
### ovviamente a seconda del punto di vista, più o meno macroscopico, si potrà considerare ambiente un qualcosa al cui interno vi sono dei sistemi
### ad esempio osservando in un ambiente il sistema composto da $\displaystyle A$ e da $\displaystyle B$ e guardando attentamente solo uno di tali sistemi, se ci concentrassimo solo su uno dei due, potremo ad esempio vedere $\displaystyle B$ come un nuovo ambiente al cui interno magari riconosciamo un sistema $\displaystyle C$, etc...

## Esercizio n.9 della lista del Borghi (svolto)
