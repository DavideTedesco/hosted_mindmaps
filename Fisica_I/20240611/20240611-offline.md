---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
  colorFreezeLevel: 2
---

# 20240611

## Riepilogo
### ![./images/01.png](./images/01.png)

## Principio di aumento dell'Entropia

## Esempio del principio di aumento dell'Entropia

## Perchè fu introdotta l'Entropia e che legami ha con l'informazione?

## Esempio

## Perchè le trasformazioni migliori sembra che siano quelle reversibili: isoterma

## Perchè le trasformazioni migliori sembra che siano quelle reversibili: adiabatica

## Esempio del passaggio del calore da una sorgente $\displaystyle A$ ad una sorgente $\displaystyle B$

## Esercizio per casa (pentola con acqua scaldata nel forno e Entropia)

## Entropia di un gas perfetto in una trasformazione reversibile

## Trasformazione reversibile isoterma ed Entropia

## Trasformazione reversibile adiabatica ed Entropia

## Trasformazione reversibile isoterma in compressione ed Entropia

## Espansione libera ed Entropia

## Esercizi
### Esercizio d'esame del 31 gennaio 2022
### Esercizio isoterma-isobara-adiabatica e calcolo del rendimento
### Esercizio con una trasformazione irreversibile e calcolo dell'Entropia
