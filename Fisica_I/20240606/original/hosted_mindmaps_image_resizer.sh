ext="png"
out_dir="../images"
resolution=600
for img in *."$ext"; do 
echo $img
mogrify -path $out_dir -filter Triangle -define filter:support=2 -thumbnail $resolution -unsharp 0.25x0.08+8.3+0.045 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB $img;
done


