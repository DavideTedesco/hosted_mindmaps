---
markmap:
  maxWidth: 600
  initialExpandLevel: 2
---

# 20240408

## Riepilogo
### Tre principi della dinamica
### Forza di gravità
### Forza peso
## Reazione vincolare
### ![Fisica_I/20240408/images/01.png](Fisica_I/20240408/images/01.png)
#### esso è un vincolo unidirezionale
## Forza di attrito
### con formula $\vec F_{AS} = \vec N\cdot \mu_{s}$ oppure $\vec F_{AD} = \vec N\cdot \mu_{d}$
### si distingue in due tipologie
#### statico
##### si ha con il corpo fermo, anche chiamato attrito al distacco e si ha quando il corpo è fermo
#### il modulo della Forza di attrito statico rispetta questa disequazione $0\geq |\vec F_{AS}| \geq \vec N \cdot \mu$
##### ciò significa che il corpo si muoverà solamente superata un certo valore di Forza applicata pari a $\vec F_{AS} = \vec N\cdot \mu_{s}$
##### ![Fisica_I/20240408/images/02.png](Fisica_I/20240408/images/02.png)
###### il corpo sarà fermo ovvero $\vec F_{AS} = \vec F$
#### dinamico
##### esso si ha solo quando il corpo è in movimento ma dipende come l'attrito statico dalla normale
##### $\vec F_{AD} = \vec N\cdot \mu_{d}$ vale sempre e non vi è disequazione
## Legame tra $\vec F_{AS}$ ed $\vec N$
## Differenza di valori tra attrito statico e dinamico
### ![Fisica_I/20240408/images/03.png](Fisica_I/20240408/images/03.png)
